<?php
/**
 * This file belongs to the Product Add-ons for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $classes Extra classes for the container.
 * @param array $group_data Data for each radio field.
 * @param string $field_name Group's name.
 * @param string $field_value Field's default or previous value.
 *
 * @param string $fieldset String or boolean for fieldset border.
 * @param string $legend If fieldset show legend.
 */

?>
<div class="yith-pawcp-input yith-pawcp-input-radio-group-container
	<?php
	if ( ! empty( $classes ) && is_array( $classes ) ) {
		foreach ( $classes as $class ) {
			echo esc_html( ' ' . $class );
		}
	}
	?>
">
	<?php
	$fieldset_bool = ! empty( $fieldset ) && ( $fieldset || 'fieldset' === $fieldset );
	if ( $fieldset_bool ) {
		echo '<fieldset>';
		if ( ! empty( $legend ) ) {
			echo '<legend>' . esc_html( $legend ) . '</legend>';
		}
	}
	foreach ( $group_data as $single_data ) {
		$single_data['checked']    = ! empty( $field_value ) && $single_data['field_value'] === $field_value;
		$single_data['field_name'] = $field_name;
		echo esc_html( yith_pawcp_get_view( '/input-fields/radio-single.php', $single_data ) );
	}
	if ( $fieldset_bool ) {
		echo '</fieldset>';
	}
	?>
</div>
