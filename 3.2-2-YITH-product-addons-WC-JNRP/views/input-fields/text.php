<?php
/**
 * This file belongs to the Product Add-ons for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $classes Extra classes for the container.
 * @param string $field_name Field's name.
 * @param string $field_name_display Name to display to users.
 * @param string $field_value Field's default or previous value.
 *
 * @param string $placeholder Text placeholder.
 */

?>
<div class="yith-pawcp-input yith-pawcp-input-text-container
	<?php
	if ( ! empty( $classes ) && is_array( $classes ) ) {
		foreach ( $classes as $class ) {
			echo esc_html( ' ' . $class );
		}
	}
	?>
">
	<?php if ( ! empty( $field_name_display ) ) { ?>
		<label class="yith-pawcp-input-text-label" for="input_text_<?php echo esc_html( $field_name ); ?>">
			<?php echo esc_html( $field_name_display ); ?>
		</label>
	<?php } ?>
	<input
		type="text"
		class="yith-pawcp-input-text-field"
		name="<?php echo esc_html( $field_name ); ?>"
		id="input_text_<?php echo esc_html( $field_name ); ?>"
		placeholder="<?php echo esc_html( ! empty( $placeholder ) ? $placeholder : '' ); ?>"
		value="<?php echo esc_html( ! empty( $field_value ) ? $field_value : '' ); ?>"
	>
</div>
