<?php
/**
 * This file belongs to the Product Add-ons for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $classes Extra classes for the container.
 * @param string $field_name Field's name.
 * @param string $field_name_display Name to display to users.
 *
 * @param string $checked .
 */

?>
<div class="yith-pawcp-input yith-pawcp-input-checkbox-container
	<?php
	if ( ! empty( $classes ) && is_array( $classes ) ) {
		foreach ( $classes as $class ) {
			echo esc_html( ' ' . $class );
		}
	}
	?>
">
	<input
		type="checkbox"
		class="yith-pawcp-input-checkbox-field"
		name="<?php echo esc_html( $field_name ); ?>"
		id="input_checkbox_<?php echo esc_html( $field_name ); ?>"
		value="yes"
		<?php echo ! empty( $checked ) && $checked ? 'checked' : ''; ?>
	>
	<label class="yith-pawcp-input-checkbox-label" for="input_check_onoff_<?php echo esc_html( $field_name ); ?>">
		<?php echo esc_html( ! empty( $field_name_display ) ? $field_name_display : $field_value ); ?>
	</label>
</div>
