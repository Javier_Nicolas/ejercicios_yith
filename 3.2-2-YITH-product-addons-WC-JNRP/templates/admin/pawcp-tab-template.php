<?php
/**
 * This file belongs to the Product Add-ons for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param string $yith_pawcp_index_v general or variation id.
 * @param array $product_data optional previous data.
 */

wp_nonce_field( '_yith_pawcp_admin_data', "_yith_pawcp_nonce[$yith_pawcp_index_v]" );
?>
<button
	type="button"
	class="yith-pawcp-add-new-addon yith-pawcp-button"
>
	<?php esc_html_e( 'ADD NEW ADD-ON', 'yith-pawcp-text-domain' ); ?>
</button>
<div class="yith-pawcp-list-addon">
	<?php
	$count = 0;

	foreach ( $product_data as $data ) {
		echo esc_html(
			yith_pawcp_get_template(
				'/admin/pawcp-tab-addon-template.php',
				array(
					'yith_pawcp_index_v' => $yith_pawcp_index_v,
					'yith_pawcp_index_a' => $count,
					'yith_pawcp_hidden'  => false,
					'product_data'       => $data,
				)
			)
		);
		$count++;
	}

	echo esc_html(
		yith_pawcp_get_template(
			'/admin/pawcp-tab-addon-template.php',
			array(
				'yith_pawcp_index_v' => $yith_pawcp_index_v,
				'yith_pawcp_index_a' => $count,
				'yith_pawcp_hidden'  => true,
				'product_data'       => array(),
			)
		)
	);
	?>
</div>
<button
	type="button"
	class="yith-pawcp-add-new-addon yith-pawcp-button yith-pawcp-add-new-addon-bottom
	<?php echo esc_html( $count > 0 ? '' : 'yith-pawcp-hidden' ); ?>"
>
	<?php esc_html_e( 'ADD NEW ADD-ON', 'yith-pawcp-text-domain' ); ?>
</button>
