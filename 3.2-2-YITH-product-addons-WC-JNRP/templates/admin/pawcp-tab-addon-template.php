<?php
/**
 * This file belongs to the Product Add-ons for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param string $yith_pawcp_index_v general or variation id.
 * @param string $yith_pawcp_index_a addon id.
 * @param boolean $yith_pawcp_hidden add class if hidden template.
 * @param array $product_data optional previous data.
 */

?>
<div class="yith-pawcp-single-addon <?php echo $yith_pawcp_hidden ? 'yith-pawcp-hidden-template-tab' : ''; ?>">
	<div class="yith-pawcp-single-addon-header">
		<div class="yith-pawcp-single-addon-header-action">
			<h5>
				<i class="yith-pawcp-single-addon-header-arrow fas fa-chevron-up"></i>
				<span class="yith-pawcp-single-addon-header-name">
					<?php echo esc_html( isset( $product_data['name'] ) ? $product_data['name'] : 'Untitled' ); ?>
				</span>
			</h5>
		</div>
		<?php
		echo esc_html(
			yith_pawcp_get_view(
				'/input-fields/check-onoff.php',
				array(
					'field_name'  => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][enable_addon]",
					// 'field_name_display' => __( 'Enable', 'yith-pawcp-text-domain' ),
					'field_value' => 'yith_pawcp_enable',
					'classes'     => array( 'yith-pawcp-enable-addon-check' ),
					'checked'     => isset( $product_data['enable_addon'] ) ? $product_data['enable_addon'] : false,
				)
			)
		);
		?>
	</div>
	<div class="yith-pawcp-single-addon-body">
		<?php
		echo esc_html(
			yith_pawcp_get_view(
				'/input-fields/text.php',
				array(
					'field_name'         => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][name]",
					'field_name_display' => __( 'Name', 'yith-pawcp-text-domain' ),
					'classes'            => array( 'yith-pawcp-input-title-name-field' ),
					'field_value'        => isset( $product_data['name'] ) ? $product_data['name'] : '',
				)
			)
		);
		echo esc_html(
			yith_pawcp_get_view(
				'/input-fields/text-area.php',
				array(
					'field_name'         => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][description]",
					'field_name_display' => __( 'Description', 'yith-pawcp-text-domain' ),
					'classes'            => array(),
					'field_value'        => isset( $product_data['description'] ) ? $product_data['description'] : '',
				)
			)
		);
		echo esc_html(
			yith_pawcp_get_view(
				'/input-fields/select.php',
				array(
					'field_name'  => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][field_type]",
					'fieldset'    => 'fieldset',
					'legend'      => __( 'Field type', 'yith-pawcp-text-domain' ),
					'classes'     => array( 'yith-pawcp-field-settings' ),
					'group_data'  => array(
						array(
							'field_name_display' => __( 'Text', 'yith-pawcp-text-domain' ),
							'field_value'        => 'text',
						),
						array(
							'field_name_display' => __( 'Text area', 'yith-pawcp-text-domain' ),
							'field_value'        => 'text-area',
						),
						array(
							'field_name_display' => __( 'Select', 'yith-pawcp-text-domain' ),
							'field_value'        => 'select',
						),
						array(
							'field_name_display' => __( 'Radio', 'yith-pawcp-text-domain' ),
							'field_value'        => 'radio-group',
						),
						array(
							'field_name_display' => __( 'Checkbox', 'yith-pawcp-text-domain' ),
							'field_value'        => 'checkbox',
						),
						array(
							'field_name_display' => __( 'On-Off', 'yith-pawcp-text-domain' ),
							'field_value'        => 'check-onoff',
						),
					),
					'field_value' => isset( $product_data['field_type'] ) ? $product_data['field_type'] : 'text',
				)
			)
		);
		echo esc_html(
			yith_pawcp_get_view(
				'/input-fields/radio-group.php',
				array(
					'field_name'  => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][price_settings]",
					'fieldset'    => 'fieldset',
					'legend'      => __( 'Price settings', 'yith-pawcp-text-domain' ),
					'classes'     => array( 'yith-pawcp-price-settings' ),
					'group_data'  => array(
						array(
							'field_name_display' => __( 'Free', 'yith-pawcp-text-domain' ),
							'field_value'        => 'free',
							'classes'            => array(),
						),
						array(
							'field_name_display' => __( 'Fixed price', 'yith-pawcp-text-domain' ),
							'field_value'        => 'pricefixed',
							'classes'            => array(),
						),
						array(
							'field_name_display' => __( 'Price per character', 'yith-pawcp-text-domain' ),
							'field_value'        => 'pricechar',
							'classes'            => array( 'yith-pawcp-only-text' ),
						),
					),
					'field_value' => isset( $product_data['price_settings'] ) ? $product_data['price_settings'] : 'free',
				)
			)
		);
		echo esc_html(
			yith_pawcp_get_view(
				'/input-fields/text.php',
				array(
					'field_name'         => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][price]",
					'field_name_display' => __( 'Price', 'yith-pawcp-text-domain' ),
					'classes'            => array( 'yith-pawcp-not-free', 'yith-pawcp-single-price' ),
					'field_value'        => isset( $product_data['price'] ) ? $product_data['price'] : '0',
				)
			)
		);
		echo esc_html(
			yith_pawcp_get_view(
				'/input-fields/number.php',
				array(
					'field_name'         => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][free_chars]",
					'field_name_display' => __( 'Free characters', 'yith-pawcp-text-domain' ),
					'classes'            => array( 'yith-pawcp-not-free', 'yith-pawcp-only-text' ),
					'min'                => '0',
					'max'                => '',
					'step'               => '1',
					'field_value'        => isset( $product_data['free_chars'] ) ? $product_data['free_chars'] : '0',
				)
			)
		);
		echo esc_html(
			yith_pawcp_get_view(
				'/input-fields/check-onoff.php',
				array(
					'field_name'         => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][enable_by_default]",
					'field_name_display' => __( 'Default enabled', 'yith-pawcp-text-domain' ),
					'field_value'        => 'yith_pawcp_enable_by_default',
					'classes'            => array( 'yith-pawcp-hidden-template-enable-by-default', 'yith-pawcp-bool-option' ),
					'checked'            => isset( $product_data['enable_by_default'] ) ? $product_data['enable_by_default'] : false,
				)
			)
		);
		?>
		<div class="yith-pawcp-group-option-list">
			<span class="yith-pawcp-group-option-list-title"><?php esc_html_e( 'Options', 'yith-pawcp-text-domain' ); ?></span>
			<div class="yith-pawcp-group-option-body">
				<?php
				if ( ! empty( $product_data['group_option'] ) ) {
					foreach ( $product_data['group_option']['text'] as $key => $text ) {
						?>
						<div class="yith-pawcp-group-option-single">
							<?php
							echo esc_html(
								yith_pawcp_get_view(
									'/input-fields/text.php',
									array(
										'field_name'  => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][group_option][text][]",
										'field_name_display' => __( 'Name', 'yith-pawcp-text-domain' ),
										'classes'     => array(),
										'field_value' => isset( $text ) ? $text : '',
									)
								)
							);
							echo esc_html(
								yith_pawcp_get_view(
									'/input-fields/text.php',
									array(
										'field_name'  => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][group_option][price][]",
										'field_name_display' => __( 'Price', 'yith-pawcp-text-domain' ),
										'classes'     => array( 'yith-pawcp-not-free' ),
										'field_value' => isset( $product_data['group_option']['price'][ $key ] ) ? $product_data['group_option']['price'][ $key ] : '0',
									)
								)
							);
							?>
							<button
								type="button"
								class="yith-pawcp-remove-group-option yith-pawcp-button"
								id="<?php echo esc_html( "remove-button_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][group_option][][remove_button]" ); ?>"
								name="<?php echo esc_html( "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][group_option][][remove_button]" ); ?>"
							>
								<i class="fas fa-trash-alt"></i>
							</button>
						</div>
						<?php
					}
				}
				?>
				<div class="yith-pawcp-group-option-single yith-pawcp-template-group-option">
					<?php
					echo esc_html(
						yith_pawcp_get_view(
							'/input-fields/text.php',
							array(
								'field_name'         => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][group_option][text][]",
								'field_name_display' => __( 'Name', 'yith-pawcp-text-domain' ),
								'classes'            => array(),
								'field_value'        => '',
							)
						)
					);
					echo esc_html(
						yith_pawcp_get_view(
							'/input-fields/text.php',
							array(
								'field_name'         => "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][group_option][price][]",
								'field_name_display' => __( 'Price', 'yith-pawcp-text-domain' ),
								'classes'            => array( 'yith-pawcp-not-free' ),
								'field_value'        => '0',
							)
						)
					);
					?>
					<button
						type="button"
						class="yith-pawcp-remove-group-option yith-pawcp-button"
						id="<?php echo esc_html( "remove-button_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][group_option][][remove_button]" ); ?>"
						name="<?php echo esc_html( "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][group_option][][remove_button]" ); ?>"
					>
						<i class="fas fa-trash-alt"></i>
					</button>
				</div>
			</div>
			<button
				type="button"
				class="yith-pawcp-add-option yith-pawcp-button"
				id="<?php echo esc_html( "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][add_group_option]" ); ?>"
				name="<?php echo esc_html( "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][add_group_option]" ); ?>"
			>
				<?php esc_html_e( 'Add new option', 'yith-pawcp-text-domain' ); ?>
			</button>
		</div>
		<div class="yith-pawcp-single-addon-footer">
			<button
				type="button"
				class="yith-pawcp-remove-addon yith-pawcp-button"
				id="<?php echo esc_html( "remove-addon_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][remove_addon]" ); ?>"
				name="<?php echo esc_html( "_yith_pawcp[$yith_pawcp_index_v][$yith_pawcp_index_a][remove_addon]" ); ?>"
			>
				<i class="fas fa-trash-alt"></i>
				<?php esc_html_e( 'REMOVE ADD-ON', 'yith-pawcp-text-domain' ); ?>
			</button>
		</div>
	</div>
</div>

