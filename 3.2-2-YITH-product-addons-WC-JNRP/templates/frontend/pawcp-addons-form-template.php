<?php
/**
 * This file belongs to the Product Add-ons for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $product Product meta data.
 * @param boolean $variation Bool to indicate if this form is for variation addons.
 */

$v_key = 'common';
if ( $variation ) {
	$v_key = 'variation';
}
?>
<div class="yith-pawcp-container">
	<?php
	foreach ( $product as $key => $addon ) {
		if ( $addon['enable_addon'] ) {
			?>
			<h5 class="yith-pawcp-client-addon-name">
				<?php echo esc_html( $addon['name'] ); ?>
			</h5>
			<div class="yith-pawcp-client-addon-body">
			<?php
			$field_type = $addon['field_type'];
			switch ( $field_type ) {
				case 'text':
				case 'text-area':
					?>
					<p class="yith-pawcp-client-addon-description">
						<?php echo esc_html( $addon['description'] ); ?>
					</p>
					<p class="yith-pawcp-client-addon-price">
					<?php
					switch ( $addon['price_settings'] ) {
						case 'free':
							break;
						case 'pricefixed':
							?>
							+ <span
								class="yith-pawcp-client-addon-price-fixed yith-pawcp-client-addon-text-price-figure"
								price="<?php echo esc_html( $addon['price'] ); ?>"
								free_chars="<?php echo esc_html( $addon['free_chars'] ); ?>"
								>
									<?php
									if ( 0 >= $addon['free_chars'] && 0 < $addon['price'] ) {
										echo esc_html( $addon['price'] );
									} else {
										echo '0.00';
									}
									?>
								</span>
							<?php
							echo esc_html( get_woocommerce_currency_symbol() );
							break;
						case 'pricechar':
							?>
							+ <span
								class="yith-pawcp-client-addon-sum-per-char yith-pawcp-client-addon-text-price-figure"
								price="<?php echo esc_html( $addon['price'] ); ?>"
								free_chars="<?php echo esc_html( $addon['free_chars'] ); ?>"
								>
									0.00
								</span>
							<?php
							echo esc_html( get_woocommerce_currency_symbol() );
							break;
						default:
							esc_html_e(
								'There\'s a problem in the pricing, if it persist consult with Customer service',
								'yith-pawcp-text-domain'
							);
					}
					?>
					</p>
					<?php
					yith_pawcp_get_view(
						"/input-fields/$field_type.php",
						array(
							'field_name' => "yith_pawcp_input[$v_key][$key]",
							'classes'    => array( 'yith-pawcp-addon-input', 'yith-pawcp-addon-input-text', "yith-pawcp-addon-input-$field_type" ),
						)
					);
					break;
				case 'select':
				case 'radio-group':
					$options_display = array();
					?>
					<p class="yith-pawcp-client-addon-description">
						<?php echo esc_html( $addon['description'] ); ?>
					</p>
					<?php
					if ( 'free' === $addon['price_settings'] ) {
						foreach ( $addon['group_option']['text'] as $option_key => $text ) {
							$options_display[ $option_key ]['field_name_display'] = $text;
							$options_display[ $option_key ]['field_value']        = $option_key;
						};
					} else {
						$prices_str = array();
						foreach ( $addon['group_option']['text'] as $option_key => $text ) {
							$aux = $addon['group_option']['price'][ $option_key ];
							$options_display[ $option_key ]['field_name_display'] = "$text +($aux " . get_woocommerce_currency_symbol() . ')';
							$options_display[ $option_key ]['field_value']        = $option_key;
							$prices_str[ $option_key ]                            = $aux;
						};
						?>
						<div
							class="yith-pawcp-group-price-hidden-array"
							array="<?php echo esc_html( wp_json_encode( $prices_str ) ); ?>"
							type="<?php echo esc_html( $field_type ); ?>"
							style="display:none;">
						</div>
						<?php
					}
					yith_pawcp_get_view(
						"/input-fields/$field_type.php",
						array(
							'field_name' => "yith_pawcp_input[$v_key][$key]",
							'classes'    => array( 'yith-pawcp-addon-input', 'yith-pawcp-addon-input-group', "yith-pawcp-addon-input-$field_type" ),
							'group_data' => $options_display,
						)
					);
					break;
				case 'checkbox':
				case 'check-onoff':
					yith_pawcp_get_view(
						"/input-fields/$field_type.php",
						array(
							'field_name'         => "yith_pawcp_input[$v_key][$key]",
							'classes'            => array( 'yith-pawcp-addon-input', 'yith-pawcp-addon-input-check', "yith-pawcp-addon-input-$field_type" ),
							'field_name_display' => $addon['description'],
							'checked'            => isset( $addon['enable_by_default'] ) ? $addon['enable_by_default'] : false,
						)
					);
					if ( 'free' !== $addon['price_settings'] ) {
						?>
						<p class="yith-pawcp-client-addon-price yith-pawcp-client-addon-check-price" price="<?php echo esc_html( $addon['price'] ); ?>">
							<?php echo esc_html( '+' . $addon['price'] . ' ' . get_woocommerce_currency_symbol() ); ?> 
						</p>
						<?php
					}
					break;
				default:
					esc_html_e( 'There\'s been an error in the addon field type', 'yith-pawcp-text-domain' );
			}
			?>
			</div>
			<?php
		}
	}
	?>
</div>
<?php
