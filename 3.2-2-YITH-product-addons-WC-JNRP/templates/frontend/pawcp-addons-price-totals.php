<?php
/**
 * This file belongs to the Product Add-ons for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $product_price Product base price.
 */

?>
<div class="yith-pawcp-price-totals-container">
	<p class="yith-pawcp-price-totals-title"><?php esc_html_e( 'Price totals', 'yith-pawcp-text-domain' ); ?> </p>
	<p class="yith-pawcp-price-totals-base">
		<?php esc_html_e( 'Product price', 'yith-pawcp-text-domain' ); ?>
		<span class="yith-pawcp-price-totals-price">
			<span class="yith-pawcp-price-totals-base-figure"><?php echo esc_html( number_format( (float) $product_price, 2 ) ); ?></span>
			<?php echo esc_html( ' ' . get_woocommerce_currency_symbol() ); ?>
		</span>
	</p>
	<p class="yith-pawcp-price-totals-addons">
		<?php esc_html_e( 'Additional options total', 'yith-pawcp-text-domain' ); ?>
		<span class="yith-pawcp-price-totals-price">
			<span class="yith-pawcp-price-totals-addons-figure"></span>
			<?php echo esc_html( ' ' . get_woocommerce_currency_symbol() ); ?>
		</span>
	</p>
	<p class="yith-pawcp-price-totals-total">
		<?php esc_html_e( 'Total', 'yith-pawcp-text-domain' ); ?>
		<span class="yith-pawcp-price-totals-price">
			<span class="yith-pawcp-price-totals-total-figure"></span>
			<?php echo esc_html( ' ' . get_woocommerce_currency_symbol() ); ?>
		</span>
	</p>
</div>
