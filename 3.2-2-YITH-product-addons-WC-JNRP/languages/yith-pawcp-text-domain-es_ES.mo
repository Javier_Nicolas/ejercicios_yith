��            )   �      �     �     �     �  �   �  
   Y     d     l     |  
   �     �     �     �     �     �     �     �     �     �     �     �            !   #     E  	   J  M   T  -   �     �  �  �     �     �     �  u   �     U     a     i     �     �     �     �     �     �     �     �     �     �     �               0     >  ,   F     s     y  \   �  7   �     	                     
                                                                    	                                                 ADD NEW ADD-ON Add new option Additional options total Alert Message: WooCommerce requiresYITH Product Add-ons for Woocommerce is enabled but not effective. It requires WooCommerce in order to work. Base price Checked Default enabled Description Field type Fixed price Free Free characters Name On-Off Options Price Price per character Price settings Price totals Product price REMOVE ADD-ON Settings Sorry, your nonce did not verify. Text Text area There's a problem in the pricing, if it persist consult with Customer service There's been an error in the addon field type Untitled Project-Id-Version: JNRP_3.2-2 Product Addons for Woocommerce Plugin 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/3.2-2-YITH-product-addons-WC-JNRP
PO-Revision-Date: 2021-01-25 13:59+0000
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Domain: yith-pawcp-text-domain
Plural-Forms: nplurals=2; plural=(n != 1);
 AÑADIR ADD-ON Añadir nueva opción Total de opciones adicionales YITH Product Add-ons para Woocommerce está habilitado pero no es efectivo. Requiere WooCommerce para poder trabajar. Precio base Marcado Habilitado por defecto Descripción Tipo de campo Precio fijo Gratis Caracteres gratuitos Nombre Enc/Apag Opciones Precio Precio por caracter Opciones de precios Total de precios Precio del producto BORRAR ADD-ON Ajustes Lo sentimos, su nonce no ha sido verificado. Texto Área de texto Hay un problema en los precios, si persiste consulte con el servicio de atención al cliente Se ha producido un error en el tipo de campo del add-on Sin título 