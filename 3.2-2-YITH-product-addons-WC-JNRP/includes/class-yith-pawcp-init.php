<?php
/**
 * This file belongs to the Product Addons for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_PAWCP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PAWCP_INIT' ) ) {

	/**
	 * Inicializador del plugin.
	 */
	class YITH_PAWCP_INIT {

		/**
		 * Main Instance
		 *
		 * @var YITH_PAWCP_INIT
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/**
		 * Main Admin Instance
		 *
		 * @var YITH_PAWCP_INIT_ADMIN
		 * @since 1.0
		 */
		public $admin = null;

		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_PAWCP_INIT_FRONTEND
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PAWCP_INIT Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6 .
		}

		/**
		 * YITH_PAWCP_INIT constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_pawcp_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
					),
					'admin'    => array(
						'includes/class-yith-pawcp-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-pawcp-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			/*
				Here set any other hooks ( actions or filters you'll use on this class)
			*/

			// Finally call the init function.
			$this->init();

		}

		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param array $main_classes array The require classes file path.
		 *
		 * @author Javier Nicolás Rodríguez Pérez
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_PAWCP_DIR_PATH . $class ) ) {
						require_once YITH_PAWCP_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 *
		 * @author Javier Nicolás Rodríguez Pérez
		 * @since  1.0
		 **/
		public function init_classes() {}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Javier Nicolás Rodríguez Pérez
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_PAWCP_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_PAWCP_Frontend::get_instance();
			}
		}

	}
}

if ( ! function_exists( 'yith_pawcp_init' ) ) {
	/**
	 * Get the YITH_PAWCP_INIT instance
	 *
	 * @return YITH_PAWCP_INIT
	 */
	function yith_pawcp_init() {
		return YITH_PAWCP_INIT::get_instance();
	}
}
