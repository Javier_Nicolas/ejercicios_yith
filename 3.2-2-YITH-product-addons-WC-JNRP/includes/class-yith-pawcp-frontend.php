<?php
/**
 * This file belongs to the Product Addons for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_PAWCP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PAWCP_Frontend' ) ) {

	/**
	 * Frontend related.
	 */
	class YITH_PAWCP_Frontend {

		/**
		 * Main Instance
		 *
		 * @var YITH_PAWCP_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PAWCP_Frontend Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PAWCP_Frotend constructor.
		 */
		private function __construct() {
			// Cart hooks.
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'add_cart_data' ), 10, 3 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'get_cart_data' ), 10, 3 );
			add_action( 'woocommerce_add_order_item_meta', array( $this, 'cart_data_to_order' ), 10, 3 );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'calculate_price' ), 10, 1 );

			// Display in shop.
			add_action( 'woocommerce_before_add_to_cart_quantity', array( $this, 'product_addons_form_display' ), 10, 1 );

			// AJAX.
			add_action( 'wp_ajax_nopriv_yith-pawcp-change-variation', array( $this, 'yith_pawcp_change_form' ) );
			add_action( 'wp_ajax_yith-pawcp-change-variation', array( $this, 'yith_pawcp_change_form' ) );
		}

		/**
		 * Save addons info at adding to cart.
		 *
		 * @param array $cart_item_data .
		 * @param int   $product_id .
		 * @param int   $variation_id .
		 */
		public function add_cart_data( $cart_item_data, $product_id, $variation_id ) {
			// Nonce verification.
			if ( isset( $_POST['_yith_pawcp_nonce'] ) ) {
				if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['_yith_pawcp_nonce'] ) ), '_yith_pawcp_client_data' ) ) {
					print esc_html__( 'Sorry, your nonce did not verify.', 'yith-pawcp-text-domain' );
					exit;
				}
			} else {
				return;
			}
			if ( empty( $_POST['yith_pawcp_input'] ) ) {
				return;
			}

			$cart_item_data['yith_pawcp']['common'] = $this::process_cart_data( wp_unslash( $_POST['yith_pawcp_input']['common'] ), $product_id, 'common' );
			if ( ! empty( $_POST['yith_pawcp_input']['variation'] ) ) {
				$cart_item_data['yith_pawcp']['variation'] = $this::process_cart_data( wp_unslash( $_POST['yith_pawcp_input']['variation'] ), $variation_id, 'variation' );
			}

			return $cart_item_data;
		}

		/**
		 * Process addons info into an array at adding to cart.
		 *
		 * @param array $data Addons values.
		 * @param int   $id product or variation id.
		 */
		public function process_cart_data( $data, $id ) {
			global  $woocommerce;
			$product = wc_get_product( $id );

			$response = array();

			$product_addons = $product->get_meta( 'yith_wcpa_data' );
			foreach ( $product_addons as $key => $addon ) {
				if ( ! empty( $data[ $key ] ) ) {
					$price          = 0;
					$input_value    = sanitize_text_field( $data[ $key ] );
					$price_settings = $addon['price_settings'];
					$display_option = null;
					switch ( $addon['field_type'] ) {
						case 'text':
						case 'text-area':
							switch ( $price_settings ) {
								case 'free':
									break;
								case 'pricefixed':
									if ( strlen( preg_replace( '/\s+/', '', $input_value ) ) > intval( $addon['free_chars'] ) ) {
										$price = $addon['price'];
									}
									break;
								case 'pricechar':
									$price = floatval( $addon['price'] )
									* ( strlen( preg_replace( '/\s+/', '', $input_value ) ) - floatval( $addon['free_chars'] ) );
									if ( $price < 0 ) {
										$price = 0;
									}
									break;
								default:
									return;
							}
							break;
						case 'select':
						case 'radio-group':
							$display_option = $addon['group_option']['text'][ $input_value ];
							if ( 'free' !== $price_settings ) {
								$price = $addon['group_option']['price'][ $input_value ];
							}
							break;
						case 'checkbox':
						case 'check-onoff':
							$input_value = true;
							if ( 'free' !== $price_settings ) {
								$price = $addon['price'];
							}
							break;
						default:
					}

					$response[ $key ] = array(
						'value'          => $input_value,
						'price'          => $price,
						'price_settings' => $price_settings,
						'display'        => $addon['name'],
						'field_type'     => $addon['field_type'],
					);
					if ( isset( $display_option ) ) {
						$response[ $key ]['display_option'] = $display_option;
					}
				}
			}
			return $response;
		}

		/**
		 * Show addons info in cart.
		 *
		 * @param int   $data .
		 * @param array $cart_item .
		 */
		public function get_cart_data( $data, $cart_item ) {
			if ( isset( $cart_item['yith_pawcp'] ) ) {
				$base_price = ! empty( $cart_item['data']->get_sale_price() ) ? $cart_item['data']->get_sale_price() : $cart_item['data']->get_price();
				$data[]     = array(
					'name'  => __( 'Base price', 'yith-pawcp-text-domain' ),
					'value' => $base_price . ' ' . get_woocommerce_currency_symbol(),
				);
				foreach ( $cart_item['yith_pawcp'] as $v_key => $variation_data ) {
					foreach ( $variation_data as $key => $input_data ) {
						$name = $input_data['display'];
						if ( 'free' !== $input_data['price_settings'] && 0 < floatval( $input_data['price'] ) ) {
							$name .= ' (+ ' . $input_data['price'] . ' ' . get_woocommerce_currency_symbol() . ')';
						}
						if ( 'checkbox' === $input_data['field_type'] || 'check-onoff' === $input_data['field_type'] ) {
							$value = __( 'Checked', 'yith-pawcp-text-domain' );
						} else {
							$value = isset( $input_data['display_option'] ) ? $input_data['display_option'] : $input_data['value'];
						}
						$data[] = array(
							'name'  => $name,
							'value' => $value,
						);
					}
				}
			}
			return $data;
		}

		/**
		 * Save addons info as meta.
		 *
		 * @param int    $item_id .
		 * @param array  $values .
		 * @param string $key .
		 */
		public function cart_data_to_order( $item_id, $values, $key ) {
			if ( isset( $values['yith_pawcp'] ) ) {
				$product    = wc_get_product( $values['data']->get_id() );
				$base_price = ! empty( $product->get_sale_price() ) ? $product->get_sale_price() : $product->get_price();
				wc_add_order_item_meta( $item_id, 'yith_pawcp_base_price', $base_price . ' ' . get_woocommerce_currency_symbol() );
				foreach ( $values['yith_pawcp'] as $v_key => $variation_data ) {
					foreach ( $variation_data as $addon_key => $input_data ) {
						$name = 'yith_pawcp_addons_data-' . $input_data['display'];
						if ( 'free' !== $input_data['price_settings'] && 0 < floatval( $input_data['price'] ) ) {
							$name .= '-' . $input_data['price'];
						}
						$value = '';
						if ( 'checkbox' === $input_data['field_type'] || 'check-onoff' === $input_data['field_type'] ) {
							$value = __( 'Checked', 'yith-pawcp-text-domain' );
						} else {
							$value = isset( $input_data['display_option'] ) ? $input_data['display_option'] : $input_data['value'];
						}
						wc_add_order_item_meta( $item_id, $name, $value );
					}
				}
			}
		}

		/**
		 * Modify the item price.
		 *
		 * @param array $cart .
		 */
		public function calculate_price( $cart ) {
			// Required since Woocommerce version 3.2 for cart items properties changes.
			if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 ) {
				return;
			}
			// Loop through cart items.
			foreach ( $cart->get_cart() as $cart_item ) {
				// Sum all addons price.
				if ( isset( $cart_item['yith_pawcp'] ) ) {
					foreach ( $cart_item['yith_pawcp'] as $v_key => $variation_data ) {
						foreach ( $variation_data as $key => $item_data ) {
							if ( 'free' !== $item_data['price_settings'] ) {
								$new_price = $cart_item['data']->get_price() + $item_data['price'];
								$cart_item['data']->set_price( $new_price );
							}
						}
					}
				}
			}
		}

		/**
		 * Addons form display for products.
		 */
		public function product_addons_form_display() {
			global  $woocommerce;
			$product = wc_get_product( get_the_ID() );

			if ( ! empty( $product->get_meta( 'yith_wcpa_data' ) ) ) {
				wp_nonce_field( '_yith_pawcp_client_data', '_yith_pawcp_nonce' );
				echo esc_html(
					yith_pawcp_get_template(
						'/frontend/pawcp-addons-form-template.php',
						array(
							'product'   => $product->get_meta( 'yith_wcpa_data' ),
							'variation' => false,
						)
					)
				);
				echo '<div class="yith-pawcp-loading-container">
						<svg class="yith-pawcp-spinner" viewBox="0 0 50 50">
							<circle class="yith-pawcp-path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle>
						</svg>
					</div>
					<div class="yith-pawcp-variations-container"></div>';
				echo esc_html(
					yith_pawcp_get_template(
						'/frontend/pawcp-addons-price-totals.php',
						array(
							'product_price' => $product->get_price(),
						)
					)
				);
			}
		}

		/**
		 * Función que procesa la llamada AJAX.
		 */
		public function yith_pawcp_change_form() {
			// Nonce verification.
			if ( isset( $_POST['_yith_pawcp_nonce'] ) ) {
				if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['_yith_pawcp_nonce'] ) ), '_yith_pawcp_variations' ) ) {
					print esc_html__( 'Sorry, your nonce did not verify.', 'yith-pawcp-text-domain' );
					exit;
				}
			} else {
				return;
			}
			if ( empty( $_POST['variation'] ) ) {
				return;
			}
			global  $woocommerce;
			$product = wc_get_product( wp_unslash( $_POST['variation'] ) );

			if ( ! empty( $product->get_meta( 'yith_wcpa_data' ) ) ) {
				echo esc_html(
					yith_pawcp_get_template(
						'/frontend/pawcp-addons-form-template.php',
						array(
							'product'   => $product->get_meta( 'yith_wcpa_data' ),
							'variation' => true,
						)
					)
				);
			}
		}
	}
}

if ( ! function_exists( 'yith_pawcp_load_addons' ) ) {
	/**
	 * Addons form css and js.
	 *
	 * @param string $inline_styles CSS to add dynamically.
	 */
	function yith_pawcp_load_addons( $inline_styles = null ) {
		wp_register_style(
			'yith-pawcp-load-product-css',
			YITH_PAWCP_DIR_ASSETS_CSS_URL . '/yith-pawcp-product.css',
			array(),
			false
		);
		wp_enqueue_style( 'yith-pawcp-load-product-css' );
		wp_register_script(
			'yith-pawcp-load-product',
			YITH_PAWCP_DIR_ASSETS_JS_URL . '/yith-pawcp-product.js',
			array( 'jquery' ),
			false,
			true
		);
		wp_enqueue_script( 'yith-pawcp-load-product' );
		wp_localize_script(
			'yith-pawcp-load-product',
			'pawcp_ajax_vars',
			array(
				'ajax_url'          => admin_url( 'admin-ajax.php' ),
				'_yith_pawcp_nonce' => wp_create_nonce( '_yith_pawcp_variations' ),
				'action'            => 'yith-pawcp-change-variation',
			)
		);
		wp_register_style(
			'yith-pawcp-load-fa-css',
			'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css',
			array(),
			'5.11.2'
		);
		wp_enqueue_style( 'yith-pawcp-load-fa-css' );
	}
}

add_action( 'wp_enqueue_scripts', 'yith_pawcp_load_addons' );
