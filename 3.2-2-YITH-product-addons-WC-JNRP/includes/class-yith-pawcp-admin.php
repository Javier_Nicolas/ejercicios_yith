<?php
/**
 * This file belongs to the Product Addons for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_PAWCP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PAWCP_ADMIN' ) ) {

	/**
	 * Admin related.
	 */
	class YITH_PAWCP_ADMIN {

		/**
		 * Main Instance
		 *
		 * @var YITH_PAWCP_ADMIN
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PAWCP_ADMIN Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PAWCP_ADMIN constructor.
		 */
		private function __construct() {
			// TABS.
			// Create the custom tab.
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'create_pawcp_tab' ) );
			// Add the custom fields.
			add_action( 'woocommerce_product_data_panels', array( $this, 'display_pawcp_general_fields' ) );
			// Save the custom fields.
			add_action( 'woocommerce_admin_process_product_object', array( $this, 'save_pawcp_general_fields' ) );

			// Create the variations form.
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'display_pawcp_variation_fields' ), 10, 3 );
			// Save the variations form.
			add_action( 'woocommerce_admin_process_variation_object', array( $this, 'save_pawcp_variation_fields' ), 10, 2 );
		}

		/**
		 * Add the new tab to the $tabs array.
		 *
		 * @param array $tabs .
		 */
		public function create_pawcp_tab( $tabs ) {
			$tabs['yith_pawcp'] = array(
				'label'    => __( 'Add-ons', 'yith-pawcp-text-domain' ),
				'target'   => 'yith_pawcp_panel',
				'class'    => array( 'pawcp_tab', 'show_if_simple', 'show_if_variable' ),
				'priority' => 80,
			);
			return $tabs;
		}

		/**
		 * Display fields for the new panel
		 */
		public function display_pawcp_general_fields() {
			$post_id = get_the_ID();
			$product = wc_get_product( $post_id );

			?>
			<div id="yith_pawcp_panel" class="panel woocommerce_options_panel">
				<div class="options_group show_if_simple show_if_variable yith-pawcp-tab-container">
					<?php
					echo esc_html(
						yith_pawcp_get_template(
							'/admin/pawcp-tab-template.php',
							array(
								'yith_pawcp_index_v' => 0,
								'product_data'       => ! empty( $product->get_meta( 'yith_wcpa_data' ) ) ? $product->get_meta( 'yith_wcpa_data' ) : array(),
							)
						)
					);
					?>
				</div>
			</div>
			<?php
		}

		/**
		 * Save the custom fields.
		 *
		 * @param array $product .
		 */
		public function save_pawcp_general_fields( $product ) {
			$this::save_pawcp_fields( $product, 0 );
		}

		/**
		 * Display fields for the new panel
		 *
		 * @param int   $loop Loop index.
		 * @param array $variation_data .
		 * @param array $variation .
		 */
		public function display_pawcp_variation_fields( $loop, $variation_data, $variation ) {
			$product = wc_get_product( $variation->ID );
			echo esc_html(
				yith_pawcp_get_template(
					'/admin/pawcp-tab-template.php',
					array(
						'yith_pawcp_index_v' => $loop + 1,
						'product_data'       => ! empty( $product->get_meta( 'yith_wcpa_data' ) ) ? $product->get_meta( 'yith_wcpa_data' ) : array(),
					)
				)
			);
		}

		/**
		 * Save the custom fields.
		 *
		 * @param array $variation .
		 * @param int   $loop Loop index.
		 */
		public function save_pawcp_variation_fields( $variation, $loop ) {
			$this::save_pawcp_fields( $variation, $loop + 1 );
		}

		/**
		 * Save the custom fields using CRUD method.
		 *
		 * @param array $product .
		 * @param int   $index .
		 */
		public function save_pawcp_fields( $product, $index = 0 ) {
			// Nonce verification.
			if ( isset( $_POST['_yith_pawcp_nonce'][ $index ] ) ) {
				if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['_yith_pawcp_nonce'][ $index ] ) ), '_yith_pawcp_admin_data' ) ) {
					print esc_html__( 'Sorry, your nonce did not verify.', 'yith-pawcp-text-domain' );
					exit;
				}
			} else {
				return;
			}

			// Unslash (? Sanitize).
			$addons_data = isset( $_POST['_yith_pawcp'][ $index ] ) ? (array) wp_unslash( $_POST['_yith_pawcp'][ $index ] ) : array();
			// Remove empty template.
			array_pop( $addons_data );
			// Save data.
			foreach ( $addons_data as $key => &$addon ) {
				// Fields dependant on field type.
				switch ( $addon['field_type'] ) {
					case 'text':
					case 'text-area':
						unset( $addon['group_option'] );
						unset( $addon['enable_by_default'] );
						if ( 'free' === $addon['price_settings'] ) {
							unset( $addon['price'] );
							unset( $addon['free_chars'] );
						}
						break;
					case 'select':
					case 'radio-group':
						unset( $addon['price'] );
						unset( $addon['free_chars'] );
						unset( $addon['enable_by_default'] );
						array_pop( $addon['group_option']['text'] );
						array_pop( $addon['group_option']['price'] );
						if ( 'free' === $addon['price_settings'] ) {
							unset( $addon['group_option']['price'] );
						}
						break;
					case 'checkbox':
					case 'check-onoff':
						unset( $addon['free_chars'] );
						unset( $addon['group_option'] );
						$addon['enable_by_default'] = isset( $addon['enable_by_default'] ) ? 1 : 0;
						if ( 'free' === $addon['price_settings'] ) {
							unset( $addon['price'] );
						}
						break;
					default:
				}
				// General.
				$addon['enable_addon'] = isset( $addon['enable_addon'] ) ? 1 : 0;
			}
			$product->update_meta_data( 'yith_wcpa_data', $addons_data );
		}
	}
}

/** Admin CSS & JS */

if ( ! function_exists( 'yith_pawcp_load_tabs_css' ) ) {
	/** Purchase note tab css. */
	function yith_pawcp_load_tabs_css() {
		$screen     = get_current_screen();
		$is_product = 'product' === $screen->id;
		if ( $is_product ) {
			// Font Awesome.
			if ( ! wp_style_is( 'yith-pawcp-load-fa-css' ) ) {
				wp_register_style(
					'yith-pawcp-load-fa-css',
					'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css',
					array(),
					'5.11.2'
				);
				wp_enqueue_style( 'yith-pawcp-load-fa-css' );
			}
			wp_register_style(
				'yith-pawcp-load-tabs-css',
				YITH_PAWCP_DIR_ASSETS_CSS_URL . '/yith-pawcp-tab.css',
				array(),
				false
			);
			wp_enqueue_style( 'yith-pawcp-load-tabs-css' );
			wp_register_script(
				'yith-pawcp-load-tabs-js',
				YITH_PAWCP_DIR_ASSETS_JS_URL . '/yith-pawcp-tab.js',
				array( 'jquery' ),
				false,
				true
			);
			wp_enqueue_script( 'yith-pawcp-load-tabs-js' );
			wp_localize_script(
				'yith-pawcp-load-tabs-js',
				'js_strings',
				array(
					'untitled' => __( 'Untitled', 'yith-pawcp-text-domain' ),
				)
			);
		}
	}
}
add_action( 'admin_enqueue_scripts', 'yith_pawcp_load_tabs_css' );


