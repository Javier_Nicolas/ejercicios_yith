jQuery(
	function($) {
		// Events
		$( document ).on( 'found_variation', update_pawcp_form );
		$( document ).ready( load_addons );
		function load_addons() {
			$( 'div.yith-pawcp-loading-container' ).fadeOut();
			$( 'div.yith-pawcp-addon-input-text' ).find( ":input" ).on( 'keyup', change_text_price );
			$( 'div.yith-pawcp-addon-input-group' ).find( ":input" ).on( 'change', update_price_summary );
			$( 'div.yith-pawcp-addon-input-check' ).find( ":input" ).on( 'change', update_price_summary );
			$( ".yith-pawcp-input-check-onoff-field" ).on( 'change', onoff_change );
		}

		$( 'div.yith-pawcp-container' ).ready( update_price_summary );

		// Update form on variation change.
		function update_pawcp_form(){
			$.ajax(
				{
					type : "post",
					url : pawcp_ajax_vars.ajax_url,
					data : {
						_yith_pawcp_nonce: pawcp_ajax_vars._yith_pawcp_nonce,
						action: pawcp_ajax_vars.action,
						variation: $( 'input.variation_id' ).val(),
					},
					beforeSend: function (qXHR, settings) {
						$( 'div.yith-pawcp-variations-container' ).fadeOut().empty();
						$( 'div.yith-pawcp-loading-container' ).fadeIn();
					},
					complete: function () {
						load_addons();
					},
					error: function(response){
						console.log( response );
					},
					success: function(response) {
						$( 'div.yith-pawcp-variations-container' ).append( $.parseHTML( response.slice( 0,-2 ) ) ).fadeIn();
						update_price_summary();
					}
				}
			)
		}

		// Custom On-off.
		function onoff_change() {
			var icon = $( this ).parent().find( ".yith-pawcp-input-check-onoff-icon" ).first();
			if ( this.checked ) {
				$( icon ).removeClass( "fa-toggle-off" );
				$( icon ).addClass( "fa-toggle-on" );
			} else {
				$( icon ).removeClass( "fa-toggle-on" );
				$( icon ).addClass( "fa-toggle-off" );
			}
		}

		// Change price shown for text addons.
		function change_text_price(){
			var fixed    = $( this ).parent().parent().find( 'span.yith-pawcp-client-addon-price-fixed' );
			var per_char = $( this ).parent().parent().find( 'span.yith-pawcp-client-addon-sum-per-char' );
			if (fixed.lenght !== 0 && parseInt( fixed.attr( 'free_chars' ) ) > 0) {
				var price = 0.00;
				if ($( this ).val().replace( /\s/g, '' ).length > parseInt( fixed.attr( 'free_chars' ) )) {
					var price = parseFloat( fixed.attr( 'price' ) );
				}
								fixed.text( price.toFixed( 2 ) );
			} else if (per_char.lenght !== 0) {
				var price = ($( this ).val().replace( /\s/g, '' ).length - parseInt( per_char.attr( 'free_chars' ) )) * parseFloat( per_char.attr( 'price' ) );
				if (price < 0 ) {
					price = 0.00;
				}
				per_char.text( price.toFixed( 2 ) );
			}
			update_price_summary();
		}

		// Update price summary.
		function update_price_summary(){
			new_price = parseFloat( 0.00 );
			$( '.yith-pawcp-client-addon-text-price-figure' ).each(
				function() {
					new_price += parseFloat( $( this ).text().replace( /\s/g, '' ) )
				}
			);
			$( '.yith-pawcp-client-addon-check-price' ).each(
				function () {
					if ( $( this ).parent().find( ":input" )[0].checked ) {
						new_price += parseFloat( $( this ).attr( 'price' ).replace( /\s/g, '' ) )
					}
				}
			);
			$( '.yith-pawcp-group-price-hidden-array' ).each(
				function () {
					var option_prices = JSON.parse( $( this ).attr( 'array' ) );
					var option_key    = 0;
					if ( $( this ).attr( 'type' ) == 'select' ) {
						option_key = $( this ).parent().find( ":selected" ).val();
					} else {
						option_key = $( this ).parent().find( ":checked" ).val();
					}
					var aux = parseFloat( option_prices[option_key] );
					if ($.isNumeric( aux )) {
						new_price += aux;
					}
				}
			);
			$( '.yith-pawcp-price-totals-addons-figure' ).text( new_price.toFixed( 2 ) )
			$( '.yith-pawcp-price-totals-total-figure' ).text( ( new_price + parseFloat( $( '.yith-pawcp-price-totals-base-figure' ).text() ) ).toFixed( 2 ) )
		}
	}
)
