jQuery(
	function($) {

		// Function to update index.
		function yith_update_addon_index() {
			$( '.yith-pawcp-list-addon' ).each(
				function() {
					$( '.yith-pawcp-single-addon' ).each(
						function (i) {
							$( this ).find( ':input' ).each(
								function(){
									$( this ).attr( 'name', $( this ).attr( 'name' ).replace( /\]\[\d+\]/g, "][" + i + "]" ) );
									$( this ).attr( 'id', $( this ).attr( 'id' ).replace( /\]\[\d+\]/g, "][" + i + "]" ) );
								}
							);
							$( this ).find( 'label' ).each(
								function(){
									$( this ).attr( 'for', $( this ).attr( 'for' ).replace( /\]\[\d+\]/g, "][" + i + "]" ) );
								}
							);
						}
					);
				}
			);
		}

		// Field type.
		function change_field_type(){
			// Get elements to hide
			var only_text         = $( this ).closest( '.yith-pawcp-single-addon-body' ).find( '.yith-pawcp-only-text' );
			var group_option_list = $( this ).closest( '.yith-pawcp-single-addon-body' ).find( '.yith-pawcp-group-option-list' );
			var bool_option       = $( this ).closest( '.yith-pawcp-single-addon-body' ).find( '.yith-pawcp-bool-option' );
			var single_price      = $( this ).closest( '.yith-pawcp-single-addon-body' ).find( '.yith-pawcp-single-price' );
			// Hide by default.
			$( only_text ).addClass( 'yith-pawcp-hidden-by-field' );
			$( group_option_list ).addClass( 'yith-pawcp-hidden-by-field' );
			$( bool_option ).addClass( 'yith-pawcp-hidden-by-field' );
			$( single_price ).removeClass( 'yith-pawcp-hidden-by-field' );
			// Get price setting of this add-on.
			var pricesetts = $( this ).closest( '.yith-pawcp-single-addon-body' ).find( '.yith-pawcp-price-settings' );
			// Show necesary fields.
			switch ( $( this ).find( ":selected" ).val() ) {
				case 'text':
				case 'text-area':
					$( only_text ).removeClass( 'yith-pawcp-hidden-by-field' );
					break;
				case 'select':
				case 'radio-group':
					$( group_option_list ).removeClass( 'yith-pawcp-hidden-by-field' );
					$( single_price ).addClass( 'yith-pawcp-hidden-by-field' );
					if ( $( pricesetts ).find( ":checked" ).val() == 'pricechar' ) {
						$( pricesetts ).find( '[value=pricefixed]' ).prop( 'checked', true );
					}
					break;
				case 'checkbox':
				case 'check-onoff':
					bool_option.removeClass( 'yith-pawcp-hidden-by-field' );
					if ( $( pricesetts ).find( ":checked" ).val() == 'pricechar' ) {
						$( pricesetts ).find( '[value=pricefixed]' ).prop( 'checked', true );
					}
					break;
				default:
			}
		}

		// Price options.
		function change_price_settings(){
			var not_free = $( this ).closest( '.yith-pawcp-single-addon-body' ).find( '.yith-pawcp-not-free' );
			$( not_free ).addClass( 'yith-pawcp-hidden-by-price' );
			switch ( $( this ).find( ":checked" ).val() ) {
				case 'free':
					break;
				case 'pricechar':
				case 'pricefixed':
					$( not_free ).removeClass( 'yith-pawcp-hidden-by-price' );
					break;
				default:
			}
		}

		// Button to add group option.
		function add_option() {
			var hidden = $( this ).parent().find( '.yith-pawcp-group-option-single.yith-pawcp-template-group-option' );
			$( hidden ).clone( true, true ).removeClass( 'yith-pawcp-template-group-option' ).insertBefore( hidden );
		}
		// Remove the option.
		function remove_option() {
			$( this ).parent().remove();
		}

		// Make add-ons sortable.
		function list_addon_sort_update($item, container, _super, event) {
			$( 'div.yith-pawcp-list-addon .yith-pawcp-single-addon' ).removeClass( 'dragged' );
			$( 'div.yith-pawcp-list-addon .yith-pawcp-single-addon' ).removeAttr( 'style' );
			$( "body" ).removeClass( 'dragging' );
			yith_update_addon_index();
		};
		// Button to add add-on.
		function add_new_addon() {
			var hidden = $( this ).parent().find( '.yith-pawcp-single-addon.yith-pawcp-hidden-template-tab' );
			var cloned = $( hidden ).clone( true, true ).removeClass( 'yith-pawcp-hidden-template-tab' ).insertBefore( hidden );
			init_addon( cloned );
			yith_update_addon_index();
			$(hidden).parent().parent().find('.yith-pawcp-add-new-addon-bottom').removeClass('yith-pawcp-hidden');
		};

		// Remove the add-on.
		function remove_addon() {
			var remove = $( this ).parent().parent().parent();
			var list = $( remove ).parent();
			remove.remove();
			yith_update_addon_index();
			if ($(list).find('.yith-pawcp-single-addon').length <= 1 ){
				$(list).parent().find('.yith-pawcp-add-new-addon-bottom').addClass('yith-pawcp-hidden');
			}
		};

		// Show or hide add-on options.
		function toggle_addon_body(e) {
			var icon = $( this ).find( '.yith-pawcp-single-addon-header-arrow' );
			var body = $( this ).parent().parent().find( '.yith-pawcp-single-addon-body' );
			if ($( icon ).hasClass( 'fa-chevron-up' )) {
				$( icon ).removeClass( 'fa-chevron-up' );
				$( icon ).addClass( 'fa-chevron-down' );
			} else {
				$( icon ).removeClass( 'fa-chevron-down' );
				$( icon ).addClass( 'fa-chevron-up' );
			}
			$( body ).slideToggle();
			e.stopPropagation();
		};

		// On off fields change.
		function onoff_change() {
			var icon = $( this ).parent().find( ".yith-pawcp-input-check-onoff-icon" ).first();
			if ( this.checked ) {
				$( icon ).removeClass( "fa-toggle-off" );
				$( icon ).addClass( "fa-toggle-on" );
			} else {
				$( icon ).removeClass( "fa-toggle-on" );
				$( icon ).addClass( "fa-toggle-off" );
			}
		}

		// Name field change
		function name_change() {
			var titleDOM = $( this ).closest( '.yith-pawcp-single-addon' ).find( '.yith-pawcp-single-addon-header-name' );
			var title    = $( this ).find( ':input' ).val();
			if ( ! title || 0 === title.length || /^\s*$/.test( title )) {
				titleDOM.text( js_strings.untitled );
			} else {
				titleDOM.text( title );
			}
		}

		// Eventos.

		$( 'div.yith-pawcp-list-addon' ).ready( list_init );
		$( document ).on(
			'woocommerce_variations_loaded',
			function(){
				$( 'div.yith-pawcp-list-addon' ).ready( list_init );
			}
		);

		function list_init () {
			$( '.yith-pawcp-list-addon' ).sortable( { update: list_addon_sort_update } );
			$( "div.yith-pawcp-input.yith-pawcp-field-settings" ).on( 'change',  change_field_type );
			$( "div.yith-pawcp-input.yith-pawcp-price-settings" ).on( 'change', change_price_settings );
			$( '.yith-pawcp-add-option' ).on( 'click',  add_option );
			$( '.yith-pawcp-remove-group-option' ).on( 'click',  remove_option );
			$( '.yith-pawcp-add-new-addon' ).on( 'click',  add_new_addon );
			$( '.yith-pawcp-remove-addon' ).on( 'click',  remove_addon );
			$( '.yith-pawcp-single-addon-header-action' ).on( 'mouseup',  toggle_addon_body );
			$( ".yith-pawcp-input-check-onoff-field" ).on( 'change', onoff_change );
			$( ".yith-pawcp-input-title-name-field" ).on( 'keyup', name_change );

			$( '.yith-pawcp-single-addon' ).each( function() { init_addon( $( this ) ) } );
		}

		function init_addon( addon ){
			//General
			$( addon ).find( '.yith-pawcp-single-addon-header-action' ).trigger( 'mouseup' );
			$( addon ).find( ".yith-pawcp-input-check-onoff-field" ).trigger( 'change' );
			//General fields
			$( addon ).find( "div.yith-pawcp-input.yith-pawcp-field-settings" ).trigger( 'change' );
			$( addon ).find( "div.yith-pawcp-input.yith-pawcp-price-settings" ).trigger( 'change' );
			yith_update_addon_index()
		}
	}
);
