<?php
/**
 * Plugin Name: JNRP_3.2-2 Product Addons for Woocommerce Plugin
 * Description:
 * Version: 1.0.0
 * Author: Javier Nicolás Rodríguez Pérez.
 * Author URI: https://yithemes.com/
 * Text Domain: yith-pawcp-text-domain
 *
 * @package yith_formacion
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Before all, check if defined ABSPATH.
}

/*Create some constant where defined PATH for Style, Assets, Templates, Views */
if ( ! defined( 'YITH_PAWCP_VERSION' ) ) {
	define( 'YITH_PAWCP_VERSION', '1.0.0' );
}
if ( ! defined( 'YITH_PAWCP_DIR_URL' ) ) {
	define( 'YITH_PAWCP_DIR_URL', plugin_dir_url( __FILE__ ) );
}
if ( ! defined( 'YITH_PAWCP_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PAWCP_DIR_ASSETS_URL', YITH_PAWCP_DIR_URL . 'assets' );
}
if ( ! defined( 'YITH_PAWCP_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PAWCP_DIR_ASSETS_CSS_URL', YITH_PAWCP_DIR_ASSETS_URL . '/css' );
}
if ( ! defined( 'YITH_PAWCP_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PAWCP_DIR_ASSETS_JS_URL', YITH_PAWCP_DIR_ASSETS_URL . '/js' );
}
if ( ! defined( 'YITH_PAWCP_DIR_PATH' ) ) {
	define( 'YITH_PAWCP_DIR_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'YITH_PAWCP_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PAWCP_DIR_INCLUDES_PATH', YITH_PAWCP_DIR_PATH . 'includes' );
}
if ( ! defined( 'YITH_PAWCP_DIR_LANGUAGES_PATH' ) ) {
	define( 'YITH_PAWCP_DIR_LANGUAGES_PATH', YITH_PAWCP_DIR_PATH . 'languages' );
}
if ( ! defined( 'YITH_PAWCP_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PAWCP_DIR_TEMPLATES_PATH', YITH_PAWCP_DIR_PATH . 'templates' );
}
if ( ! defined( 'YITH_PAWCP_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PAWCP_DIR_VIEWS_PATH', YITH_PAWCP_DIR_PATH . 'views' );
}


if ( ! function_exists( 'yith_pawcp_init_classes' ) ) {
	/**
	 * Include the scripts
	 */
	function yith_pawcp_init_classes() {
		load_plugin_textdomain( 'yith-pawcp-text-domain', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PAWCP_DIR_INCLUDES_PATH . '/class-yith-pawcp-init.php';

		if ( class_exists( 'YITH_PAWCP_INIT' ) ) {
			/*
			*	Call the main function
			*/
			yith_pawcp_init();
		}
	}
}
add_action( 'yith_pawcp_init_hook', 'yith_pawcp_init_classes', 11 );

if ( ! function_exists( 'yith_pawcp_install_admin_notice' ) ) {
	/**
	 * Print an admin notice if WooCommerce is deactivated
	 *
	 * @author Carlos Rodriguez <carlos.rodriguez@yourinspiration.it>
	 * @since 1.0
	 * @return void
	 * @use admin_notices hooks
	 */
	function yith_pawcp_install_admin_notice() { ?>
		<div class="error">
			<p><?php echo esc_html_x( 'YITH Product Add-ons for Woocommerce is enabled but not effective. It requires WooCommerce in order to work.', 'Alert Message: WooCommerce requires', 'yith-pawcp-text-domain' ); ?></p>
		</div>
		<?php
	}
}

if ( ! function_exists( 'yith_wcgpf_install' ) ) {
	/**
	 * Check if WooCommerce is activated
	 *
	 * @author Carlos Rodriguez <carlos.rodriguez@yourinspiration.it>
	 * @since 1.0
	 * @return void
	 * @use admin_notices hooks
	 */
	function yith_wcgpf_install() {
		if ( ! function_exists( 'WC' ) ) {
			add_action( 'admin_notices', 'yith_pawcp_install_admin_notice' );
		} else {
			do_action( 'yith_pawcp_init_hook' );
		}
	}
}
add_action( 'plugins_loaded', 'yith_wcgpf_install', 11 );
