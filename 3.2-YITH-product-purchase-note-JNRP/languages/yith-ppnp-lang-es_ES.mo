��            )         �  �   �     O     f     �     �  
   �     �     �  
   �  
   �     �     �     �        
                  *  (   9     b     p  
   w  !   �     �     �  	   �  M   �       	          �  !  {   �     V  !   q     �     �     �     �     �     �               "     )     >     C     U     \     p     �     �     �     �  ,   �     �     �     �  K   	     S	     i	     }	                                                                  
                                           	                           Alert Message: WooCommerce requiresYITH Product Purchase Note Plugin for WooCommerce is enabled but not effective. It requires WooCommerce in order to work. Badge background color Badge position in product Badge position in shop Badge settings Badge text Badge text color Border Error Name Field type Fixed price Free Free characters Note Note price Price Price per character Price settings Product purchase note plugin description Purchase Note Radius Show badge Sorry, your nonce did not verify. Style Text Text area There's a problem in the pricing, if it persist consult with Customer service Top left Top right Weight Project-Id-Version: JNRP_3.2 Product Purchase Note Plugin 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/3.2-YITH-product-purchase-note-JNRP
PO-Revision-Date: 2021-02-10 14:08+0000
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Domain: yith-ppnp-text-domain
Plural-Forms: nplurals=2; plural=(n != 1);
 YITH Product Purchase Note Plugin para WooCommerce esta habilitado pero no efectivo. Se rquiere WooCommerce para funcionar. Color de fondo de la placa Posición de la placa en producto Posición de la placa en tienda Ajustes de badge Texto de la placa Color de texto de la placa Borde Error al nombrar Tipo de campo Precio fijo Gratis Caracteres gratuitos Nota Precio de la nota Precio Precio por caracter Ajustes de precios Descripción del plugin Nota de Compra Radio Mostrar placa Lo sentimos, su nonce no ha sido verificado. Estilo Texto Área de texto Hay un problema en el precio, si persiste consulte con Atención al Cliente Arriba a la izquierda Arriba a la derecha Peso 