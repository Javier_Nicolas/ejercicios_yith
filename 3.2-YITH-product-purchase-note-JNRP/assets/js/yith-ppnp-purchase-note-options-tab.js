jQuery(
	function($) {
		//General.
		var yith_ppnp_note_check             = $( ".yith_ppnp_enable_check" ).find( ":input" );
		var yith_ppnp_note_check_enable_list = $( ".yith_ppnp_hide_on_disable" ).closest( ".yith-pnpp-input" );;
		yith_ppnp_note_check.change( function (event) {yith_ppnp_show_note_options();} );
		//Badge.
		var yith_ppnp_badge_check             = $( ".yith_ppnp_enable_badge_check" ).find( ":input" );
		var yith_ppnp_badge_check_enable_list = $( ".yith_ppnp_show_if_badge" ).closest( ".yith-pnpp-input" );;
		yith_ppnp_badge_check.change( function (event) {yith_ppnp_show_note_options();} );
		//Price options.
		var yith_ppnp_price_radius             = $( ".yith_ppnp_price_settings" );
		var yith_ppnp_price_radius_enable_list = $( ".yith_ppnp_not_free" ).closest( ".yith-pnpp-input" );
		yith_ppnp_price_radius.change( function (event) {yith_ppnp_show_note_options();} );

		function yith_ppnp_show_note_options(){
			//General.
			if ( yith_ppnp_note_check[0].checked ) {
				yith_ppnp_note_check_enable_list.show();
				//Badge.
				if ( ! yith_ppnp_badge_check[0].checked ) {
					yith_ppnp_badge_check_enable_list.hide();
				}
				//Price options.
				if ( yith_ppnp_price_radius.find( ":checked" ).val() == 'free' ) {
					yith_ppnp_price_radius_enable_list.hide();
				}
			} else {
				yith_ppnp_note_check_enable_list.hide();
			}
		}
		yith_ppnp_show_note_options();
	}
);
