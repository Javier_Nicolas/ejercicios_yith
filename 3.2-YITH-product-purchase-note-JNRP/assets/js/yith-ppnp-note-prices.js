jQuery(
	function($) {
		var prueba = $( ".yith-ppnp-note-input" );
		$( ".yith-ppnp-note-input" ).keyup(
			function( event ) {
				var fixed    = $( this ).closest( "form" ).find( ".yith-ppnp-note-price-fixed" );
				var per_char = $( this ).closest( "form" ).find( ".yith-ppnp-note-price-sum-per-char" );
				if (fixed.length > 0 && parseInt( fixed.attr( 'free_chars' ) ) > 0) {
					var price = 0.00;
					if ($( this ).val().replace( /\s/g, '' ).length > parseInt( fixed.attr( 'free_chars' ) )) {
						price = parseFloat( fixed.attr( 'price' ) );
					}
					fixed.text( price.toFixed( 2 ) );
				} else if (per_char.length > 0) {
					var price = ($( this ).val().replace( /\s/g, '' ).length - parseInt( per_char.attr( 'free_chars' ) )) * parseFloat( per_char.attr( 'price' ) );
					if (price < 0 ) {
						price = 0.00;
					}
					per_char.text( price.toFixed( 2 ) );
				}
			}
		);
	}
);
