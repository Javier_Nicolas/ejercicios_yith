<?php
/**
 * This file belongs to the Product Purchase Note Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_PPNP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPNP_ADMIN' ) ) {

	/**
	 * Admin related.
	 */
	class YITH_PPNP_ADMIN {

		/**
		 * Main Instance
		 *
		 * @var YITH_PPNP_ADMIN
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/** The panel
		 *
		 * @var YIT_Plugin_Panel $panel .
		 */
		private $panel;

		/** The page
		 *
		 * @var Panel $pannel_page .
		 */
		protected $panel_page = 'yith_ppnp_panel';

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PPNP_ADMIN Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PPNP_ADMIN constructor.
		 */
		private function __construct() {
			// TABS.
			// Create the custom tab.
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_ppnp_tab_create' ) );
			// Add the custom fields.
			add_action( 'woocommerce_product_data_panels', array( $this, 'yith_ppnp_tab_display' ) );
			// Save the custom fields.
			add_action( 'woocommerce_admin_process_product_object', array( $this, 'yith_ppnp_tab_save' ) );
			add_action( 'woocommerce_admin_process_variation_object', array( $this, 'yith_ppnp_tab_save' ) );

			// Admin menu.
			add_action( 'admin_menu', array( $this, 'yith_ppnp_menus' ), 5 );
			add_action( 'yith_ppnp_print_custom_field', array( $this, 'yith_ppnp_custom_field' ), 10, 1 );

			// Framework.
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_PPNP_DIR_PATH . '/' . basename( YITH_PPNP_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_PPNP_SLUG );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_PPNP_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_PPNP_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}

		/**
		 *  Create menus for testimonials
		 */
		public function yith_ppnp_menus() {
			if ( ! empty( $this->panel ) ) {
				return;
			}

			$admin_tabs = array(
				'badge' => __( 'Badge settings', 'yith-ppnp-text-domain' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH PPNP Plugin', // this text MUST be NOT translatable
				'menu_title'         => 'YITH PPNP Plugin', // this text MUST be NOT translatable
				'plugin_description' => __( 'Product purchase note plugin description', 'yith-ppnp-text-domain' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => YITH_PPNP_SLUG,
				'parent_page'        => 'yith_plugin_panel',
				'page'               => YITH_PPNP_SLUG . '_panel',
				'admin-tabs'         => $admin_tabs,
				'plugin-url'         => YITH_PPNP_DIR_PATH,
				'options-path'       => YITH_PPNP_DIR_PATH . 'plugin-options',
			);

			$this->panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}

		/**
		 * Print custom field.
		 */
		public function yith_ppnp_custom_field( $field ) {
			// TODO: Labels arent showing.
			// TODO: Reset defaults isnt working.
			// if( 'yith_ppnp_options_border' === $field['name'] ) {
			$weight = array(
				'id'      => $field['id'] . '_weight',
				'name'    => $field['name'] . '[weight]',
				'title'   => esc_html__( 'Weight', 'yith-ppnp-text-domain' ),
				'type'    => 'number',
				'min'     => 0,
				'step'    => 1,
				'value'   => isset( $field['value']['weight'] ) ? $field['value']['weight'] : '1',
				'default' => '1',
			);
			$style  = array(
				'id'      => $field['id'] . '_style',
				'name'    => $field['name'] . '[style]',
				'title'   => esc_html__( 'Style', 'yith-ppnp-text-domain' ),
				'type'    => 'select',
				'class'   => 'wc-enhanced-select',
				'options' => array(
					'hidden' => esc_html__( 'Hidden', 'yith-plugin-testimonial' ),
					'none'   => esc_html__( 'None', 'yith-plugin-testimonial' ),
					'dashed' => esc_html__( 'Dashed', 'yith-plugin-testimonial' ),
					'dotted' => esc_html__( 'Dotted', 'yith-plugin-testimonial' ),
					'double' => esc_html__( 'Double', 'yith-plugin-testimonial' ),
					'solid'  => esc_html__( 'Solid', 'yith-plugin-testimonial' ),
				),
				'value'   => isset( $field['value']['style'] ) ? $field['value']['style'] : 'hidden',
				'default' => 'hidden',
			);
			$color  = array(
				'id'            => $field['id'] . '_color',
				'name'          => $field['name'] . '[color]',
				'title'         => esc_html__( 'Color', 'yith-ppnp-text-domain' ),
				'type'          => 'colorpicker',
				'alpha_enabled' => false,
				'value'         => isset( $field['value']['color'] ) ? $field['value']['color'] : '',
				'default'       => '#d8d8d8',
			);
			$radius = array(
				'id'      => $field['id'] . '_radius',
				'name'    => $field['name'] . '[radius]',
				'title'   => esc_html__( 'Radius', 'yith-ppnp-text-domain' ),
				'type'    => 'number',
				'min'     => 0,
				'step'    => 1,
				'default' => '7',
				'value'   => isset( $field['value']['radius'] ) ? $field['value']['radius'] : '7',
			);

			?>
			<div class="yith-plugin-ui yith-ppnp-border-field-container" >
				<?php
				yith_plugin_fw_get_field( $weight, true );
				yith_plugin_fw_get_field( $style, true );
				yith_plugin_fw_get_field( $color, true );
				yith_plugin_fw_get_field( $radius, true );
				?>
			</div>
			<?php
			// }
		}

		/**
		 *  Tabs display
		 */
		public function yith_ppnp_tab_display() {
			global $post;
			$tabs = array(
				'purchase-note' => 'yith_ppnp_panel',
			);

			$post_id = get_the_ID();
			$product = wc_get_product( $post_id );
			$data    = $product->get_meta( 'yith_ppnp_data' );
			if ( empty( $data ) ) {
				$data = array();
			}

			echo '<div id="yith_ppnp_panel" class="panel woocommerce_options_panel">';
			yith_ppnp_get_template( '/admin/product-tabs/purchase-note-tab.php', array( 'data' => $data ) );
			echo '</div>';
			global  $woocommerce;
			$post_id = get_the_ID();
			$product = wc_get_product( $post_id );
			$data    = $product->get_meta( 'yith_ppnp_data' );
			$options = get_option( 'yith_ppnp', array() );
		}

		/**
		 * Add the new tab to the $tabs array.
		 *
		 * @param array $tabs .
		 */
		public function yith_ppnp_tab_create( $tabs ) {
			$tabs['yith_ppnp_panel'] = array(
				'label'    => __( 'Purchase Note', 'yith-ppnp-text-domain' ),
				'target'   => 'yith_ppnp_panel',
				'class'    => array( 'ppnp_tab', 'show_if_simple', 'show_if_variable' ),
				'priority' => 80,
			);
			return $tabs;
		}

		/**
		 * Save the plugin data.
		 *
		 * @param array $product .
		 */
		public function yith_ppnp_tab_save( $product ) {
			// Nonce verification.
			if ( isset( $_POST['yith_ppnp_nonce'] ) ) {
				if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['yith_ppnp_nonce'] ) ), 'yith_ppnp_data' ) ) {
					print esc_html__( 'Sorry, your nonce did not verify.', 'yith-ppnp-text-domain' );
					exit;
				}
			} else {
				return;
			}
			// Unslash (? Sanitize).
			$data           = isset( $_POST['yith_ppnp'] ) ? (array) wp_unslash( $_POST['yith_ppnp'] ) : array();
			$data['enable'] = isset( $data['enable'] );
			if ( ! $data['enable'] ) {
				$data = array( 'enable' => 0 );
			} else {
				$data['enable_badge'] = isset( $data['enable_badge'] );
				if ( ! $data['enable_badge'] ) {
					unset( $data['badge_text'] );
					unset( $data['badge_background_color'] );
					unset( $data['badge_text_color'] );
				}
				if ( 'free' === $data['price_settings'] ) {
					unset( $data['price'] );
					unset( $data['free_chars'] );
				}
			}
			$product->update_meta_data( 'yith_ppnp_data', $data );
		}
	}
}

/** Admin CSS & JS */

if ( ! function_exists( 'yith_ppnp_load_tabs_css' ) ) {
	/** Purchase note tab css. */
	function yith_ppnp_load_tabs_css() {
		$screen     = get_current_screen();
		$is_product = 'product' === $screen->id;
		if ( $is_product ) {
			wp_register_style(
				'yith-ppnp-load-tabs-css',
				YITH_PPNP_DIR_ASSETS_CSS_URL . '/yith-ppnp-tabs.css',
				array( 'yith-plugin-fw-fields' ),
				false
			);
			wp_enqueue_style( 'yith-ppnp-load-tabs-css' );
			wp_register_script(
				'yith-ppnp-load-tabs-js',
				YITH_PPNP_DIR_ASSETS_JS_URL . '/yith-ppnp-purchase-note-options-tab.js',
				array( 'jquery', 'yith-plugin-fw-fields' ),
				false,
				true
			);
			wp_enqueue_script( 'yith-ppnp-load-tabs-js' );
		}
	}
}
add_action( 'admin_enqueue_scripts', 'yith_ppnp_load_tabs_css' );


