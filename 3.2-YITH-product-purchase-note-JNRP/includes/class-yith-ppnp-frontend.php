<?php
/**
 * This file belongs to the Product Purchase Note Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_PPNP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PPNP_Frontend' ) ) {

	/**
	 * Frontend related.
	 */
	class YITH_PPNP_Frontend {

		/**
		 * Main Instance
		 *
		 * @var YITH_PPNP_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PPNP_Frontend Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PPNP_Frotend constructor.
		 */
		private function __construct() {
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'add_cart_data', 10, 3 ) );
			add_filter( 'woocommerce_get_item_data', array( $this, 'get_cart_data', 10, 3 ) );
			add_action( 'woocommerce_add_order_item_meta', array( $this, 'cart_data_to_order', 10, 3 ) );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'calculate_price', 10, 1 ) );

			add_action( 'woocommerce_before_add_to_cart_quantity', array( $this, 'purchase_note_form_display' ) );
			add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'badge_shop_display' ) );
			add_action( 'woocommerce_product_thumbnails', array( $this, 'badge_product_display' ) );
		}

		/**
		 * Save purchase note info at adding to cart.
		 *
		 * @param array $cart_item_data .
		 * @param int   $product_id .
		 * @param int   $variation_id .
		 */
		public function add_cart_data( $cart_item_data, $product_id, $variation_id ) {
			// Nonce verification.
			if ( isset( $_POST['yith_ppnp_note_nonce'] ) ) {
				if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['yith_ppnp_note_nonce'] ) ), 'yith_ppnp_note_save_meta' ) ) {
					print esc_html__( 'Sorry, your nonce did not verify.', 'yith-ppnp-text-domain' );
					exit;
				}
			} else {
				return;
			}
			if ( empty( $_POST['yith_ppnp_note_input'] ) ) {
				return;
			}
			$post_id    = $product_id;
			$price      = 0;
			$text_input = sanitize_text_field( wp_unslash( $_POST['yith_ppnp_note_input'] ) );
			switch ( get_post_meta( $post_id, '_yith_ppnp_price_settings', true ) ) {
				case 'free':
					break;
				case 'pricefixed':
					if ( strlen( preg_replace( '/\s+/', '', $text_input ) ) > intval( get_post_meta( $post_id, '_yith_ppnp_free_chars', true ) ) ) {
						$price = get_post_meta( $post_id, '_yith_ppnp_price', true );
					}
					break;
				case 'pricechar':
					$price = floatval( get_post_meta( $post_id, '_yith_ppnp_price', true ) )
					* ( strlen( preg_replace( '/\s+/', '', $text_input ) ) - floatval( get_post_meta( $post_id, '_yith_ppnp_free_chars', true ) ) );
					break;
				default:
					return;
			}
			// $key   = ''; // Define your key here
			// $value = $_POST['key_name']; // Get your value here
			$cart_item_data['yith_ppnp_note_text']  = $text_input;
			$cart_item_data['yith_ppnp_note_price'] = $price;

			return $cart_item_data;
		}

		/**
		 * Show note info in cart.
		 *
		 * @param int   $data .
		 * @param array $cart_item .
		 */
		public function get_cart_data( $data, $cart_item ) {
			if ( isset( $cart_item['yith_ppnp_note_text'] ) ) {
				$data[] = array(
					'name'  => __( 'Note', 'yith-ppnp-text-domain' ),
					'value' => $cart_item['yith_ppnp_note_text'],
				);
			}
			return $data;
		}

		/**
		 * Save purchase note info as meta.
		 *
		 * @param int    $item_id .
		 * @param array  $values .
		 * @param string $key .
		 */
		public function yith_ppnp_cart_data_to_order( $item_id, $values, $key ) {
			if ( isset( $values['yith_ppnp_note_text'] ) ) {
				wc_add_order_item_meta( $item_id, 'yith_ppnp_note_text', $values['yith_ppnp_note_text'] );
			}
			if ( isset( $values['yith_ppnp_note_price'] ) ) {
				wc_add_order_item_meta( $item_id, 'yith_ppnp_note_price', $values['yith_ppnp_note_price'] );
			}
		}

		/**
		 * Modify the item price.
		 *
		 * @param array $cart .
		 */
		public function yith_ppnp_calculate_price( $cart ) {
			// Required since Woocommerce version 3.2 for cart items properties changes.
			if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 ) {
				return;
			}
			// Loop through cart items.
			foreach ( $cart->get_cart() as $cart_item ) {
				if ( isset( $cart_item['yith_ppnp_note_price'] ) ) {
					$new_price = $cart_item['data']->get_price() + $cart_item['yith_ppnp_note_price'];
					$cart_item['data']->set_price( $new_price );
				}
			}
		}

		/**
		 * Purchase note form display for products.
		 */
		public function purchase_note_form_display() {
			global  $woocommerce;
			$post_id = get_the_ID();
			$product = wc_get_product( $post_id );
			$data    = $product->get_meta( 'yith_ppnp_data' );
			if ( ! empty( $data['enable'] ) && $data['enable'] ) {
					echo esc_html(
						yith_ppnp_get_template(
							'/metaboxes/field-row.php',
							array(
								'field' => array(
									'id'    => 'yith_ppnp_note_input',
									'name'  => 'yith_ppnp_note_input',
									'title' => $data['note_label'],
									'type'  => $data['field_type'],
									'class' => 'yith-ppnp-note-input',
									'desc'  => $data['note_description'],
								),
							)
						)
					);
					echo '<p class="yith-ppnp-note-pricing">';
				switch ( $data['price_settings'] ) {
					case 'free':
						esc_html_e( 'Free', 'yith-ppnp-text-domain' );
						break;
					case 'pricefixed':
						$price      = floatval( $data['price'] );
						$free_chars = intval( $data['free_chars'] );
						?>
						+ <span
							class="yith-ppnp-note-price-fixed"
							price="<?php echo esc_html( $price ); ?>"
							free_chars="<?php echo esc_html( $free_chars ); ?>"
							>
								<?php
								if ( 0 >= $free_chars ) {
									echo esc_html( $price );
								} else {
									echo '0.00';
								}
								?>
							</span>
						<?php
						echo esc_html( get_woocommerce_currency_symbol() );
						break;
					case 'pricechar':
						?>
						+ <span
							class="yith-ppnp-note-price-sum-per-char"
							price="<?php echo esc_html( $data['price'] ); ?>"
							free_chars="<?php echo esc_html( $data['free_chars'] ); ?>"
							>
								0.00
							</span>
						<?php
						echo esc_html( get_woocommerce_currency_symbol() );
						break;
					default:
						esc_html_e(
							'There\'s a problem in the pricing, if it persist consult with Customer service',
							'yith-ppnp-text-domain'
						);
				}
				echo '</p>';
				wp_nonce_field( 'yith_ppnp_note_save_meta', 'yith_ppnp_note_nonce' );
				?>
				</div>
				<?php
			}
		}

		/**
		 * Badge for purchase note display at shop.
		 */
		public function badge_shop_display() {
			$this::badge_display( 'shop_pos' );
		}

		/**
		 * Badge for purchase note display at product.
		 */
		public function badge_product_display() {
			$this::badge_display( 'product_pos' );
		}

		/**
		 * Badge to display.
		 *
		 * @param string $option If shop or product.
		 */
		public function badge_display( string $option ) {
			global  $woocommerce;
			$post_id = get_the_ID();
			$product = wc_get_product( $post_id );
			$data    = $product->get_meta( 'yith_ppnp_data' );
			$options = get_option( 'yith_ppnp', array() );

			if ( ! empty( $data['enable'] ) && $data['enable'] && ! empty( $data['enable_badge'] ) && $data['enable_badge'] ) {
				// Colors.
				$text_color = ! empty( $data['badge_text_color'] ) ? $data['badge_text_color'] : '#ffffff';
				$back_color = ! empty( $data['badge_background_color'] ) ? $data['badge_background_color'] : '#007694';
				// Padding.
				$padding = '';
				$unit    = ! empty( $options['padding']['unit'] ) ? $options['padding']['unit'] : 'px';
				foreach ( $options['padding']['dimensions'] as $dimension ) {
					$padding .= "$dimension$unit ";
				}
				// Position.
				$position = ! empty( $options[ $option ] ) ? $options[ $option ] : 'right';
				// Border.
				$weight        = ! empty( $options['weight'] ) ? $options['weight'] : '1';
				$radius        = ! empty( $options['radius'] ) ? $options['radius'] : '7';
				$border_style  = ! empty( $options['style'] ) ? $options['style'] : 'solid';
				$border_color  = ! empty( $options['color'] ) ? $options['color'] : '#d8d8d8';
				$px            = 'px';
				$inline_styles = ".yith-ppnp-badge-container.yith-ppnp-badge-product-$post_id{
					color: $text_color;
					background-color: $back_color;
					padding: $padding;
					$position:0px;
					border-width: $weight$px;
					border-radius: $radius$px;
					border-style: $border_style;
					border-color: $border_color;
				}";
				yith_ppnp_load_badge_css( $inline_styles );
				?>
				<div class="yith-ppnp-badge-container yith-ppnp-badge-product-<?php echo esc_html( $post_id ); ?>">
					<span class="yith-ppnp-badge-text">
						<?php echo esc_html( $data['badge_text'] ); ?>
					</span>
				</div>
				<?php
			}
		}
	}
}

if ( ! function_exists( 'yith_ppnp_load_badge_css' ) ) {
	/**
	 * Purchase note tab css.
	 *
	 * @param string $inline_styles CSS to add dynamically.
	 */
	function yith_ppnp_load_badge_css( $inline_styles = null ) {
		wp_register_style(
			'yith-ppnp-load-badge-css',
			YITH_PPNP_DIR_ASSETS_CSS_URL . '/yith-ppnp-badge.css',
			array(),
			false
		);
		wp_enqueue_style( 'yith-ppnp-load-badge-css' );
		if ( isset( $inline_styles ) ) {
			wp_add_inline_style( 'yith-ppnp-load-badge-css', $inline_styles );
		}
		wp_enqueue_script( 'yith-ppnp-load-jquery' );
		wp_register_script(
			'yith-ppnp-load-note-prices',
			YITH_PPNP_DIR_ASSETS_JS_URL . '/yith-ppnp-note-prices.js',
			array( 'jquery' ),
			false,
			true
		);
		wp_enqueue_script( 'yith-ppnp-load-note-prices' );
	}
}
// add_action( 'wp_enqueue_scripts', 'yith_ppnp_load_badge_css' );.
