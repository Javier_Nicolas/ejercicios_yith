<?php
/**
 * This file belongs to the Product Purchase Note Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

// HERE SET FUNCTIONS TO USE ON YOUR PROJECT LIKE Template functions.

if ( ! function_exists( 'yith_ppnp_get_template' ) ) {
	/**
	 * Include templates
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function yith_ppnp_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_PPNP_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_ppnp_get_view' ) ) {
	/**
	 * Include views
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function yith_ppnp_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_PPNP_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_ppnp_get_template' ) ) {
	/**
	 * Include templates
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function yith_ppnp_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_CR_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_ppnp_order_columns' ) ) {
	/** Adds columns to orders */
	function yith_ppnp_order_columns() {
		// Priority 20, with 1 parameter (the 1 here is optional).
		add_filter(
			'manage_edit-shop_order_columns',
			function ( $columns ) {
				$columns['yith_ppnp_note_text']  = __( 'Note', 'yith-ppnp-text-domain' );
				$columns['yith_ppnp_note_price'] = __( 'Note price', 'yith-ppnp-text-domain' );
				return $columns;
			},
			20,
			1
		);
		// Priority 20, with 2 parameters.
		add_action(
			'manage_shop_order_posts_custom_column',
			function ( $column_name, $post_id ) {
				$val = null;
				if ( 'yith_ppnp_note_text' === $column_name ) {
					$val = 'text';
				} elseif ( 'yith_ppnp_note_price' === $column_name ) {
					$val = 'price';
				}
				if ( isset( $val ) ) {
					echo esc_html( "<strong style='color:#f00;'> $val </strong>" );
				}

			},
			20,
			2
		);
		// Default priority, default parameters (zero or one).
		add_filter(
			'manage_edit-shop_order_sortable_columns',
			function ( $columns ) {
				$columns['yith_ppnp_note_text']  = 'yith_ppnp_note_text';
				$columns['yith_ppnp_note_price'] = 'yith_ppnp_note_price';
				return $columns;
			}
		);
	}
}
//add_action( 'admin_init', 'yith_ppnp_order_columns' );


if ( ! function_exists( 'yith_ppnp_show_meta_name' ) ) {
	/**
	 * Filter metadata key to show a name.
	 *
	 * @param string $display_key .
	 */
	function yith_ppnp_show_meta_name( $display_key ) {
		if ( strpos( $display_key, 'yith_ppnp' ) !== false ) {
			switch ( $display_key ) {
				case 'yith_ppnp_note_text':
					$display_key = __( 'Note', 'yith-ppnp-text-domain' );
					break;
				case 'yith_ppnp_note_price':
					$display_key = __( 'Note price', 'yith-ppnp-text-domain' );
					break;
				default:
					$display_key = __( 'Error Name', 'yith-ppnp-text-domain' );
			}
		}
		return $display_key;
	}
}
add_filter( 'woocommerce_order_item_display_meta_key', 'yith_ppnp_show_meta_name', 10, 3 );
