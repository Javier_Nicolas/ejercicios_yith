<?php
/**
 * Plugin Name: JNRP_3.2 Product Purchase Note Plugin
 * Description:
 * Version: 1.0.0
 * Author: Javier Nicolás Rodríguez Pérez.
 * Author URI: https://yithemes.com/
 * Text Domain: yith-ppnp-text-domain
 *
 * @var array $data Previous data.
 *
 * @package yith_formacion
 */

// TODO; add descriptions.
$fields = array(
	'yith_ppnp[enable]'                 => array(
		'id'    => 'yith_ppnp[enable]',
		'name'  => 'yith_ppnp[enable]',
		'title' => __( 'Enable', 'tyith-ppnp-text-domain' ),
		'type'  => 'onoff',
		'class' => 'yith_ppnp_enable_check',
		'value' => isset( $data['enable'] ) ? $data['enable'] : false,
	),
	'yith_ppnp[note_label]'             => array(
		'id'    => 'yith_ppnp[note_label]',
		'name'  => 'yith_ppnp[note_label]',
		'title' => __( 'Note label', 'tyith-ppnp-text-domain' ),
		'type'  => 'text',
		'class' => 'yith_ppnp_hide_on_disable',
		'value' => isset( $data['note_label'] ) ? $data['note_label'] : '',
	),
	'yith_ppnp[note_description]'       => array(
		'id'    => 'yith_ppnp[note_description]',
		'name'  => 'yith_ppnp[note_description]',
		'title' => __( 'Note description', 'tyith-ppnp-text-domain' ),
		'type'  => 'textarea',
		'class' => 'yith_ppnp_hide_on_disable',
		'value' => isset( $data['note_description'] ) ? $data['note_description'] : '',
	),
	'yith_ppnp[field_type]'             => array(
		'id'      => 'yith_ppnp[field_type]',
		'name'    => 'yith_ppnp[field_type]',
		'title'   => __( 'Field type', 'tyith-ppnp-text-domain' ),
		'type'    => 'radio',
		'class'   => 'yith_ppnp_hide_on_disable',
		'options' => array(
			'text'     => __( 'Text', 'yith-ppnp-text-domain' ),
			'textarea' => __( 'Text area', 'yith-ppnp-text-domain' ),
		),
		'value'   => isset( $data['field_type'] ) ? $data['field_type'] : 'text',
	),
	'yith_ppnp[price_settings]'         => array(
		'id'      => 'yith_ppnp[price_settings]',
		'name'    => 'yith_ppnp[price_settings]',
		'title'   => __( 'Field type', 'tyith-ppnp-text-domain' ),
		'type'    => 'radio',
		'class'   => 'yith_ppnp_hide_on_disable yith_ppnp_price_settings',
		'options' => array(
			'free'       => __( 'Free', 'yith-ppnp-text-domain' ),
			'pricefixed' => __( 'Fixed price', 'yith-ppnp-text-domain' ),
			'pricechar'  => __( 'Price per character', 'yith-ppnp-text-domain' ),
		),
		'value'   => isset( $data['price_settings'] ) ? $data['price_settings'] : 'free',
	),
	'yith_ppnp[price]'                  => array(
		'id'    => 'yith_ppnp[price]',
		'name'  => 'yith_ppnp[price]',
		'title' => __( 'Price', 'tyith-ppnp-text-domain' ),
		'type'  => 'number',
		'min'   => '0',
		'step'  => 'any',
		'class' => 'yith_ppnp_hide_on_disable yith_ppnp_not_free',
		'value' => isset( $data['price'] ) ? $data['price'] : '0',
	),
	'yith_ppnp[free_chars]'             => array(
		'id'    => 'yith_ppnp[free_chars]',
		'name'  => 'yith_ppnp[free_chars]',
		'title' => __( 'Free characters', 'tyith-ppnp-text-domain' ),
		'type'  => 'number',
		'min'   => '0',
		'step'  => '1',
		'class' => 'yith_ppnp_hide_on_disable yith_ppnp_not_free',
		'value' => isset( $data['free_chars'] ) ? $data['free_chars'] : '0',
	),
	'yith_ppnp[enable_badge]'           => array(
		'id'    => 'yith_ppnp[enable_badge]',
		'name'  => 'yith_ppnp[enable_badge]',
		'title' => __( 'Show badge', 'tyith-ppnp-text-domain' ),
		'type'  => 'onoff',
		'class' => 'yith_ppnp_hide_on_disable yith_ppnp_enable_badge_check',
		'value' => isset( $data['enable_badge'] ) ? $data['enable_badge'] : false,
	),
	'yith_ppnp[badge_text]'             => array(
		'id'    => 'yith_ppnp[badge_text]',
		'name'  => 'yith_ppnp[badge_text]',
		'title' => __( 'Badge text', 'tyith-ppnp-text-domain' ),
		'type'  => 'text',
		'class' => 'yith_ppnp_hide_on_disable yith_ppnp_show_if_badge',
		'value' => isset( $data['badge_text'] ) ? $data['badge_text'] : '',
	),
	'yith_ppnp[badge_background_color]' => array(
		'id'            => 'yith_ppnp[badge_background_color]',
		'name'          => 'yith_ppnp[badge_background_color]',
		'title'         => __( 'Badge background color', 'tyith-ppnp-text-domain' ),
		'type'          => 'colorpicker',
		'alpha_enabled' => false,
		'step'          => 'any',
		'class'         => 'yith-plugin-fw-colorpicker color-picker yith_ppnp_hide_on_disable yith_ppnp_show_if_badge',
		'default'       => '#007694',
		'value'         => isset( $data['badge_background_color'] ) ? $data['badge_background_color'] : '',
	),
	'yith_ppnp[badge_text_color]'       => array(
		'id'            => 'yith_ppnp[badge_text_color]',
		'name'          => 'yith_ppnp[badge_text_color]',
		'title'         => __( 'Badge text color', 'tyith-ppnp-text-domain' ),
		'type'          => 'colorpicker',
		'alpha_enabled' => false,
		'step'          => 'any',
		'class'         => 'yith-plugin-fw-colorpicker color-picker yith_ppnp_hide_on_disable yith_ppnp_show_if_badge',
		'default'       => '#ffffff',
		'value'         => isset( $data['badge_text_color'] ) ? $data['badge_text_color'] : '',
	),
);

foreach ( $fields as $field ) {
	yith_ppnp_get_template( '/metaboxes/field-row.php', array( 'field' => $field ) );
}
wp_nonce_field( 'yith_ppnp_data', 'yith_ppnp_nonce' );
