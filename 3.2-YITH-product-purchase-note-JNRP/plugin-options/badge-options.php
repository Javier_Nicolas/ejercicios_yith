<?php
/**
 * This file belongs to the Product Purchase Note Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

$settings = array(
	'badge' => array(
		'badge-options'          => array(
			'name' => __( 'Badge options', 'yith-test-plugin' ),
			'type' => 'title',
			'desc' => '',
		),
		'yith_ppnp[padding]'     => array(
			'id'           => 'yith_ppnp[padding]',
			'name'         => esc_html__( 'Padding', 'yith-ppnp-text-domain' ),
			'type'         => 'yith-field',
			'yith-type'    => 'dimensions',
			'default'      => array(
				'dimensions' => array(
					'top'    => 20,
					'right'  => 25,
					'bottom' => 25,
					'left'   => 25,
				),
				'linked'     => 'no',
			),
			'allow_linked' => true,
			'min'          => 0,
			'units'        => array( 'px' => 'px' ),
		),
		'yith_ppnp[border]'      => array(
			'id'        => 'yith_ppnp[border]',
			'name'      => __( 'Border', 'yith-ppnp-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'custom',
			'action'    => 'yith_ppnp_print_custom_field',
		),
		'yith_ppnp[shop_pos]'    => array(
			'id'        => 'yith_ppnp[shop_pos]',
			'name'      => esc_html__( 'Badge position in shop', 'yith-ppnp-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'options'   => array(
				'right' => esc_html__( 'Top right', 'yith-ppnp-text-domain' ),
				'left'  => esc_html__( 'Top left', 'yith-ppnp-text-domain' ),
			),
			'default'   => 'right',
		),
		'yith_ppnp[product_pos]' => array(
			'id'        => 'yith_ppnp[product_pos]',
			'name'      => esc_html__( 'Badge position in product', 'yith-ppnp-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'options'   => array(
				'right' => esc_html__( 'Top right', 'yith-ppnp-text-domain' ),
				'left'  => esc_html__( 'Top left', 'yith-ppnp-text-domain' ),
			),
			'default'   => 'right',
		),
		'badge-options-end'      => array(
			'type' => 'sectionend',
			'id'   => 'yith-wcbk-badge-options',
		),
	),
);
return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
