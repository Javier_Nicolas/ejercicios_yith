<?php
/**
 * This file belongs to the YITH CPB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_CPB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_CPB_Post_Types' ) ) {

	/**
	 * Custom post types.
	 */
	class YITH_CPB_Post_Types {

		/**
		 * Main Instance
		 *
		 * @var YITH_CPB_Post_Types
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_CPB_Post_Types
		 * @since 1.0
		 * @access public
		 */
		public static $post_type = 'yith-cpb-book';


		/**
		 * Main plugin Instance
		 *
		 * @return YITH_CPB_Post_Types Main instance
		 * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_CPB_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );
			add_action( 'init', array( $this, 'register_taxonomy' ) );

		}

		/**
		 * Setup the 'Book' custom post type.
		 */
		public function setup_post_type() {
			$args = array(
				'label'        => __( 'Book', 'yith-plugin-book' ),
				'description'  => __( 'Book post type', 'yith-plugin-book' ),
				'public'       => false,
				'menu_icon'    => 'dashicons-universal-access',
				'show_in_menu' => true,
				'show_ui'      => true,
				'rewrite'      => false,
				'supports'     => array( 'title', 'editor', 'author', 'thumbnail' ),
			);
			register_post_type( self::$post_type, $args );
		}

		/**
		 * Register taxonomy.
		 */
		public function register_taxonomy() {
			// Add new taxonomy, make it hierarchical (like categories).
			$labels = array(
				'name'              => _x( 'Hierarchical taxonomy', 'taxonomy general name', 'yith-plugin-book' ),
				'singular_name'     => _x( 'Hierarchical', 'taxonomy singular name', 'yith-plugin-book' ),
				'search_items'      => __( 'Search Hierarchical', 'yith-plugin-book' ),
				'all_items'         => __( 'All Hierarchical', 'yith-plugin-book' ),
				'parent_item'       => __( 'Parent Hierarchical', 'yith-plugin-book' ),
				'parent_item_colon' => __( 'Parent Hierarchical:', 'yith-plugin-book' ),
				'edit_item'         => __( 'Edit Hierarchical', 'yith-plugin-book' ),
				'update_item'       => __( 'Update Hierarchical', 'yith-plugin-book' ),
				'add_new_item'      => __( 'Add New Hierarchical', 'yith-plugin-book' ),
				'new_item_name'     => __( 'New Hierarchical Name', 'yith-plugin-book' ),
				'menu_name'         => __( 'Hierarchical', 'yith-plugin-book' ),
			);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_hierarchical' ),
			);

			register_taxonomy( 'yith_CPB_hietatchical_tax', array( self::$post_type ), $args );

			// Add new taxonomy, make it no hierarchical (like tags).

			$labels1 = array(
				'name'              => _x( 'No Hierarchical taxonomy', 'taxonomy general name', 'yith-plugin-book' ),
				'singular_name'     => _x( 'No Hierarchical', 'taxonomy singular name', 'yith-plugin-book' ),
				'search_items'      => __( 'Search No Hierarchical', 'yith-plugin-book' ),
				'all_items'         => __( 'All No Hierarchical', 'yith-plugin-book' ),
				'parent_item'       => __( 'Parent No Hierarchical', 'yith-plugin-book' ),
				'parent_item_colon' => __( 'Parent No Hierarchical:', 'yith-plugin-book' ),
				'edit_item'         => __( 'Edit No Hierarchical', 'yith-plugin-book' ),
				'update_item'       => __( 'Update No Hierarchical', 'yith-plugin-book' ),
				'add_new_item'      => __( 'Add New No Hierarchical', 'yith-plugin-book' ),
				'new_item_name'     => __( 'New No Hierarchical Name', 'yith-plugin-book' ),
				'menu_name'         => __( 'No Hierarchical', 'yith-plugin-book' ),
			);

			$args1 = array(
				'hierarchical'      => false,
				'labels'            => $labels1,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_no_hierarchical' ),
			);

			register_taxonomy( 'yith_CPB_no_hietatchical_tax', array( self::$post_type ), $args1 );
		}
	}
}
