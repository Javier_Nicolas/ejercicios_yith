<?php
/**
 * This file belongs to the YITH CPB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_CPB_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_CPB_Admin' ) ) {

	/**
	 * Admin related.
	 */
	class YITH_CPB_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_CPB_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_CPB_Admin Main instance
		 * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_CPB_Admin constructor.
		 */
		private function __construct() {
			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'save_meta_box' ) );

			// See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns .
			add_filter( 'manage_yith-cpb-book_posts_columns', array( $this, 'add_book_post_type_columns' ) );

			// See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column .
			add_action( 'manage_yith-cpb-book_posts_custom_column', array( $this, 'display_book_post_type_custom_column' ), 10, 2 );

			// Admin menu .

			add_action( 'admin_menu', array( $this, 'create_menu_for_general_options' ) );
			add_action( 'admin_init', array( $this, 'register_settings' ) );

		}


		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box(
				'yith-cpb-additional-information',
				__( 'Additional information', 'yith-plugin-book' ),
				array(
					$this,
					'view_meta_boxes',
				),
				YITH_CPB_Post_Types::$post_type
			);
		}

		/**
		 * Wiev meta boxes
		 *
		 * @param string $post Post.
		 */
		public function view_meta_boxes( $post ) {
			yith_cpb_get_view( '/metaboxes/plugin-book-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 * Save meta box values
		 *
		 * @param string $post_id ID.
		 */
		public function save_meta_box( $post_id ) {

			if ( get_post_type( $post_id ) !== YITH_CPB_Post_Types::$post_type ) {
				return;
			}

			if ( isset( $_POST['_yith_cpb_role'] ) ) { 										//phpcs:ignore
				update_post_meta( $post_id, '_yith_cpb_role', $_POST['_yith_cpb_role'] );	//phpcs:ignore
			}

		}
		/**
		 * Filters the columns displayed in the Posts list table for plugin book post type.
		 *
		 * @param string[] $post_columns An associative array of column headings.
		 */
		public function add_book_post_type_columns( $post_columns ) {

			$new_columns = apply_filters(
				'yith_cpb_book_custom_columns',
				array(
					'column1' => esc_html__( 'Column1', 'yith-plugin-book' ),
					'column2' => esc_html__( 'Column2', 'yith-plugin-book' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}
		/**
		 * Fires for each custom column of a specific post type in the Posts list table.
		 *
		 * @param string $column_name The name of the column to display.
		 * @param int    $post_id     The current post ID.
		 * */
		public function display_book_post_type_custom_column( $column_name, $post_id ) {

			switch ( $column_name ) {

				case 'column1':
						// Operations for get information for column1 example.

						// $value_custom_2 = get_post_meta($post_id,'_meta_column1',true);
						// echo $value_custom_1;

						esc_html( 'value column1 for post ' . $post_id );

					break;
				case 'column2':
						// Operations for get information for column2 example.

						// $value_custom_2 = get_post_meta($post_id,'_meta_column2',true);
						// echo $value_custom_2;

						esc_html( 'value column2 for post ' . $post_id );
					break;
				default:
					do_action( 'yith_cpb_book_display_custom_column', $column, $post_id );
					break;
			}

		}
		/**
		 *  Create menu for general options
		 */
		public function create_menu_for_general_options() {

			// See the following option https://developer.wordpress.org/reference/functions/add_menu_page/ .
				add_menu_page(
					esc_html__( 'Plugin Book Options', 'yith-plugin-book' ),
					esc_html__( 'Plugin Book Options', 'yith-plugin-book' ),
					'manage_options',
					'plugin_book_options',
					array( $this, 'book_custom_menu_page' ),
					'',
					40
				);

		}
		/**
		 *  Callback custom menu page
		 */
		public function book_custom_menu_page() {
			yith_cpb_get_view( '/admin/plugin-options-panel.php', array() );
		}

		/**
		 * Add the fields in the shortcode attribute management page
		 */
		public function register_settings() {

			$page_name    = 'cpb-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_cpb_shortcode_number',
					'title'    => esc_html__( 'Number', 'yith-plugin-book' ),
					'callback' => 'print_number_input',
				),
				array(
					'id'       => 'yith_cpb_shortcode_show_image',
					'title'    => esc_html__( 'Show image', 'yith-plugin-book' ),
					'callback' => 'print_show_image',
				),
			);

			add_settings_section(
				$section_name,
				esc_html__( 'Section', 'yith-plugin-book' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}

		}

		/**
		 * Print the number input field
		 */
		public function print_number_input() {
			$tst_number = intval( get_option( 'yith_cpb_shortcode_number', 6 ) );
			?>
			<input type="number" id="yith_cpb_shortcode_number" name="yith_cpb_shortcode_number"
				value="<?php esc_html( '' !== $tst_number ? $tst_number : 6 ); ?>">
			<?php
		}

		/**
		 * Print the show image toggle field
		 */
		public function print_show_image() {
			?>
			<input type="checkbox" class="cpb-tst-option-panel__onoff__input" name="yith_cpb_shortcode_show_image"
					value='yes'
					id="yith_cpb_shortcode_show_image"
				<?php checked( get_option( 'yith_cpb_shortcode_show_image', '' ), 'yes' ); ?>
			>
			<label for="shortcode_show_image" class="cpb-tst-option-panel__onoff__label ">
				<span class="cpb-tst-option-panel__onoff__btn"></span>
			</label>
			<?php
		}



	}
}
