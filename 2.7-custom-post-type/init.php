<?php
/**
 * Plugin Name: JNRP_2.7_Custom_Post_Book
 * Description: Exercise about adding a post type, book.
 * Version: 1.0.0
 * Author: Javier Nicolas Rodriguez Perez.
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-book
 *
 * @package yith_formacion
 */

! defined( 'ABSPATH' ) && exit; // Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */
if ( ! defined( 'YITH_CPB_VERSION' ) ) {
	define( 'YITH_CPB_VERSION', '1.0.0' );
}
if ( ! defined( 'YITH_CPB_DIR_URL' ) ) {
	define( 'YITH_CPB_DIR_URL', plugin_dir_url( __FILE__ ) );
}
if ( ! defined( 'YITH_CPB_DIR_ASSETS_URL' ) ) {
	define( 'YITH_CPB_DIR_ASSETS_URL', YITH_CPB_DIR_URL . 'assets' );
}
if ( ! defined( 'YITH_CPB_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_CPB_DIR_ASSETS_CSS_URL', YITH_CPB_DIR_ASSETS_URL . '/css' );
}
if ( ! defined( 'YITH_CPB_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_CPB_DIR_ASSETS_JS_URL', YITH_CPB_DIR_ASSETS_URL . '/js' );
}
if ( ! defined( 'YITH_CPB_DIR_PATH' ) ) {
	define( 'YITH_CPB_DIR_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'YITH_CPB_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_CPB_DIR_INCLUDES_PATH', YITH_CPB_DIR_PATH . '/includes' );
}
if ( ! defined( 'YITH_CPB_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_CPB_DIR_TEMPLATES_PATH', YITH_CPB_DIR_PATH . '/templates' );
}
if ( ! defined( 'YITH_CPB_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_CPB_DIR_VIEWS_PATH', YITH_CPB_DIR_PATH . 'views' );
}


if ( ! function_exists( 'yith_cpb_init_classes' ) ) {
	/**
	 * Include the scripts
	 */
	function yith_cpb_init_classes() {

		load_plugin_textdomain( 'yith-plugin-book', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_CPB_DIR_INCLUDES_PATH . '/class-yith-cpb-plugin-book.php';

		if ( class_exists( 'YITH_CPB_Plugin_Book' ) ) {
			/*
			*	Call the main function
			*/
			yith_cpb_plugin_book();
		}
	}
}
add_action( 'plugins_loaded', 'yith_cpb_init_classes', 11 );
