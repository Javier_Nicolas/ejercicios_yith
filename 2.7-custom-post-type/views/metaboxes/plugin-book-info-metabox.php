<?php
/**
 * This file belongs to the YITH CPB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

?>
<div class="ga-tst-form">
<!-- Testimonial role field -->
<div class="ga-tst-form__input ga-tst-form__input-role">
		<label class="ga-tst-form__input-role__label" for="tst_role"><?php _e( 'Role', 'yith-plugin-book' ); /* phpcs:ignore */ ?></label>
		<input type="text" class="ga-tst-form__input-role__input" name="_yith_cpb_role" id="tst_role"
			value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_cpb_role', true ) ); ?>">
	</div>
</div>
