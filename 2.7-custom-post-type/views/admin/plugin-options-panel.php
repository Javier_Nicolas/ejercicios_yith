<?php
/**
 * This file belongs to the YITH CPB Plugin Book.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

?>
<div class="wrap">
	<h1><?php esc_html_e( 'Settings', 'yith-plugin-book' ); ?></h1>

	<form method="post" action="options.php">
		<?php
			settings_fields( 'cpb-options-page' );
			do_settings_sections( 'cpb-options-page' );
			submit_button();
		?>

	</form>
</div>
