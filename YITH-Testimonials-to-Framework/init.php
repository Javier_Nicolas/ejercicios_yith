<?php
/**
 * Plugin Name: JNRP_2.7.1 Testimonial post plugin - framework
 * Description: Exercise about adding a post type, testimonial and its various features.
 * Version: 1.0.0
 * Author: Javier Nicolás Rodríguez Pérez.
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-testimonial
 *
 * @package yith_formacion
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Before all, check if defined ABSPATH.
}

/*Create some constant where defined PATH for Style, Assets, Templates, Views */
if ( ! defined( 'YITH_TPP_VERSION' ) ) {
	define( 'YITH_TPP_VERSION', '1.0.0' );
}
if ( ! defined( 'YITH_TPP_DIR_URL' ) ) {
	define( 'YITH_TPP_DIR_URL', plugin_dir_url( __FILE__ ) );
}
if ( ! defined( 'YITH_TPP_DIR_ASSETS_URL' ) ) {
	define( 'YITH_TPP_DIR_ASSETS_URL', YITH_TPP_DIR_URL . 'assets' );
}
if ( ! defined( 'YITH_TPP_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_TPP_DIR_ASSETS_CSS_URL', YITH_TPP_DIR_ASSETS_URL . '/css' );
}
if ( ! defined( 'YITH_TPP_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_TPP_DIR_ASSETS_JS_URL', YITH_TPP_DIR_ASSETS_URL . '/js' );
}
if ( ! defined( 'YITH_TPP_DIR_ASSETS_IMG_URL' ) ) {
	define( 'YITH_TPP_DIR_ASSETS_IMG_URL', YITH_TPP_DIR_ASSETS_URL . 'img' );
}
if ( ! defined( 'YITH_TPP_DIR_PATH' ) ) {
	define( 'YITH_TPP_DIR_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'YITH_TPP_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_TPP_DIR_INCLUDES_PATH', YITH_TPP_DIR_PATH . 'includes' );
}
if ( ! defined( 'YITH_TPP_DIR_LANGUAGES_PATH' ) ) {
	define( 'YITH_TPP_DIR_LANGUAGES_PATH', YITH_TPP_DIR_PATH . 'languages' );
}
if ( ! defined( 'YITH_TPP_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_TPP_DIR_TEMPLATES_PATH', YITH_TPP_DIR_PATH . 'templates' );
}
if ( ! defined( 'YITH_TPP_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_TPP_DIR_VIEWS_PATH', YITH_TPP_DIR_PATH . 'views' );
}


if ( ! defined( 'YITH_TPP_INIT' ) ) {
	define( 'YITH_TPP_INIT', plugin_basename( __FILE__ ) );
}
if ( ! defined( 'YITH_TPP_FILE' ) ) {
	define( 'YITH_TPP_FILE', __FILE__ );
}
if ( ! defined( 'YITH_TPP_SLUG' ) ) {
	define( 'YITH_TPP_SLUG', 'yith-plugin-testimonial' );
}
if ( ! defined( 'YITH_TPP_SECRET_KEY' ) ) {
	define( 'YITH_TPP_SECRET_KEY', 'zd9egFgFdF1D8Azh2ifK' );
}


if ( ! function_exists( 'yith_tpp_init_classes' ) ) {
	/**
	 * Include the scripts
	 */
	function yith_tpp_init_classes() {
		load_plugin_textdomain( 'yith-plugin-testimonial', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_TPP_DIR_INCLUDES_PATH . '/class-yith-tpp-plugin-testimonial.php';

		if ( class_exists( 'YITH_TPP_Plugin_Testimonial' ) ) {
			/*
			*	Call the main function
			*/
			yith_tpp_plugin_testimonial();
		}
	}
}
add_action( 'plugins_loaded', 'yith_tpp_init_classes', 11 );

if ( ! function_exists( 'yith_tpp_update_testimonials_transient' ) ) {
	/**
	 * Update testimonials post in transient.
	 */
	function yith_tpp_update_testimonials_transient() {
		$yith_testimonial_posts = get_posts(
			array(
				'numberposts' => 3,
				'post_type'   => 'yith-tpp-testimonial',
			)
		);
		set_transient( 'yith_tpp_testimonials_transient', $yith_testimonial_posts, 3 * HOUR_IN_SECONDS );
		return $yith_testimonial_posts;
	}
}

if ( ! function_exists( 'yith_tpp_update_transients' ) ) {
	/**
	 * Update testimonials transient at new post.
	 *
	 * @param string $post_id .
	 */
	function yith_tpp_update_transients( $post_id ) {
		yith_tpp_update_testimonials_transient();
	}
}
add_action( 'publish_yith-tpp-testimonial', 'yith_tpp_update_transients' );
add_action( 'trash_yith-tpp-testimonial', 'yith_tpp_update_transients' );

if ( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_TPP_DIR_PATH . 'plugin-fw/init.php' ) ) {
	require_once YITH_TPP_DIR_PATH . 'plugin-fw/init.php';
}
yit_maybe_plugin_fw_loader( YITH_TPP_DIR_PATH );

if ( ! function_exists( 'yith_plugin_registration_hook' ) ) {
	require_once 'plugin-fw/yit-plugin-registration-hook.php';
}

register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );

if ( ! function_exists( 'yit_deactive_free_version' ) ) {
	require_once 'plugin-fw/yit-deactive-plugin.php';
}

yit_deactive_free_version( 'MY_PLUGIN_FREE_INIT', plugin_basename( __FILE__ ) );
