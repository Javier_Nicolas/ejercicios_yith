jQuery(
	function($) {
		// Rating
		for (let yith_tpp_stars of $( ".yith-tpp-rating-stars" )) {
			// Calculate and save rating
			yith_tpp_stars.addEventListener(
				"click",
				function (event) {
					star_count        = this.getElementsByClassName( "fas fa-star" ).length;
					half_star         = this.getElementsByClassName( "fas fa-star-half-alt" ).length * 0.5;
					yith_input_rating = this.nextSibling;
					while (yith_input_rating.className != "yith_tpp_rating") {
						yith_input_rating = yith_input_rating.nextSibling;
					}
					yith_input_rating.value = star_count + half_star;
				}
			);
			// Load saved rating upon exit the element.
			yith_tpp_stars.addEventListener(
				"mouseleave",
				function (event) {
					yith_tpp_testimonial_show_stars( this );
				}
			);
			// On mouse move, change the stars filling.
			for (let yith_tpp_star of yith_tpp_stars.children) {
				let half = (yith_tpp_star.getBoundingClientRect().left + yith_tpp_star.getBoundingClientRect().right) / 2
				yith_tpp_star.addEventListener(
					"mousemove",
					function (event){
						if (event.clientX > half) {
							this.className = "fas fa-star";
						} else {
							this.className = "fas fa-star-half-alt";
						}

						for (let aux = this.previousSibling; aux; aux = aux.previousSibling) {
							aux.className = "fas fa-star";
						}
						for (let aux = this.nextSibling; aux; aux = aux.nextSibling) {
							aux.className = "far fa-star";
						}
					}
				);
			}
			yith_tpp_testimonial_show_stars( yith_tpp_stars );
		}
		// Load saved rating.
		function yith_tpp_testimonial_show_stars(yith_tpp_stars){
			actual_star       = yith_tpp_stars.children[0];
			yith_input_rating = yith_tpp_stars.nextSibling;
			while (yith_input_rating.className != "yith_tpp_rating") {
				yith_input_rating = yith_input_rating.nextSibling;
			}
			rating = Number( yith_input_rating.value );
			while ( rating >= 1 ) {
				actual_star.className = "fas fa-star";
				actual_star           = actual_star.nextSibling;
				rating--;
			}
			if (rating > 0) {
				actual_star.className = "fas fa-star-half-alt";
				actual_star           = actual_star.nextSibling;
			}
			while (actual_star) {
				actual_star.className = "far fa-star";
				actual_star           = actual_star.nextSibling;
			}
		}

	}
);
