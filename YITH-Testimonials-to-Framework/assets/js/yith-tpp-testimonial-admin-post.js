// Show or hide badge text and color inputs
yith_tpp_badge_check = document.getElementById( "_yith_tpp_badgeonoff" );
yith_tpp_badgetext   = document.getElementById( "_yith_tpp_badgetext-container" );
yith_tpp_bcolor      = document.getElementById( "_yith_tpp_bcolor-container" );
yith_tpp_badge_check.addEventListener(
	"click",
	function (event) {
		yith_tpp_show_badgeoptions();
	}
);
yith_tpp_show_badgeoptions();
function yith_tpp_show_badgeoptions(){
	if ( yith_tpp_badge_check.checked ) {
		yith_tpp_badgetext.style = "display: block;";
		yith_tpp_bcolor.style    = "display: block;";
	} else {
		yith_tpp_badgetext.style = "display: none;";
		yith_tpp_bcolor.style    = "display: none;";
	}
}
