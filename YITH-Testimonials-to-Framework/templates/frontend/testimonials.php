<?php
/**
 * This file belongs to the YITH Testimonial Post Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

echo '<div class="yith-tpp-testimonials-container alignfull">';
foreach ( $yith_testimonial_posts as $yith_testimonial_post ) {
	$meta = get_post_meta( $yith_testimonial_post->ID );
	echo '<div class="yith-tpp-testimonials-single';
	// Select hover effect (if any).
	$hover_effect = isset( $args['hover_effect'] ) ? $args['hover_effect'] : get_option( 'yith_tpp_shortcode_hover', null );
	if ( isset( $hover_effect ) ) {
		switch ( $hover_effect ) {
			case 'zoom':
				echo ' yith-tpp-testimonials-zoom';
				break;
			case 'highlight':
				echo ' yith-tpp-testimonials-highlight';
		}
	}
	// Show if vip.
	if ( isset( $meta['_yith_tpp_vip'][0] ) && $meta['_yith_tpp_vip'][0] ) {
		echo ' yith-tpp-testimonials-vip';
	}
	echo '">';
	// Show badge if set.
	if ( isset( $meta['_yith_tpp_badgeonoff'][0] ) && $meta['_yith_tpp_badgeonoff'][0] ) {
		echo '<span class="yith-tpp-testimonials-badge" style="background-color:';
		echo esc_html( $meta['_yith_tpp_bcolor'][0] );
		echo '">';
		echo esc_html( $meta['_yith_tpp_badgetext'][0] );
		echo '</span>';
	}
	echo '<div class="yith-tpp-testimonials-header">';
	// Show image if set.
	$img_bool = ! empty( get_option( 'yith_tpp_shortcode_show_image', true ) ) ? get_option( 'yith_tpp_shortcode_show_image', true ) : true;
	if ( isset( $args['show_image'] ) ) {
		$img_bool = 'no' === $args['show_image'] ? false : true;
	}
	if ( $img_bool ) {
		echo get_the_post_thumbnail( $yith_testimonial_post->ID, 'thumbnail' );
	}
	// Show title.
	echo '<div class="yith-tpp-testimonials-header-text"><h5>' . esc_html( $yith_testimonial_post->post_title ) . '</h5>';
	// Show role and company info.
	$bool_role    = isset( $meta['_yith_tpp_role'][0] );
	$bool_company = isset( $meta['_yith_tpp_company'][0] );
	$bool_url     = isset( $meta['_yith_tpp_url'][0] );
	$bool_email   = isset( $meta['_yith_tpp_email'][0] );
	if ( $bool_role || $bool_company || $bool_url ) {
		echo '<span class="yith-tpp-testimonials-workplace">';
		if ( $bool_role ) {
			echo esc_html( $meta['_yith_tpp_role'][0] );
		}
		if ( $bool_role && ( $bool_company || $bool_url ) ) {
			echo esc_html__( ' at ', 'text_domain' );
		}
		if ( $bool_url ) {
			echo '<a href="' . esc_url( $meta['_yith_tpp_url'][0] ) . '">';
			if ( $bool_company ) {
				echo esc_html( $meta['_yith_tpp_company'][0] );
			} else {
				echo esc_html__( 'Company url', 'text_domain' );
			}
			echo '</a>';
		} elseif ( $bool_company ) {
			echo esc_html( $meta['_yith_tpp_company'][0] );
		}
		echo '</span>';
		if ( $bool_email ) {
			echo '<br>';
		}
	}
	// Show email if set.
	if ( $bool_email ) {
		echo '<span class="yith-tpp-testimonials-email">' . esc_html( $meta['_yith_tpp_email'][0] ) . '</span>';
	}
	// Show rating if set.
	if ( isset( $meta['_yith_tpp_rating'] ) ) {
		echo '<br><span class="yith-tpp-testimonials-show-rating">';
		$rating = floatval( $meta['_yith_tpp_rating'][0] );
		for ( $i = 0; $i < 5; $i++ ) {
			echo '<i class="';
			if ( $rating >= 1 ) {
				echo 'fas fa-star';
			} elseif ( $rating > 0 ) {
				echo 'fas fa-star-half-alt';
			} else {
				echo 'far fa-star';
			}
			echo '"></i>';
			$rating--;
		}
		echo '</span>';
	}
	// Show message.
	echo '</div></div><div class="yith-tpp-testimonials-body">' . esc_html( $yith_testimonial_post->post_content ) . '</div>';
	echo '<div class="yith-tpp-testimonials-footer">';
	$post_yith_taxonomies = wp_get_post_terms( $yith_testimonial_post->ID, array( 'yith_tpp_sector_tax', 'yith_tpp_country_tax' ) );
	foreach ( $post_yith_taxonomies as $yith_taxonomy ) {
		echo esc_html( $yith_taxonomy->name );
		echo ' &nbsp ';
	}
	echo '</div></div>';
}
echo '</div>';
