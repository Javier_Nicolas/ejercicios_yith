<?php
/**
 * Plugin Name: JNRP_2.7.1 Testimonial post plugin
 * Description: Exercise about adding a post type, testimonial and its various features.
 * Version: 1.0.0
 * Author: Javier Nicolás Rodríguez Pérez.
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-testimonial
 *
 * @package yith_formacion
 */

if ( ! function_exists( 'register_testimonial_transient_widget' ) ) {
	/**
	 * Registro del widget.
	 */
	function register_testimonial_transient_widget() {
		register_widget( 'YITH_TPP_Testimonials_Transient_Widget' );
	}
}
add_action( 'widgets_init', 'register_testimonial_transient_widget' );

if ( ! class_exists( 'YITH_TPP_Testimonials_Transient_Widget' ) ) {
	/**
	 * Widget que muestra los datos introducidos de usuario.
	 */
	class YITH_TPP_Testimonials_Transient_Widget extends WP_Widget {

		/**
		 * Sets up the widgets name etc
		 */
		public function __construct() {
			parent::__construct(
				'testimonial_transient_widget',
				'Last 3 testimonials',
				array( 'description' => __( 'Last 3 testimonials', 'text_domain' ) )
			);
		}

		/**
		 * Outputs the content of the widget
		 *
		 * @param array $args Arguments.
		 * @param array $instance .
		 */
		public function widget( $args, $instance ) {
			echo '<h4>' . esc_html__( 'Last 3 testimonials', 'text_domain' ) . '</h4>';
			echo do_shortcode( '[testimonials_transient]' );
		}

		/**
		 * Outputs the options form on admin
		 *
		 * @param array $instance The widget options.
		 */
		public function form( $instance ) {}

		/**
		 * Processing widget options on save
		 *
		 * @param array $new_instance The new options.
		 * @param array $old_instance The previous options.
		 */
		public function update( $new_instance, $old_instance ) {}
	}
}
