<?php
/**
 * This file belongs to the YITH Testimonial Post Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_TPP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_TPP_Plugin_Testimonial' ) ) {

	/**
	 * Inicializador del plugin.
	 */
	class YITH_TPP_Plugin_Testimonial {

		/**
		 * Main Instance
		 *
		 * @var YITH_TPP_Plugin_Testimonial
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/**
		 * Main Admin Instance
		 *
		 * @var YITH_TPP_Plugin_Testimonial_Admin
		 * @since 1.0
		 */
		public $admin = null;

		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_TPP_Plugin_Testimonial_Frontend
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_TPP_Plugin_Testimonial Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6 .
		}

		/**
		 * YITH_TPP_Plugin_Testimonial constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_tpp_require_class',
				array(
					'common'   => array(
						'includes/class-yith-tpp-post-types.php',
						'includes/functions.php',
						'includes/class-yith-tpp-shortcodes.php',
						'includes/class-yith-tpp-css.php',
						'includes/class-yith-tpp-testimonials-transient-widget.php',
						'includes/class-yith-tpp-custom-roles.php',
					),
					'admin'    => array(
						'includes/class-yith-tpp-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-tpp-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			/*
				Here set any other hooks ( actions or filters you'll use on this class)
			*/

			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );

			// Finally call the init function.
			$this->init();
		}

		/**
		 * Plugin framework loader.
		 */
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once $plugin_fw_file;
				}
			}
		}

		/**
		 * Register plugins for activation tab
		 */
		public function register_plugin_for_activation() {
			if ( function_exists( 'YIT_Plugin_Licence' ) ) {
				YIT_Plugin_Licence()->register( YITH_TPP_INIT, YITH_TPP_SECRET_KEY, YITH_TPP_SLUG );
			}
		}

		/**
		 * Register plugins for update tab
		 */
		public function register_plugin_for_updates() {
			if ( function_exists( 'YIT_Upgrade' ) ) {
				YIT_Upgrade()->register( YITH_TPP_SLUG, YITH_TPP_INIT );
			}
		}

		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param array $main_classes array The require classes file path.
		 *
		 * @author Javier Nicolás Rodríguez Pérez
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_TPP_DIR_PATH . $class ) ) {
						require_once YITH_TPP_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 *
		 * @author Javier Nicolás Rodríguez Pérez
		 * @since  1.0
		 **/
		public function init_classes() {
			// $this->function = YITH_TPP_Other_Class::get_instance();
			// $this->ajax = YITH_TPP_Ajax::get_instance();
			// $this->compatibility = YITH_TPP_Compatibility::get_instance();
			YITH_TPP_Post_Types::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Javier Nicolás Rodríguez Pérez
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_TPP_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_TPP_Frontend::get_instance();
			}
		}

	}
}

if ( ! function_exists( 'yith_tpp_plugin_testimonial' ) ) {
	/**
	 * Get the YITH_TPP_Plugin_Testimonial instance
	 *
	 * @return YITH_TPP_Plugin_Testimonial
	 */
	function yith_tpp_plugin_testimonial() {
		return YITH_TPP_Plugin_Testimonial::get_instance();
	}
}
