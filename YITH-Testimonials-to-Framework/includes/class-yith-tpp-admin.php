<?php
/**
 * This file belongs to the YITH TPP Plugin Testimonial.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_TPP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_TPP_Admin' ) ) {

	/**
	 * Admin related.
	 */
	class YITH_TPP_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_TPP_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		// @var YIT_Plugin_Panel $_panel the panel
		private $panel;

		/**
		 * @var Panel page
		 */
		protected $panel_page = 'yith_testimonial_panel';

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_TPP_Admin Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_TPP_Admin constructor.
		 */
		private function __construct() {
			// Admin menu.
			add_action( 'admin_menu', array( $this, 'yith_tpp_menus' ) );
			add_action( 'admin_init', array( $this, 'yith_tpp_metabox' ) );
			add_action( 'yith_tpp_print_custom_field', array( $this, 'yith_tpp_custom_field' ), 10, 1 );

			// Testimonials manager role.
			add_action( 'plugins_loaded', 'yith_tpp_testimonials_manager_role' );

			// Allows custom role to save in the plugin options.
			add_filter(
				'option_page_capability_tpp-options-page',
				function( $capability ) {
					return 'manage_yith-tpp-testimonials';
				}
			);

			// Framework.
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_TPP_DIR_PATH . '/' . basename( YITH_TPP_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_TPP_SLUG );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_TPP_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_TPP_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}

		/**
		 *  Create menus for testimonials
		 */
		public function yith_tpp_menus() {
			if ( ! empty( $this->panel ) ) {
				return;
			}

			$admin_tabs = array(
				'settings'              => __( 'Shortcode settings', 'yith-plugin-testimonial' ),
				'testimonial-post-type' => __( 'Testimonial Post Type', 'yith-plugin-testimonial' ),
				'taxonomy-sector'       => __( 'Sector Taxonomy', 'yith-plugin-testimonial' ),
				'taxonomy-country'      => __( 'Country Taxonomy', 'yith-plugin-testimonial' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH TPP Plugin', // this text MUST be NOT translatable
				'menu_title'         => 'YITH TPP Plugin', // this text MUST be NOT translatable
				'plugin_description' => __( 'Testimonials plugin description', 'yith-plugin-testimonial' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => YITH_TPP_SLUG,
				'parent_page'        => 'yith_plugin_panel',
				'page'               => YITH_TPP_SLUG . '_panel',
				'admin-tabs'         => $admin_tabs,
				'plugin-url'         => YITH_TPP_DIR_PATH,
				'options-path'       => YITH_TPP_DIR_PATH . 'plugin-options',
			);

			$this->panel = new YIT_Plugin_Panel( $args );
		}

		/**
		 *  Create metaboxes for testimonials
		 */
		public function yith_tpp_metabox() {

			$fields = array(
				'yith_tpp_role'       => array(
					'label'   => __( 'Role', 'yith-plugin-testimonial' ),
					'type'    => 'text',
					'private' => true,
					'std'     => '',
				),
				'yith_tpp_company'    => array(
					'label'   => __( 'Company', 'yith-plugin-testimonial' ),
					'type'    => 'text',
					'private' => true,
					'std'     => '',
				),
				'yith_tpp_url'        => array(
					'label'   => __( 'Company url', 'yith-plugin-testimonial' ),
					'type'    => 'text',
					'private' => true,
					'std'     => '',
				),
				'yith_tpp_email'      => array(
					'label'   => __( 'Email', 'yith-plugin-testimonial' ),
					'type'    => 'text',
					'private' => true,
					'std'     => '',
				),
				'yith_tpp_vip'        => array(
					'label'   => __( 'VIP', 'yith-plugin-testimonial' ),
					'type'    => 'checkbox',
					'private' => true,
				),
				'yith_tpp_badgeonoff' => array(
					'label'   => __( 'Enable badgeonoff', 'yith-plugin-testimonial' ),
					'type'    => 'checkbox',
					'private' => true,
				),
				'yith_tpp_badgetext'  => array(
					'label'   => __( 'Badge text', 'yith-plugin-testimonial' ),
					'type'    => 'text',
					'private' => true,
					'std'     => '',
				),
				'yith_tpp_bcolor'     => array(
					'label'         => __( 'Background color', 'yith-plugin-testimonial' ),
					'type'          => 'colorpicker',
					'private'       => true,
					'alpha_enabled' => false,
					'default'       => '#effeff',
				),
				'yith_tpp_rating'     => array(
					'label'   => __( 'Rating', 'yith-plugin-testimonial' ),
					'type'    => 'custom',
					'private' => true,
					'action'  => 'yith_tpp_print_custom_field',
				),
			);

			$args = array(
				'label'    => __( 'Additional - Information', 'yith-plugin-testimonial' ),
				'pages'    => YITH_TPP_Post_Types::$post_type,
				'context'  => 'normal',
				'priority' => 'default',
				'tabs'     => array(
					'info' => array( //tab
						'label'  => __( 'Additional Information', 'yith-plugin-testimonial' ),
						'fields' => $fields,
					),
				),
			);

			$metabox1 = YIT_Metabox( 'yith_tpp_metabox' );
			$metabox1->init( $args );
		}

		/**
		 * Print Rating field.
		 */
		public function yith_tpp_custom_field( $field ) {
			$max_rating = 5;
			?>
			<div class="ga-tst-form__input ga-tst-form__input-rating">
				<fieldset class="yith-tpp-rating">
					<div class="yith-tpp-rating-stars">
						<?php
						for ( $i = 0; $i < $max_rating; $i++ ) {
							echo '<i class="far fa-star yith-tpp-rating-star"></i>';
						}
						?>
					</div>
					<input type="hidden" class="yith_tpp_rating" name="<?php echo esc_html( $field['name'] ); ?>" id="<?php echo esc_html( $field['id'] ); ?>"
					value="<?php echo esc_html( $field['value'] ); ?>">
				</fieldset>
			</div>
			<?php
		}
	}
}

