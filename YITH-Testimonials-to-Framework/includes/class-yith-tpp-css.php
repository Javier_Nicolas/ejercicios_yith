<?php
/**
 * Plugin Name: JNRP_2.7.1 Testimonial post plugin
 * Description: Exercise about adding a post type, testimonial and its various features.
 * Version: 1.0.0
 * Author: Javier Nicolás Rodríguez Pérez.
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-testimonial
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_TPP_CSS_JS_VER' ) ) {
	define( 'YITH_TPP_CSS_JS_VER', '1.0.0.0' );
}

if ( ! function_exists( 'yith_tpp_testimonial_shortcode_css' ) ) {
	/**
	 * Css needed for testimonials shortcode.
	 *
	 * @param string $inline_styles CSS to add dynamically.
	 */
	function yith_tpp_testimonial_shortcode_css( $inline_styles = null ) {
		wp_register_style(
			'yith-tpp-testimonial-shortcode-css',
			YITH_TPP_DIR_ASSETS_CSS_URL . '/yith-tpp-testimonial-shortcode.css',
			array(),
			YITH_TPP_CSS_JS_VER
		);
		wp_enqueue_style( 'yith-tpp-testimonial-shortcode-css' );
		if ( isset( $inline_styles ) ) {
			wp_add_inline_style( 'yith-tpp-testimonial-shortcode-css', $inline_styles );
		}
	}
}

if ( ! function_exists( 'mw_enqueue_color_picker' ) ) {
	/** Color picker and font awesome. */
	function mw_enqueue_color_picker() {
		wp_enqueue_style( 'wp-color-picker' );
		wp_register_script(
			'yith-tpp-testimonial-load-color-picker',
			YITH_TPP_DIR_ASSETS_JS_URL . '/yith-tpp-testimonial-color-picker.js',
			array( 'wp-color-picker' ),
			YITH_TPP_CSS_JS_VER,
			true
		);
		wp_enqueue_script( 'yith-tpp-testimonial-load-color-picker' );
		wp_register_style(
			'yith-tpp-testimonial-load-fa-css',
			'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css',
			array(),
			YITH_TPP_CSS_JS_VER
		);
		wp_enqueue_style( 'yith-tpp-testimonial-load-fa-css' );
	}
}
add_action( 'admin_enqueue_scripts', 'mw_enqueue_color_picker' );

if ( ! function_exists( 'yith_tpp_testimonial_admin_post_imports' ) ) {
	/** Css needed for rating. */
	function yith_tpp_testimonial_admin_post_imports() {
		// Font Awesome.
		wp_register_style(
			'yith-tpp-testimonial-load-fa-css',
			'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css',
			array(),
			YITH_TPP_CSS_JS_VER
		);
		wp_enqueue_style( 'yith-tpp-testimonial-load-fa-css' );
		// Custom rating.
		wp_register_script(
			'yith-tpp-testimonial-rating-js',
			YITH_TPP_DIR_ASSETS_JS_URL . '/yith-tpp-testimonial-rating.js',
			array( 'jquery' ),
			YITH_TPP_CSS_JS_VER,
			true
		);
		wp_enqueue_script( 'yith-tpp-testimonial-rating-js' );
		// Admin js (Badge).
		wp_register_script(
			'yith-tpp-testimonial-admin-js',
			YITH_TPP_DIR_ASSETS_JS_URL . '/yith-tpp-testimonial-admin-post.js',
			array(),
			YITH_TPP_CSS_JS_VER,
			true
		);
		wp_enqueue_script( 'yith-tpp-testimonial-admin-js' );
	}
}
add_action( 'admin_enqueue_scripts', 'yith_tpp_testimonial_admin_post_imports' );
