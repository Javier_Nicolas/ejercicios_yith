<?php
/**
 * This file belongs to the YITH Testimonial Post Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

// TODO: Move code to admin file.
if ( ! function_exists( 'yith_tpp_testimonials_manager_role' ) ) {
	/** Create testimonials manager role and add the capability to manage the plugin to those with manage_options. **/
	function yith_tpp_testimonials_manager_role() {
		if ( empty( $GLOBALS['wp_roles']->is_role( 'yith_tpp_testimonials_manager' ) ) ) {
			add_role(
				'yith_tpp_testimonials_manager',
				'Testimonials Manager',
				array(
					'read_private_yith-tpp-testimonials' => true,
					'create_yith-tpp-testimonials'       => true,
					'edit_yith-tpp-testimonials'         => true,
					'delete_yith-tpp-testimonials'       => true,
					'manage_yith-tpp-testimonials'       => true,
					'edit-yith-tpp-tax'                  => true,
					'manage_yith-tpp-testimonials-tax'   => true,
					'edit_yith-tpp-testimonials-tax'     => true,
					'delete_yith-tpp-testimonials-tax'   => true,
					'assign_yith-tpp-testimonials-tax'   => true,
					'read'                               => true,
					'upload_files'                       => true,
				)
			);
			yith_tpp_testimonials_add_capabilities_to_role( 'administrator' );
		}
	}
}

if ( ! function_exists( 'yith_tpp_testimonials_add_capabilities_to_role' ) ) {
	/**
	 * Create testimonials manager role and add the capability to manage the plugin to those with manage_options.
	 *
	 * @param string $role_name .
	 */
	function yith_tpp_testimonials_add_capabilities_to_role( $role_name ) {
		$role = get_role( $role_name );
		if ( ! isset( $role->capabilities['read_private_yith-tpp-testimonials'] ) && ! $role->capabilities['read_private_yith-tpp-testimonials'] ) {
			$keys = array(
				'read_private_yith-tpp-testimonials',
				'create_yith-tpp-testimonials',
				'edit_yith-tpp-testimonials',
				'delete_yith-tpp-testimonials',
				'manage_yith-tpp-testimonials',
				'edit-yith-tpp-tax',
				'manage_yith-tpp-testimonials-tax',
				'edit_yith-tpp-testimonials-tax',
				'delete_yith-tpp-testimonials-tax',
				'assign_yith-tpp-testimonials-tax',
			);
			foreach ( $keys as $cap ) {
				$role->add_cap( $cap );
			}
		}
	}
}
