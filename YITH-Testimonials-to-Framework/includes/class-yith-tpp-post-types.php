<?php
/**
 * This file belongs to the YITH Testimonial Post Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_TPP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_TPP_Post_Types' ) ) {

	/**
	 * Custom post types.
	 */
	class YITH_TPP_Post_Types {

		/**
		 * Main Instance
		 *
		 * @var YITH_TPP_Post_Types
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_TPP_Post_Types
		 * @since 1.0
		 * @access public
		 */
		public static $post_type = 'yith-tpp-testimonial';


		/**
		 * Main plugin Instance
		 *
		 * @return YITH_TPP_Post_Types Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_TPP_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );
			add_action( 'init', array( $this, 'register_taxonomy' ) );
		}

		/**
		 * Setup the 'Testimonial' custom post type.
		 */
		public function setup_post_type() {
			$args = array(
				'label'           => __( 'Testimonial', 'yith-plugin-testimonial' ),
				'description'     => __( 'Testimonial post type', 'yith-plugin-testimonial' ),
				'public'          => false,
				'menu_icon'       => 'dashicons-universal-access',
				'show_in_menu'    => false,
				'show_ui'         => true,
				'rewrite'         => false,
				'supports'        => array( 'title', 'editor', 'author', 'thumbnail' ),
				'capability_type' => 'yith-tpp-testimonial',
				'capabilities'    => array(
					'read_private_posts'     => 'read_yith-tpp-testimonials',
					'create_posts '          => 'create_yith-tpp-testimonials',
					'publish_posts'          => 'create_yith-tpp-testimonials',
					'edit_post'              => 'edit_yith-tpp-testimonials',
					'edit_posts'             => 'edit_yith-tpp-testimonials',
					'edit_others_posts'      => 'edit_yith-tpp-testimonials',
					'edit_private_posts'     => 'edit_yith-tpp-testimonials',
					'edit_published_posts'   => 'edit_yith-tpp-testimonials',
					'delete_post'            => 'delete_yith-tpp-testimonials',
					'delete_posts'           => 'delete_yith-tpp-testimonials',
					'delete_others_posts'    => 'delete_yith-tpp-testimonials',
					'delete_private_posts'   => 'delete_yith-tpp-testimonials',
					'delete_published_posts' => 'delete_yith-tpp-testimonials',
				),
			);
			register_post_type( self::$post_type, $args );
		}

		/**
		 * Register taxonomy.
		 */
		public function register_taxonomy() {

			$capabilities = array(
				'manage_terms' => 'manage_yith-tpp-testimonials-tax',
				'edit_terms '  => 'edit_yith-tpp-testimonials-tax',
				'delete_terms' => 'delete_yith-tpp-testimonials-tax',
				'assign_terms' => 'assign_yith-tpp-testimonials-tax',
			);

			// New taxonomy, hierarchical (like categories).

			$labels = array(
				'name'              => _x( 'Sector', 'taxonomy general name', 'yith-plugin-testimonial' ),
				'singular_name'     => _x( 'Sector', 'taxonomy singular name', 'yith-plugin-testimonial' ),
				'search_items'      => __( 'Search sector', 'yith-plugin-testimonial' ),
				'all_items'         => __( 'All sectors', 'yith-plugin-testimonial' ),
				'parent_item'       => __( 'Parent sector', 'yith-plugin-testimonial' ),
				'parent_item_colon' => __( 'Parent sector:', 'yith-plugin-testimonial' ),
				'edit_item'         => __( 'Edit sector', 'yith-plugin-testimonial' ),
				'update_item'       => __( 'Update sector', 'yith-plugin-testimonial' ),
				'add_new_item'      => __( 'Add new sector', 'yith-plugin-testimonial' ),
				'new_item_name'     => __( 'New sector name', 'yith-plugin-testimonial' ),
				'menu_name'         => __( 'Sector', 'yith-plugin-testimonial' ),
			);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'capabilities'      => $capabilities,
				'rewrite'           => array( 'slug' => 'yith_tpp_sector_tax' ),
			);

			register_taxonomy( 'yith_tpp_sector_tax', array( self::$post_type ), $args );

			// New taxonomy, no hierarchical (like tags).

			$labels1 = array(
				'name'              => _x( 'Country', 'taxonomy general name', 'yith-plugin-testimonial' ),
				'singular_name'     => _x( 'Country', 'taxonomy singular name', 'yith-plugin-testimonial' ),
				'search_items'      => __( 'Search country', 'yith-plugin-testimonial' ),
				'all_items'         => __( 'All country', 'yith-plugin-testimonial' ),
				'parent_item'       => __( 'Parent country', 'yith-plugin-testimonial' ),
				'parent_item_colon' => __( 'Parent country:', 'yith-plugin-testimonial' ),
				'edit_item'         => __( 'Edit country', 'yith-plugin-testimonial' ),
				'update_item'       => __( 'Update country', 'yith-plugin-testimonial' ),
				'add_new_item'      => __( 'Add new country', 'yith-plugin-testimonial' ),
				'new_item_name'     => __( 'New country Name', 'yith-plugin-testimonial' ),
				'menu_name'         => __( 'Country', 'yith-plugin-testimonial' ),
			);

			$args1 = array(
				'hierarchical'      => false,
				'labels'            => $labels1,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'capabilities'      => $capabilities,
				'rewrite'           => array( 'slug' => 'yith_tpp_country_tax' ),
			);

			register_taxonomy( 'yith_tpp_country_tax', array( self::$post_type ), $args1 );
		}
	}
}
