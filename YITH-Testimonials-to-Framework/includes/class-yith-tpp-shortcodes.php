<?php
/**
 * Plugin Name: JNRP_2.7.1 Testimonial post plugin
 * Description: Exercise about adding a post type, testimonial and its various features.
 * Version: 1.0.0
 * Author: Javier Nicolás Rodríguez Pérez.
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-testimonial
 *
 * @package yith_formacion
 */

if ( ! function_exists( 'testimonial_shortcode' ) ) {
	/**
	 * Shortcode to show the testimonials posts.
	 *
	 * @param array $args Argumentos.
	 */
	function testimonial_shortcode( $args ) {
		$options                = get_option( 'yit_yith-plugin-testimonial_options', array() );
		$yith_testimonial_posts = array();
		// What or how many posts to show.
		if ( isset( $args['ids'] ) ) {
			$ids = explode( ',', $args['ids'] );
			foreach ( $ids as $id ) {
				array_push( $yith_testimonial_posts, get_post( $id ) );
			}
		} elseif ( isset( $args['tax_id'] ) ) {
			// Taxonomy filter.
			$number   = isset( $args['number'] ) ? $args['number'] : isset( $options['yith_tpp_shortcode_number'] ) ? $options['yith_tpp_shortcode_number'] : 6;
			$number   = isset( $number ) && is_int( $number ) ? $number : 6;
			$term_ids = explode( ',', $args['tax_id'] );
			$args     = array( 'relation' => 'OR' );
			foreach ( $term_ids as $t_id ) {
				array_push(
					$args,
					array(
						'taxonomy' => get_term( $t_id )->taxonomy,
						'field'    => 'term_id',
						'terms'    => $t_id,
					)
				);
			}
			$yith_testimonial_posts = get_posts(
				array(
					'numberposts' => $number,
					'post_type'   => 'yith-tpp-testimonial',
					'tax_query'   => $args,
				)
			);
		} else {
			$number                 = isset( $args['number'] ) ? $args['number'] : isset( $options['yith_tpp_shortcode_number'] ) ? $options['yith_tpp_shortcode_number'] : 6;
			$number                 = isset( $number ) && is_int( $number ) ? $number : 6;
			$yith_testimonial_posts = get_posts(
				array(
					'numberposts' => $number,
					'post_type'   => 'yith-tpp-testimonial',
				)
			);
		}

		$inline_styles = '';
		// If set, change border radius.
		if ( ! empty( $options['yith_tpp_shortcode_border'] ) ) {
			$inline_styles .= '.yith-tpp-testimonials-single { border-radius: ' . ( isset( $options['yith_tpp_shortcode_border'] ) ? $options['yith_tpp_shortcode_border'] : '' ) . 'px;}';
		}
		// If set, change link color.
		if ( isset( $options['yith_tpp_shortcode_color'] ) ) {
			$inline_styles .= '.yith-tpp-testimonials-workplace > a { color: ' . ( isset( $options['yith_tpp_shortcode_color'] ) ? $options['yith_tpp_shortcode_color'] : '' ) . ';}';
		}
		// Load css.
		yith_tpp_testimonial_shortcode_css( $inline_styles );

		// Show posts.
		ob_start();
		yith_tpp_get_template( '/frontend/testimonials.php', array( 'yith_testimonial_posts' => $yith_testimonial_posts ) );
		return ob_get_clean();
	}
}
add_shortcode( 'testimonials', 'testimonial_shortcode' );

if ( ! function_exists( 'last_three_testimonials_shortcode' ) ) {
	/**
	 * Shortcode to show the last 3 testimonials posts from a transient.
	 *
	 * @param array $args Argumentos.
	 */
	function last_three_testimonials_shortcode( $args ) {
		// Load transient.
		$yith_testimonial_posts = get_transient( 'yith_tpp_testimonials_transient' );
		if ( ! $yith_testimonial_posts ) {
			$yith_testimonial_posts = yith_tpp_update_testimonials_transient();
		}
		// Load css.
		yith_tpp_testimonial_shortcode_css();
		// Show posts.
		ob_start();
		yith_tpp_get_template( '/frontend/testimonials.php', array( 'yith_testimonial_posts' => $yith_testimonial_posts ) );
		return ob_get_clean();
	}
}
add_shortcode( 'testimonials_transient', 'last_three_testimonials_shortcode' );
