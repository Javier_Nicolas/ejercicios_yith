<?php
/**
 * This file belongs to the YITH TPP Plugin Testimonial.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

$tab_post_type = array(
	'testimonial-post-type' => array(
		'testimonial-post-type_list_table' => array(
			'type'      => 'post_type',
			'post_type' => YITH_TPP_Post_Types::$post_type,
		),
	),
);

return $tab_post_type;
