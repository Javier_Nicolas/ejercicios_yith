<?php
/**
 * This file belongs to the YITH TPP Plugin Testimonial.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

$tab_taxonomies = array(
	'taxonomy-sector' => array(
		'yith_tpp_sector_tax_list_table' => array(
			'type'     => 'taxonomy',
			'taxonomy' => 'yith_tpp_sector_tax',
		),
	),
);

return $tab_taxonomies;
