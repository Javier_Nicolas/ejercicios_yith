<?php
/**
 * This file belongs to the YITH TPP Plugin Testimonial.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

$settings = array(
	'settings' => array(
		'options_section' => array(
			array(
				'name' => __( 'Shortcode options', 'yith-test-plugin' ),
				'type' => 'title',
			),
			array(
				'id'   => 'yith_tpp_shortcode_number',
				'name' => esc_html__( 'Default number of testimonials', 'yith-plugin-testimonial' ),
				'type' => 'number',
				'min'  => 0,
				'max'  => 100,
				'step' => 1,
				'std'  => 6,
			),
			array(
				'id'   => 'yith_tpp_shortcode_show_image',
				'name' => esc_html__( 'Show image by default', 'yith-plugin-testimonial' ),
				'type' => 'checkbox',
			),
			array(
				'id'      => 'yith_tpp_shortcode_hover',
				'name'    => esc_html__( 'Default hover', 'yith-plugin-testimonial' ),
				'type'    => 'select',
				'options' => array(
					'none'      => esc_html__( 'None', 'yith-plugin-testimonial' ),
					'zoom'      => esc_html__( 'Zoom', 'yith-plugin-testimonial' ),
					'highlight' => esc_html__( 'Highlight', 'yith-plugin-testimonial' ),
				),
			),
			array(
				'id'   => 'yith_tpp_shortcode_border',
				'name' => esc_html__( 'Default border radius (px)', 'yith-plugin-testimonial' ),
				'type' => 'number',
				'min'  => 0,
				'max'  => 100,
				'step' => 1,
				'std'  => 7,
			),
			array(
				'id'            => 'yith_tpp_shortcode_color',
				'name'          => esc_html__( 'Default link color', 'yith-plugin-testimonial' ),
				'type'          => 'colorpicker',
				'alpha_enabled' => false,
				'default'       => '#0073aa',
			),
			array(
				'type' => 'close',
			),
		),
	),
);
return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
