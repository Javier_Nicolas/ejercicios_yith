<?php
/**
 * This file belongs to the Product Message and Hide Price Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

$settings = array(
	'msg-table' => array(
		'msg-table-options'     => array(
			'type' => 'title',
			'desc' => '',
		),
		'yith_pmhpp[msg_table]' => array(
			'id'                   => 'yith_pmhpp[msg_table]',
			'name'                 => __( 'Messages table', 'yith-pmhpp-text-domain' ),
			'type'                 => 'yith-field',
			'yith-type'            => 'list-table',
			'list_table_class'     => 'YITH_PMHPP_MESSAGE_LIST_TABLE',
			'list_table_class_dir' => YITH_PMHPP_DIR_INCLUDES_PATH . '/class-yith-pnhpp-message-list-table.php',
			'title'                => __( 'Messages table', 'yith-pmhpp-text-domain' ),
			'add_new_button'       => __( 'Add new admin message', 'yith-pmhpp-text-domain' ),
		),
		'msg-table-options-end' => array(
			'type' => 'sectionend',
			'id'   => 'yith-pmhpp-table-options',
		),
	),
);
return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
