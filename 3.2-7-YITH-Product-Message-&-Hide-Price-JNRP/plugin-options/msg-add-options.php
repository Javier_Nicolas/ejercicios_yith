<?php
/**
 * This file belongs to the Product Message and Hide Price Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

global $woocommerce;
$products = array();
$message  = '';
$position = 'bf-cart';
if ( isset( $_GET['edit'] ) && 'edit' === $_GET['edit'] ) {
	$products  = isset( $_GET['products'] ) ? $_GET['products'] : array();
	$messages  = array();
	$positions = array();
	foreach ( $products as $product_id ) {
		$product     = wc_get_product( $product_id );
		$data        = $product->get_meta( 'yith_pmhpp_data' );
		$messages[]  = $data['message'];
		$positions[] = $data['position'];
	}
	if ( count( array_unique( $messages ) ) === 1 ) {
		$message = end( $messages );
	}
	if ( count( array_unique( $positions ) ) === 1 ) {
		$position = end( $positions );
	}
}
$settings = array(
	'msg-add' => array(
		'msg-add-options'       => array(
			'type' => 'title',
			'desc' => '',
		),
		'yith_pmhpp_enable'     => array(
			'id'        => 'yith_pmhpp_enable',
			'name'      => esc_html__( 'Apply this rule', 'yith-pmhpp-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'yes',
		),
		'yith_pmhpp_products'   => array(
			'id'        => 'yith_pmhpp_products',
			'name'      => __( 'Select products', 'yith-pmhpp-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'ajax-products',
			'multiple'  => true,
			'default'   => $products,
			'desc'      => __( 'If the product already have this rule it will be overwritten', 'yith-pmhpp-text-domain' ),
		),
		'yith_pmhpp_message'    => array(
			'id'        => 'yith_pmhpp_message',
			'name'      => esc_html__( 'Message', 'yith-pmhpp-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'textarea-editor',
			'default'   => $message,
		),
		'yith_pmhpp_position'   => array(
			'id'        => 'yith_pmhpp_position',
			'name'      => esc_html__( 'Where to display the message', 'yith-pmhpp-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'select',
			'options'   => array(
				'bf-title' => esc_html__( 'Before title', 'yith-pmhpp-text-domain' ),
				'af-title' => esc_html__( 'After title', 'yith-pmhpp-text-domain' ),
				'bf-price' => esc_html__( 'Before price', 'yith-pmhpp-text-domain' ),
				'af-price' => esc_html__( 'After price', 'yith-pmhpp-text-domain' ),
				'bf-cart'  => esc_html__( 'Before add to cart', 'yith-pmhpp-text-domain' ),
				'af-cart'  => esc_html__( 'After add to cart', 'yith-pmhpp-text-domain' ),
			),
			'default'   => $position,
		),
		'yith_pmhpp_hide_price' => array(
			'id'        => 'yith_pmhpp_hide_price',
			'name'      => esc_html__( 'Hide product price', 'yith-pmhpp-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'default'   => 'no',
		),
		'yith_pmhpp_buttons'    => array(
			'type'      => 'yith-field',
			'yith-type' => 'buttons',
			'buttons'   => array(
				array(
					'name' => esc_html__( 'Back to product list', 'yith-pmhpp-text-domain' ),
					'data' => array(
						'action' => 'go_back',
					),
				),
				array(
					'name'  => esc_html__( 'Save product', 'yith-pmhpp-text-domain' ),
					'class' => 'button-primary',
					'data'  => array(
						'action' => 'save_product',
					),
				),
			),
		),
		'msg-add-options-end'   => array(
			'type' => 'sectionend',
			'id'   => 'yith-pmhpp-add-options',
		),
	),
);
return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
