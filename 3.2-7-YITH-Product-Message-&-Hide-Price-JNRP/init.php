<?php
/**
 * Plugin Name: JNRP_3.2-7 Product Message and Hide Price Plugin
 * Description:
 * Version: 1.0.0
 * Author: Javier Nicolás Rodríguez Pérez.
 * Author URI: https://yithemes.com/
 * Text Domain: yith-pmhpp-text-domain
 *
 * @package yith_formacion
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Before all, check if defined ABSPATH.
}

/*Create some constant where defined PATH for Style, Assets, Templates, Views */
if ( ! defined( 'YITH_PMHPP_VERSION' ) ) {
	define( 'YITH_PMHPP_VERSION', '1.0.0' );
}
if ( ! defined( 'YITH_PMHPP_DIR_URL' ) ) {
	define( 'YITH_PMHPP_DIR_URL', plugin_dir_url( __FILE__ ) );
}
if ( ! defined( 'YITH_PMHPP_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PMHPP_DIR_ASSETS_URL', YITH_PMHPP_DIR_URL . 'assets' );
}
if ( ! defined( 'YITH_PMHPP_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PMHPP_DIR_ASSETS_CSS_URL', YITH_PMHPP_DIR_ASSETS_URL . '/css' );
}
if ( ! defined( 'YITH_PMHPP_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PMHPP_DIR_ASSETS_JS_URL', YITH_PMHPP_DIR_ASSETS_URL . '/js' );
}
if ( ! defined( 'YITH_PMHPP_DIR_PATH' ) ) {
	define( 'YITH_PMHPP_DIR_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'YITH_PMHPP_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PMHPP_DIR_INCLUDES_PATH', YITH_PMHPP_DIR_PATH . 'includes' );
}
if ( ! defined( 'YITH_PMHPP_DIR_LANGUAGES_PATH' ) ) {
	define( 'YITH_PMHPP_DIR_LANGUAGES_PATH', YITH_PMHPP_DIR_PATH . 'languages' );
}
if ( ! defined( 'YITH_PMHPP_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PMHPP_DIR_TEMPLATES_PATH', YITH_PMHPP_DIR_PATH . 'templates' );
}
if ( ! defined( 'YITH_PMHPP_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PMHPP_DIR_VIEWS_PATH', YITH_PMHPP_DIR_PATH . 'views' );
}

if ( ! defined( 'YITH_PMHPP_INIT' ) ) {
	define( 'YITH_PMHPP_INIT', plugin_basename( __FILE__ ) );
}
if ( ! defined( 'YITH_PMHPP_FILE' ) ) {
	define( 'YITH_PMHPP_FILE', __FILE__ );
}
if ( ! defined( 'YITH_PMHPP_SLUG' ) ) {
	define( 'YITH_PMHPP_SLUG', 'yith-pmhpp-text-domain' );
}
if ( ! defined( 'YITH_PMHPP_SECRET_KEY' ) ) {
	define( 'YITH_PMHPP_SECRET_KEY', 'zd9egFgFdF1D8Azh2ifK' );
}

if ( ! function_exists( 'yith_pmhpp_init_classes' ) ) {
	/**
	 * Include the scripts
	 */
	function yith_pmhpp_init_classes() {
		load_plugin_textdomain( 'yith-plugin-testimonial', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PMHPP_DIR_INCLUDES_PATH . '/class-yith-pmhpp-init.php';

		if ( class_exists( 'YITH_PMHPP_INIT' ) ) {
			/*
			*	Call the main function
			*/
			yith_pmhpp_init();
		}
	}
}
add_action( 'yith_pmhpp_init_hook', 'yith_pmhpp_init_classes', 11 );

if ( ! function_exists( 'yith_pmhpp_install_admin_notice' ) ) {
	/**
	 * Print an admin notice if WooCommerce is deactivated
	 *
	 * @author Carlos Rodriguez <carlos.rodriguez@yourinspiration.it>
	 * @since 1.0
	 * @return void
	 * @use admin_notices hooks
	 */
	function yith_pmhpp_install_admin_notice() { ?>
		<div class="error">
			<p><?php echo esc_html_x( 'YITH Product Message and Hide Price Plugin for WooCommerce is enabled but not effective. It requires WooCommerce in order to work.', 'Alert Message: WooCommerce requires', 'yith-pmhpp-text-domain' ); ?></p>
		</div>
		<?php
	}
}

if ( ! function_exists( 'yith_wcgpf_install' ) ) {
	/**
	 * Check if WooCommerce is activated
	 *
	 * @author Carlos Rodriguez <carlos.rodriguez@yourinspiration.it>
	 * @since 1.0
	 * @return void
	 * @use admin_notices hooks
	 */
	function yith_wcgpf_install() {
		if ( ! function_exists( 'WC' ) ) {
			add_action( 'admin_notices', 'yith_pmhpp_install_admin_notice' );
		} else {
			do_action( 'yith_pmhpp_init_hook' );
		}
	}
}
add_action( 'plugins_loaded', 'yith_wcgpf_install', 11 );

if ( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_PMHPP_DIR_PATH . 'plugin-fw/init.php' ) ) {
	require_once YITH_PMHPP_DIR_PATH . 'plugin-fw/init.php';
}
yit_maybe_plugin_fw_loader( YITH_PMHPP_DIR_PATH );

if ( ! function_exists( 'yith_plugin_registration_hook' ) ) {
	require_once 'plugin-fw/yit-plugin-registration-hook.php';
}

register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );

if ( ! function_exists( 'yit_deactive_free_version' ) ) {
	require_once 'plugin-fw/yit-deactive-plugin.php';
}

yit_deactive_free_version( 'MY_PLUGIN_FREE_INIT', plugin_basename( __FILE__ ) );
