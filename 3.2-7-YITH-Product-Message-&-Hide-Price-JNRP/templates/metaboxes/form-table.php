<?php
/**
 * This file belongs to the Product Message and Hide Price Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

?>
<form method="post">
	<table class="form-table yith-pmhpp-table">
		<tbody>
			<?php foreach ( $fields as $field ) : ?>
				<tr valign="top" class="yith-plugin-fw-panel-wc-row <?php echo esc_attr( $field['type'] ); ?>">
					<?php if ( ! empty( $field['title'] ) ) : ?>
						<th scope="row" class="titledesc">
							<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo wp_kses_post( $field['title'] ); ?></label>
						</th>
						<td class="forminp forminp-<?php echo esc_attr( $field['type'] ); ?>">
							<?php yith_plugin_fw_get_field( $field, true ); ?>
							<span class="description description-<?php echo esc_attr( $field['id'] ); ?>">
								<?php echo ! empty( $field['desc'] ) ? wp_kses_post( $field['desc'] ) : ''; ?>
							</span>
						</td>
					<?php else : ?>
						<td colspan="2">
							<?php yith_plugin_fw_get_field( $field, true ); ?>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</form>
