<?php
/**
 * This file belongs to the Product Message and Hide Price Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_PMHPP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

if ( ! class_exists( 'YITH_PMHPP_MESSAGE_LIST_TABLE' ) ) {

	/**
	 * Custom post types.
	 */
	class YITH_PMHPP_MESSAGE_LIST_TABLE extends WP_List_Table {

		/**
		 * Main Instance
		 *
		 * @var YITH_PMHPP_MESSAGE_LIST_TABLE
		 * @since 1.0
		 * @access private
		 */

		private $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_PMHPP_MESSAGE_LIST_TABLE
		 * @since 1.0
		 * @access public
		 */
		public $post_type = 'yith-pmhpp-message';


		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PMHPP_MESSAGE_LIST_TABLE Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public function get_instance() {
			return ! is_null( $this->$instance ) ? $this->$instance : $this->$instance = new self();
		}

		/**
		 * YITH_PMHPP_MESSAGE_LIST_TABLE constructor.
		 */
		public function __construct() {
			parent::__construct(
				array(
					'singular' => __( 'Admin message', 'yith-pmhpp-text-domain' ), // Singular name of the listed records.
					'plural'   => __( 'Admin messages', 'yith-pmhpp-text-domain' ), // Plural name of the listed records.
					'ajax'     => false,
				)
			);
		}

		/**
		 * Handles data query and filter, sorting, and pagination.
		 */
		public function prepare_items() {
			$columns  = $this->get_columns();
			$hidden   = $this->get_hidden_columns();
			$sortable = $this->get_sortable_columns();
			// TODO: Para ordenar por las columnas que muestro tendria que recoger todos los meta correspondientes para consultar su info.

			/** Process bulk action */
			$this->process_bulk_action();

			$per_page     = $this->get_items_per_page( 'messages_per_page', 20 );
			$current_page = $this->get_pagenum();
			$total_items  = self::record_count();

			$this->set_pagination_args(
				array(
					'total_items' => $total_items,
					'per_page'    => $per_page,
				)
			);

			//$data = $this->table_data( $per_page, $current_page );
			//usort( $data, array( $this, 'sort_data' ) );

			$this->_column_headers = array( $columns, $hidden/*, $sortable*/ );
			$this->items           = self::table_data( $per_page, $current_page );
		}

		/**
		 * Retrieve table data from the database
		 *
		 * @param int $per_page .
		 * @param int $page_number .
		 *
		 * @return mixed
		 */
		public function table_data( $per_page = 20, $page_number = 1 ) {
			global $wpdb;
			global  $woocommerce;

			$offset = ( $page_number - 1 ) * $per_page; // TODO: Use offset in query.
			$sql    =
				"SELECT post_id, meta_id
				FROM {$wpdb->prefix}postmeta
				WHERE meta_key LIKE 'yith_pmhpp_data'
				LIMIT $per_page OFFSET $offset";

			$ids = $wpdb->get_results( $sql, 'ARRAY_A' );

			$result = array();
			foreach ( $ids as $row ) {
				$product = wc_get_product( $row['post_id'] );
				$data    = $product->get_meta( 'yith_pmhpp_data' );
				array_push(
					$result,
					array(
						'ID'         => $row['meta_id'],
						'product_id' => $row['post_id'],
						'meta_value' => $data['enable'],
						'post_name'  => $product->get_title(),
					)
				);
			}
			return $result;
		}

		/**
		 * Returns the count of records in the database.
		 *
		 * @return null|string
		 */
		public static function record_count() {
			global $wpdb;
			$sql =
				"SELECT COUNT(*)
				FROM {$wpdb->prefix}postmeta
				WHERE meta_key LIKE 'yith_pmhpp_data'";
			return $wpdb->get_var( $sql );
		}

		/**
		 * Render the bulk edit checkbox
		 *
		 * @param array $item
		 *
		 * @return string
		 */
		public function column_cb( $item ) {
			return sprintf(
				'<input type="checkbox" name="bulk-id[]" value="%s" />',
				$item['ID']
			);
		}

		/**
		 * Define what data to show on each column of the table.
		 *
		 * @param Array  $item Data.
		 * @param String $column_name - Current column name.
		 *
		 * @return Mixed
		 */
		public function column_default( $item, $column_name ) {
			switch ( $column_name ) {
				case 'product_id':
				case 'ID':
					return $item[ $column_name ];
				case 'post_name':
					$nonce   = wp_create_nonce( 'nonce_action_links' );
					$title   = '<strong>' . $item['post_name'] . '</strong>';
					$actions = array(
						'delete' => sprintf(
							'<a href="?page=%s&action=%s&message=%s&_wpnonce=%s">' . __( 'Delete', 'yith-pmhpp-text-domain' ) . '</a>',
							esc_attr( $_REQUEST['page'] ),
							'delete',
							absint( $item['ID'] ),
							$nonce
						),
						'edit'   => sprintf(
							'<a href="?page=%s&action=%s&message=%s&_wpnonce=%s">' . __( 'Edit', 'yith-pmhpp-text-domain' ) . '</a>',
							esc_attr( $_REQUEST['page'] ),
							'edit',
							absint( $item['product_id'] ),
							$nonce
						),
					);
					return $title . $this->row_actions( $actions );
				case 'meta_value':
					$onoff_field = array(
						'id'    => "yith_pmhpp_enable[$item[product_id]]",
						'name'  => "yith_pmhpp_enable[$item[product_id]]",
						'class' => 'yith-pmhpp-table-onoff',
						'type'  => 'onoff',
						'value' => $item['meta_value'],
					);
					return yith_plugin_fw_get_field( $onoff_field, true );
				default:
					return print_r( $item, true );
			}
		}

		/**
		 *  Associative array of columns
		 *
		 * @return array
		 */
		public function get_columns() {
			$columns = array(
				'cb'         => '<input type="checkbox" />',
				'post_name'  => __( 'Product', 'yith-pmhpp-text-domain' ),
				'meta_value' => __( 'Apply rule', 'yith-pmhpp-text-domain' ),
				'ID'         => __( 'ID', 'yith-pmhpp-text-domain' ),
				'product_id' => __( 'Product Id', 'yith-pmhpp-text-domain' ),
			);

			return $columns;
		}

		/**
		 * Define which columns are hidden.
		 *
		 * @return Array
		 */
		public function get_hidden_columns() {
			return array(
				'ID'         => 'ID',
				'product_id' => 'product_id',
			);
		}

		/**
		 * Define the sortable columns.
		 *
		 * @return Array
		 */
		public function get_sortable_columns() {
			return array(
				'post_name'  => array( 'post_name', true ),
				'meta_value' => array( 'meta_value', false ),
			);
		}

		/**
		 * Returns an associative array containing the bulk action
		 *
		 * @return array
		 */
		public function get_bulk_actions() {
			$actions = array(
				'bulk-delete' => __( 'Delete msg', 'yith-pmhpp-text-domain' ),
				'bulk-edit'   => __( 'Edit msg', 'yith-pmhpp-text-domain' ),
			);
			return $actions;
		}

		/**
		 * Delete a message record.
		 *
		 * @param int $id post ID.
		 */
		public function delete_message( $id ) {
			global $wpdb;
			$wpdb->delete(
				"{$wpdb->prefix}postmeta",
				array(
					'meta_id' => $id,
				)
			);
		}

		/**
		 * Bulk actions
		 */
		public function process_bulk_action() {
			// Delete.
			if ( 'delete' === $this->current_action() ) {
				$nonce = esc_attr( $_REQUEST['_wpnonce'] );
				if ( ! wp_verify_nonce( $nonce, 'nonce_action_links' ) ) {
					die( 'Go get a life script kiddies' );
				} else {
					$this->delete_message( absint( $_GET['message'] ) );
					wp_safe_redirect( esc_url( add_query_arg() ) );
					exit;
				}
			}
			// Delete bulk.
			if ( ( isset( $_POST['action'] ) && 'bulk-delete' === $_POST['action'] )
				|| ( isset( $_POST['action2'] ) && 'bulk-delete' === $_POST['action2'] )
			) {
				$delete_ids = esc_sql( $_POST['bulk-id'] );
				foreach ( $delete_ids as $id ) {
					$this->delete_message( $id );
				}
				wp_safe_redirect( esc_url( add_query_arg() ) );
				exit;
			}
			// Edit.
			$tab     = 'msg-add';
			$tab_url = admin_url( 'admin.php?page=' . YITH_PMHPP_SLUG . "_panel&tab=$tab" );
			if ( 'edit' === $this->current_action() ) {
				$nonce = esc_attr( $_REQUEST['_wpnonce'] );
				if ( ! wp_verify_nonce( $nonce, 'nonce_action_links' ) ) {
					die( 'Go get a life script kiddies' );
				} else {
					$this->edit_message( absint( $_GET['message'] ) );
					$args = array(
						'edit'     => 'edit',
						'products' => array( absint( $_GET['message'] ) ),
					);
					wp_safe_redirect( esc_url_raw( add_query_arg( $args, $tab_url ) ) );
					exit;
				}
			}
			// Edit bulk.
			if ( ( isset( $_POST['action'] ) && 'bulk-edit' === $_POST['action'] )
				|| ( isset( $_POST['action2'] ) && 'bulk-edit' === $_POST['action2'] )
			) {
				global $wpdb;
				$sql      =
					"SELECT post_id
					FROM {$wpdb->prefix}postmeta
					WHERE meta_id IN ('" . implode( ', ', $_POST['bulk-id'] ) . "')";
				$response = $wpdb->get_results( $sql, 'ARRAY_A' );
				$ids      = array();
				foreach ( $response as $id ) {
					$ids[] = $id['post_id'];
				}
				$args = array(
					'edit'     => 'edit',
					'products' => $ids,
				);
				wp_safe_redirect( esc_url_raw( add_query_arg( $args, $tab_url ) ) );
				exit;
			}
		}

		/**
		 * Allows you to sort the data by the variables set in the $_GET.
		 *
		 * @param Array $a .
		 * @param Array $b .
		 *
		 * @return Mixed
		 */
		private function sort_data( $a, $b ) {
			// Set defaults.
			$orderby = 'post_name';
			$order   = 'asc';

			// If orderby is set, use this as the sort column.
			if ( ! empty( $_GET['orderby'] ) ) {
				$orderby = sanitize_text_field( wp_unslash( $_GET['orderby'] ) );
			}

			// If order is set use this as the order.
			if ( ! empty( $_GET['order'] ) ) {
				$order = sanitize_text_field( wp_unslash( $_GET['order'] ) );
			}

			$result = strcmp( $a[ $orderby ], $b[ $orderby ] );

			if ( 'asc' === $order ) {
				return $result;
			}

			return -$result;
		}
	}
}
