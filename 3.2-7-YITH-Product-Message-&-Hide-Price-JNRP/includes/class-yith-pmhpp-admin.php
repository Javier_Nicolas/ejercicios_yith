<?php
/**
 * This file belongs to the Product Message and Hide Price Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_PMHPP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMHPP_ADMIN' ) ) {

	/**
	 * Admin related.
	 */
	class YITH_PMHPP_ADMIN {

		/**
		 * Main Instance
		 *
		 * @var YITH_PMHPP_ADMIN
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/** The panel
		 *
		 * @var YIT_Plugin_Panel $panel .
		 */
		private $panel;

		/** The page
		 *
		 * @var Panel $pannel_page .
		 */
		protected $panel_page = 'yith_pmhpp_panel';

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PMHPP_ADMIN Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PMHPP_ADMIN constructor.
		 */
		private function __construct() {
			// Admin menu.
			add_action( 'admin_menu', array( $this, 'menus' ), 5 );
			add_action( 'yith_pmhpp_admin_messages_tab_display', array( $this, 'admin_messages_tab_display' ), 10, 1 );

			// Framework.
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_PMHPP_DIR_PATH . '/' . basename( YITH_PMHPP_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );

			// AJAX.
			add_action( 'wp_ajax_yith-pmhpp-save-message', array( $this, 'save_message' ), 10, 1 );
			add_action( 'wp_ajax_yith-pmhpp-table-onoff', array( $this, 'table_onoff' ), 10, 1 );

			add_action( 'admin_enqueue_scripts', array( $this, 'load_tabs' ) );
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_PMHPP_SLUG );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_PMHPP_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_PMHPP_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}

		/**
		 *  Create menus for testimonials
		 */
		public function menus() {
			if ( ! empty( $this->panel ) ) {
				return;
			}

			$admin_tabs = array(
				'msg-table' => __( 'Messages table', 'yith-pmhpp-text-domain' ),
				'msg-add'   => __( 'Add message', 'yith-pmhpp-text-domain' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH PMHPP Plugin', // this text MUST be NOT translatable
				'menu_title'         => 'YITH PMHPP Plugin', // this text MUST be NOT translatable
				'plugin_description' => __( 'Product Message and Hide Price plugin description', 'yith-pmhpp-text-domain' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => YITH_PMHPP_SLUG,
				'parent_page'        => 'yith_plugin_panel',
				'page'               => YITH_PMHPP_SLUG . '_panel',
				'admin-tabs'         => $admin_tabs,
				'plugin-url'         => YITH_PMHPP_DIR_PATH,
				'options-path'       => YITH_PMHPP_DIR_PATH . 'plugin-options',
			);

			$this->panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}

		/**
		 * Add the button action from tabs.
		 */
		public function save_message() {
			// Nonce verification.
			if ( isset( $_POST['_yith_pmhpp_nonce'] ) ) {
				if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['_yith_pmhpp_nonce'] ) ), 'yith_pmhpp_add_nonce' ) ) {
					print esc_html__( 'Sorry, your nonce did not verify.', 'yith-pmhpp-text-domain' );
					exit;
				}
			} else {
				return;
			}

			$params = array();
			parse_str( $_POST['data'], $params );

			$data               = array();
			$data['enable']     = isset( $params['yith_pmhpp_enable'] ) ? $params['yith_pmhpp_enable'] : 'yes';
			$data['message']    = isset( $params['yith_pmhpp_message'] ) ? $params['yith_pmhpp_message'] : '';
			$data['position']   = isset( $params['yith_pmhpp_position'] ) ? $params['yith_pmhpp_position'] : 'bf-cart';
			$data['hide_price'] = isset( $params['yith_pmhpp_hide_price'] ) ? $params['yith_pmhpp_hide_price'] : 'no';

			if ( empty( $params['yith_pmhpp_products'] ) ) {
				echo 'no_product';
				return;
			}

			global  $woocommerce;
			foreach ( $params['yith_pmhpp_products'] as $product_id ) {
				$product = wc_get_product( $product_id );
				$product->update_meta_data( 'yith_pmhpp_data', $data );
				$product->save();
			}

			echo 'success';
		}

		/**
		 * Add the onoff action from tabs.
		 */
		public function table_onoff() {
			// Nonce verification.
			if ( isset( $_POST['_yith_pmhpp_nonce'] ) ) {
				if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['_yith_pmhpp_nonce'] ) ), 'yith_pmhpp_add_nonce' ) ) {
					print esc_html__( 'Sorry, your nonce did not verify.', 'yith-pmhpp-text-domain' );
					exit;
				}
			} else {
				return;
			}

			if ( ! isset( $_POST['name'] ) ) {
				echo 'no_product';
				return;
			}
			$product_id = substr_replace( substr_replace( $_POST['name'], '', 0, 18 ), '', -1, 1 );

			global  $woocommerce;
			$product        = wc_get_product( $product_id );
			$data           = $product->get_meta( 'yith_pmhpp_data' );
			$data['enable'] = isset( $_POST['value'] ) ? $_POST['value'] : 'yes';
			$product->update_meta_data( 'yith_pmhpp_data', $data );
			$product->save();

			echo 'success';
		}

		/**
		 * Styles and scripts.
		 */
		public function load_tabs() {
			if( ! isset( $_GET['page'] ) || 'yith-pmhpp-text-domain_panel' !== $_GET['page'] ) {
				return;
			}
			wp_register_style(
				'yith-pmhpp-load-tabs-css',
				YITH_PMHPP_DIR_ASSETS_CSS_URL . '/yith-pmhpp-tabs.css',
				array(),
				false
			);
			wp_enqueue_style( 'yith-pmhpp-load-tabs-css' );
			wp_register_script(
				'yith-pmhpp-load-tabs-js',
				YITH_PMHPP_DIR_ASSETS_JS_URL . '/yith-pmhpp-options-tab.js',
				array( 'jquery' ),
				false,
				true
			);
			wp_enqueue_script( 'yith-pmhpp-load-tabs-js' );
			wp_localize_script(
				'yith-pmhpp-load-tabs-js',
				'pmhpp_ajax_vars',
				array(
					'ajax_url'            => admin_url( 'admin-ajax.php' ),
					'_yith_pmhpp_nonce'   => wp_create_nonce( 'yith_pmhpp_add_nonce' ),
					'action_save_message' => 'yith-pmhpp-save-message',
					'action_table_onoff'  => 'yith-pmhpp-table-onoff',
					'message_table_url'   => admin_url( 'admin.php?page=' . YITH_PMHPP_SLUG . '_panel&tab=msg-table' ),
					'confirm_msg'         => __( 'If you continue, you will delet all progress in this page.\nAre you sure?', 'yith-pmhpp-text-domain' ),
					'no_product_msg'      => __( 'There\'s no selected product\nPlease, select at least one before save', 'yith-pmhpp-text-domain' ),
				)
			);
		}
	}
}


