<?php
/**
 * This file belongs to the Product Message and Hide Price Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

// HERE SET FUNCTIONS TO USE ON YOUR PROJECT LIKE Template functions.

if ( ! function_exists( 'yith_pmhpp_get_template' ) ) {
	/**
	 * Include templates
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function yith_pmhpp_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_PMHPP_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_pmhpp_get_view' ) ) {
	/**
	 * Include views
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function yith_pmhpp_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_PMHPP_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_pmhpp_get_template' ) ) {
	/**
	 * Include templates
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function yith_pmhpp_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_CR_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
