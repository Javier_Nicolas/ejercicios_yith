<?php
/**
 * This file belongs to the Product Message and Hide Price Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_PMHPP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMHPP_INIT' ) ) {

	/**
	 * Inicializador del plugin.
	 */
	class YITH_PMHPP_INIT {

		/**
		 * Main Instance
		 *
		 * @var YITH_PMHPP_INIT
		 * @since 1.0
		 * @access private
		 */
		private static $instance;

		/**
		 * Main Admin Instance
		 *
		 * @var YITH_PMHPP_INIT_ADMIN
		 * @since 1.0
		 */
		public $admin = null;

		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_PMHPP_INIT_FRONTEND
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PMHPP_INIT Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6 .
		}

		/**
		 * YITH_PMHPP_INIT constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_pmhpp_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
					),
					'admin'    => array(
						'includes/class-yith-pmhpp-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-pmhpp-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			/*
				Here set any other hooks ( actions or filters you'll use on this class)
			*/

			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );

			// Finally call the init function.
			$this->init();
		}

		/**
		 * Plugin framework loader.
		 */
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once $plugin_fw_file;
				}
			}
		}

		/**
		 * Register plugins for activation tab
		 */
		public function register_plugin_for_activation() {
			if ( function_exists( 'YIT_Plugin_Licence' ) ) {
				YIT_Plugin_Licence()->register( YITH_PMHPP_INIT, YITH_PMHPP_SECRET_KEY, YITH_PMHPP_SLUG );
			}
		}

		/**
		 * Register plugins for update tab
		 */
		public function register_plugin_for_updates() {
			if ( function_exists( 'YIT_Upgrade' ) ) {
				YIT_Upgrade()->register( YITH_PMHPP_SLUG, YITH_PMHPP_INIT );
			}
		}

		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param array $main_classes array The require classes file path.
		 *
		 * @author Javier Nicolás Rodríguez Pérez
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_PMHPP_DIR_PATH . $class ) ) {
						require_once YITH_PMHPP_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 *
		 * @author Javier Nicolás Rodríguez Pérez
		 * @since  1.0
		 **/
		public function init_classes() {}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Javier Nicolás Rodríguez Pérez
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_PMHPP_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_PMHPP_Frontend::get_instance();
			}
		}

	}
}

if ( ! function_exists( 'yith_pmhpp_init' ) ) {
	/**
	 * Get the YITH_PMHPP_INIT instance
	 *
	 * @return YITH_PMHPP_INIT
	 */
	function yith_pmhpp_init() {
		return YITH_PMHPP_INIT::get_instance();
	}
}
