<?php
/**
 * This file belongs to the Product Message and Hide Price Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_PMHPP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PMHPP_FRONTEND' ) ) {

	/**
	 * Frontend related.
	 */
	class YITH_PMHPP_FRONTEND {

		/**
		 * Main Instance
		 *
		 * @var YITH_PMHPP_FRONTEND
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PMHPP_FRONTEND Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PMHPP_Frotend constructor.
		 */
		private function __construct() {
			add_action( 'woocommerce_before_single_product', array( $this, 'select_action' ), 5 );
			add_action( 'woocommerce_after_shop_loop_item_title', array( $this, 'prevent_price_shop' ), 9 );

			add_filter( 'woocommerce_cart_item_price', array( $this, 'prevent_price_cart' ), 10, 3 );
			add_filter( 'woocommerce_cart_product_subtotal', array( $this, 'prevent_subtotal_cart' ), 10, 3 );
		}

		/**
		 * Function to select where the message should appear.
		 */
		public function select_action() {
			global  $woocommerce;
			$product = wc_get_product( get_the_ID() );
			$data    = $product->get_meta( 'yith_pmhpp_data' );
			if ( isset( $data['enable'] ) && 'yes' === $data['enable'] ) {
				switch ( $data['position'] ) {
					case 'bf-title':
						add_action( 'woocommerce_single_product_summary', array( $this, 'message_display' ), 4, 1 );
						break;
					case 'af-title':
						add_action( 'woocommerce_single_product_summary', array( $this, 'message_display' ), 6, 1 );
						break;
					case 'bf-price':
						add_action( 'woocommerce_single_product_summary', array( $this, 'message_display' ), 9, 1 );
						break;
					case 'af-price':
						add_action( 'woocommerce_single_product_summary', array( $this, 'message_display' ), 11, 1 );
						break;
					case 'bf-cart':
						add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'message_display' ), 10, 1 );
						break;
					case 'af-cart':
						add_action( 'woocommerce_after_add_to_cart_button', array( $this, 'message_display' ), 10, 1 );
						break;
					default:
						add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'message_display' ), 10, 1 );
				}
				if ( isset( $data['hide_price'] ) && 'yes' === $data['hide_price'] ) {
					remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
				}
			}
		}

		/**
		 * Function to select where the message should appear.
		 */
		public function message_display() {
			global  $woocommerce;
			$product = wc_get_product( get_the_ID() );
			$data    = $product->get_meta( 'yith_pmhpp_data' );
			?>
				<div class="yith-pmhpp-product-message-container">
					<p><?php echo esc_html( $data['message'] ); ?></p>
				</div>
			<?php
		}

		/**
		 * Function to prevent price display if necesary in shop.
		 */
		public function prevent_price_shop() {
			global  $woocommerce;
			$product = wc_get_product( get_the_ID() );
			$data    = $product->get_meta( 'yith_pmhpp_data' );
			if ( isset( $data['hide_price'] ) && 'yes' === $data['hide_price'] ) {
				remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
			}
		}

		/**
		 * Function to prevent price display if necesary in cart.
		 */
		public function prevent_price_cart( $price, $cart_item, $cart_item_key ) {
			$product   = wc_get_product( $cart_item['product_id'] );
			$meta_data = $product->get_meta( 'yith_pmhpp_data' );
			if ( isset( $meta_data['hide_price'] ) && 'yes' === $meta_data['hide_price'] ) {
				$price = '';
			}
			return $price;
		}

		/**
		 * Function to prevent price display if necesary in cart.
		 */
		public function prevent_subtotal_cart( $product_subtotal, $product, $quantity ) {
			$meta_data = $product->get_meta( 'yith_pmhpp_data' );
			if ( isset( $meta_data['hide_price'] ) && 'yes' === $meta_data['hide_price'] ) {
				$product_subtotal = '';
			}
			return $product_subtotal;
		}
	}
}
