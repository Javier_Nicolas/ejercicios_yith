jQuery(
	function($){
		var function_call = new URLSearchParams( window.location.search );
		console.log( function_call.get( 'edit' ) );
		console.log( function_call.get( 'products' ) );
		// Init.
		//$( '.yith-pmhpp-add-msg-container' ).fadeOut();
		// Hide buttons.
		$( '#yit_panel_wc_options_nonce' ).parent().fadeOut();
		$( '#yith_wc_reset_options_nonce' ).parent().fadeOut();
		//Spinners
		$( 'div.yith-pmhpp-table-onoff' ).after( '<div class="yith-pmhpp-load-dual-ring"></div>' );
		$( 'div.yith-pmhpp-load-dual-ring' ).fadeOut();
		$( 'div.yith-pmhpp-load-dual-ring-large' ).fadeOut();
		//
		$( 'input[data-action="go_back"]' ).click(
			function(e){
				if ( confirm( pmhpp_ajax_vars.confirm_msg ) ) {
					$( '.yith-pmhpp-add-msg-container' ).fadeOut();
					$( '.yith-pmhpp-msg-table-container' ).fadeIn();
				}
			}
		);
		$( 'input[data-action="save_product"]' ).click(
			function(e){
				e.preventDefault();
				$.ajax(
					{
						type : "post",
						url : pmhpp_ajax_vars.ajax_url,
						data : {
							_yith_pmhpp_nonce: pmhpp_ajax_vars._yith_pmhpp_nonce,
							action: pmhpp_ajax_vars.action_save_message,
							data: $( this ).closest( 'form' ).serialize(),
						},
						beforeSend: function (qXHR, settings) {
							$( '.yith-pmhpp-load-dual-ring-large' ).fadeIn();
						},
						complete: function () {
							$( '.yith-pmhpp-load-dual-ring-large' ).fadeOut();
						},
						error: function(response){
							console.log( response );
						},
						success: function(response) {
							var result = response.slice( 0,-1 );
							switch (result) {
								case 'success':
									window.location.replace( pmhpp_ajax_vars.message_table_url );
									break;
								case 'no_product':
									alert( pmhpp_ajax_vars.no_product_msg );
									break;
								default:
									console.log( result );
							}
							// TODO: prevenir alertas.
						}
					}
				)

			}
		);
		$( 'div.yith-pmhpp-table-onoff' ).find( 'input.on_off' ).change(
			function(e){
				var container = $( this ).closest( '.yith-pmhpp-table-onoff' );
				var loading   = $( container ).next();
				$.ajax(
					{
						type : "post",
						url : pmhpp_ajax_vars.ajax_url,
						data : {
							_yith_pmhpp_nonce: pmhpp_ajax_vars._yith_pmhpp_nonce,
							action: pmhpp_ajax_vars.action_table_onoff,
							value: $( this ).val(),
							name: $( this ).attr( 'name' ),
						},
						beforeSend: function (qXHR, settings) {
							$( container ).fadeOut();
							$( loading ).fadeIn();
						},
						complete: function () {
							$( loading ).fadeOut();
							$( container ).fadeIn();
						},
						error: function(response){
							console.log( response );
						},
						success: function(response) {
						}
					}
				)

			}
		);
	}
);
