jQuery(
	function($){
		$( '.yith-ajax-test-loading' ).fadeIn();
		$( '.yith-ajax-test-button' ).click(
			function(){
				//La llamada AJAX
				$.ajax(
					{
						type : "post",
						url : wp_ajax_test_vars.ajax_url,
						data : {
							action: "notify_button_click",
							message : "Button has been clicked!"
						},
						beforeSend: function (qXHR, settings) {
							$( '.yith-ajax-test-loading' ).fadeIn();
						},
						complete: function () {
							$( '.yith-ajax-test-loading' ).fadeOut();
						},
						error: function(response){
							console.log( response );
						},
						success: function(response) {
							// Actualiza el mensaje con la respuesta
							$( '.yith-ajax-test-text' ).text( response.message );
						}
					}
				)

			}
		);
	}
);
