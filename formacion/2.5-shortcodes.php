<?php
/**
 * Plugin Name: JNRP_2.5_Shortcodes
 * Plugin URI:
 * Description: Ejercicio de shortcodes.
 * Author: Javier Nicolas Rodriguez Perez
 * Version: 1.0.0
 * Author URI: yithemes.com
 *
 * @package yith_formacion
 */

/**
 * Shortcode que muestra el contenido de un post.
 *
 * @param array $atts Argumentos, recibira el id.
 */
function content_post_by_id( $atts ) {
	if ( isset( $atts['id'] ) ) {
		return get_post( $atts['id'] )->post_content;
	} else {
		return 'Id no encontrado';
	}
}
add_shortcode( 'post_content', 'content_post_by_id' );

/**
 * Shortcode que muestra los 5 ultimos posts.
 */
function last_five_posts() {
	$posts   = wp_get_recent_posts();
	$content = '';
	for ( $i = 0; $i < 5; $i++ ) {
		$content .= '<p> .-' . $posts[ $i ]['post_title'] . '</p>';
	}
	return $content;
}
add_shortcode( 'lasts_posts', 'last_five_posts' );

/**
 * Shortcode que muestra la informacion de un usuario.
 *
 * @param array $atts Argumentos, recibira el id.
 */
function user_simple_data( $atts ) {
	$content = 'Id no encontrado';
	if ( isset( $atts['id'] ) ) {
		$user     = get_userdata( $atts['id'] );
		$content  = '<p> .- Nombre : ' . $user->user_firstname . '</p>';
		$content .= '<p> .- Apellidos : ' . $user->user_lastname . '</p>';
		$content .= '<p> .- Nick  : ' . $user->nickname . '</p>';
	}
	return $content;
}
add_shortcode( 'user_data', 'user_simple_data' );
