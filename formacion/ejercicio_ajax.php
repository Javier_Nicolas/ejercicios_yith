<?php
/**
 * Plugin Name: JNRP_Ejercicio_Ajax
 * Plugin URI:
 * Description: Ejercicio de Ajax.
 * Author: Javier Nicolas Rodriguez Perez
 * Version: 1.0.0
 * Author URI: yithemes.com
 *
 * @package yith_formacion
 */

// Hook para usuarios no logueados.
add_action( 'wp_ajax_nopriv_notify_button_click', 'yith_notify_button_click' );

// Hook para usuarios logueados.
add_action( 'wp_ajax_notify_button_click', 'yith_notify_button_click' );

if ( ! function_exists( 'yith_notify_button_click' ) ) {
	/**
	 * Función que procesa la llamada AJAX.
	 */
	function yith_notify_button_click() {
		// Check parameters.
		$message = isset( $_POST['message'] ) ? $_POST['message'] : false;
		if ( ! $message ) {
			wp_send_json( array( 'message' => __( 'Message not received :(', 'wpduf' ) ) );
		} else {
			wp_send_json( array( 'message' => __( 'Message received, greetings from server!', 'wpduf' ) ) );
		}
	}
}

if ( ! function_exists( 'yith_ajax_test_html' ) ) {
	/**
	 * Boton y parrafo para hacer la prueba.
	 */
	function yith_ajax_test_html() {
		return '<button class="yith-ajax-test-button">Click me</button><p class="yith-ajax-test-loading">Loading...</p><p class="yith-ajax-test-text">Nothing yet</p>';
	}
}
add_shortcode( 'ajax_test', 'yith_ajax_test_html' );

if ( ! function_exists( 'yith_ajax_test_enqueue_scripts' ) ) {
	/**
	 * Load js.
	 */
	function yith_ajax_test_enqueue_scripts() {
		wp_register_script(
			'ejercicio-ajax',
			plugin_dir_url( __FILE__ ) . '/ejercicio_ajax.js',
			array( 'jquery' ),
			false,
			true
		);
		wp_enqueue_script( 'ejercicio-ajax' );

		wp_localize_script(
			'ejercicio-ajax',
			'wp_ajax_test_vars',
			array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
			)
		);
	}
}
add_action( 'wp_enqueue_scripts', 'yith_ajax_test_enqueue_scripts' );

