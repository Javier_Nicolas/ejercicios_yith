<?php
/**
 * Plugin Name: JNRP_2.4_Styles_and_Scripts
 * Plugin URI:
 * Description: Ejercicio de añadir estilo con condiciones.
 * Author: Javier Nicolas Rodriguez Perez
 * Version: 1.0.0
 * Author URI: yithemes.com
 *
 * @package yith_formacion
 */

/**
 * Añadir un archivo css que cambia el color de texto de <p>.
 */
function change_text_color() {
	if ( is_single() ) {
		wp_register_style( // phpcs:ignore
			'change_textc',
			get_template_directory_uri() . '/assets/css/change_textc.css'
		);
		wp_enqueue_style( 'change_textc' );
	}
}
add_action( 'wp_enqueue_scripts', 'change_text_color' );
