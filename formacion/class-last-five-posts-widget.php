<?php
/**
 * Plugin Name: JNRP_2.6_Widgets_lasts_five
 * Plugin URI:
 * Description: Ejercicio de Widgets - Widget que muestra los ultimmos 5 posts.
 * Author: Javier Nicolas Rodriguez Perez
 * Version: 1.0.0
 * Author URI: yithemes.com
 *
 * @package yith_formacion
 */

add_action( 'widgets_init', 'register_last_five_posts_widget' );

/**
 * Registro del widget.
 */
function register_last_five_posts_widget() {
	register_widget( 'Last_Five_Posts_Widget' );
}

/**
 * Widget que muestra los ultimmos 5 posts.
 */
class Last_Five_Posts_Widget extends WP_Widget {
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'last_five_posts',
			'Last 5 posts',
			array( 'description' => __( 'Listar los 5 ultimos posts', 'text_domain' ) )
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args Argumentos.
	 * @param array $instance Instancia.
	 */
	public function widget( $args, $instance ) {
		esc_html( $args['before_widget'] );
		esc_html( '<h4>Ultimos 5 Posts</h4>' );
		$posts = wp_get_recent_posts();
		?>
			<ul>
				<?php for ( $i = 0; $i < 5; $i++ ) { ?>
				<li>
						<a href='<?php esc_url( get_permalink( $posts[ $i ]['ID'] ) ); ?>'>
						<?php esc_html( $posts[ $i ]['post_title'] ); ?>
						</a>
				</li>
				<?php } ?>
			</ul>
		<?php
		esc_html( $args['after_widget'] );
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options.
	 */
	public function form( $instance ) {}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 */
	public function update( $new_instance, $old_instance ) {}
}
?>
