<?php
/**
 * Plugin Name: JNRP_2.3_Hooks
 * Plugin URI:
 * Description: Ejercicio de hooks.
 * Author: Javier Nicolas Rodriguez Perez
 * Version: 1.0.0
 * Author URI: yithemes.com
 *
 * @package yith_formacion
 */

/**
 * Hook que muestra la fecha en cada post.
 *
 * @param string $content Contenido previo.
 */
function mostrar_fecha( $content ) {
	return '<p>Fecha: ' . get_post( get_the_ID() )->post_date . '</p>' . $content;
}
add_filter( 'the_content', 'mostrar_fecha', 6 );

/**
 * Hook que muestra el autor en cada post, pero cambiando el de dos posts concretos.
 *
 * @param string $content Contenido previo.
 */
function mostrar_autor( $content ) {
	switch ( get_the_ID() ) {
		case 15:
			$author = 'Javier Nicolás Pérez';
			break;
		case 18:
			$author = 'Juan Rodriguez';
			break;
		default:
			$author = get_the_author();
	}
	return '<p>Escrito por ' . $author . '</p>' . $content;
}
add_filter( 'the_content', 'mostrar_autor', 7 );

/**
 * Hook que muestra el titulo en cada post.
 *
 * @param string $content Contenido previo.
 */
function mostrar_titulo( $content ) {
	return '<p>"' . get_post( get_the_ID() )->post_title . '" </p>' . $content;
}
add_filter( 'the_content', 'mostrar_titulo', 8 );

/**
 * Hook que muestra los datos inventados en cada post que los tenga.
 *
 * @param string $content Contenido previo.
 */
function mostrar_metadata( $content ) {
	if ( count( get_post_meta( get_the_ID() ) ) > 1 && get_post_meta( get_the_ID(), 'number_code', true ) !== null ) {
		$content .= '<p>Codigo numerico: ' . get_post_meta( get_the_ID(), 'number_code', true ) . '</p>';
		$content .= '<p>Codigo comun: ' . get_post_meta( get_the_ID(), 'common_code', true ) . '</p>';
		$content .= '<p>Codigo id: ' . get_post_meta( get_the_ID(), 'id_code', true ) . '</p>';
	}
	return $content;
}
add_filter( 'the_content', 'mostrar_metadata', 9 );

/**
 * Hook que añade datos inventados a cada post creado.
 *
 * @param string $post_id Contenido previo.
 */
function datos_inventados( $post_id ) {
	update_post_meta( $post_id, 'number_code', wp_rand( 1, 99999 ) );
	update_post_meta( $post_id, 'common_code', 'codigo_comun' );
	update_post_meta( $post_id, 'id_code', $post_id );
}
add_action( 'save_post', 'datos_inventados' );


remove_action( 'the_content', 'mostrar_autor', 7 );
