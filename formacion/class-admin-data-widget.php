<?php
/**
 * Plugin Name: JNRP_2.6_Widgets_admin_data
 * Plugin URI:
 * Description: Ejercicio de Widgets - Widget que muestra los datos introducidos de usuario.
 * Author: Javier Nicolas Rodriguez Perez
 * Version: 1.0.0
 * Author URI: yithemes.com
 *
 * @package yith_formacion
 */

add_action( 'widgets_init', 'register_admin_data_widget' );

/**
 * Registro del widget.
 */
function register_admin_data_widget() {
	register_widget( 'Admin_Data_Widget' );
}

/**
 * Widget que muestra los datos introducidos de usuario.
 */
class Admin_Data_Widget extends WP_Widget {
	const CAMPOS = array(
		array(
			'name' => 'Nombre',
			'id'   => 'c_fname',
			'type' => 'text',
		),
		array(
			'name' => 'Apellidos',
			'id'   => 'c_lname',
			'type' => 'text',
		),
		array(
			'name' => 'Telefono',
			'id'   => 'c_tlf',
			'type' => 'text',
		),
		array(
			'name' => 'Direccion',
			'id'   => 'c_adress',
			'type' => 'text',
		),
	);

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'admin_data_widget',
			'Form admin data',
			array( 'description' => __( 'Formulario de datos del admin', 'text_domain' ) )
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args Argumentos.
	 * @param array $instance Instancia.
	 */
	public function widget( $args, $instance ) {
		esc_html( $args['before_widget'] );
		esc_html( '<h4>Datos Admin</h4>' );
		?>
		<ul>
			<?php foreach ( self::CAMPOS as $campo ) { ?>
				<li><?php esc_html( "{$campo['name']} .- {$instance[ $campo['id'] ]}" ); ?> </li>
			<?php } ?>
		</ul>
		<?php
		esc_html( $args['after_widget'] );
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options.
	 */
	public function form( $instance ) {
		foreach ( self::CAMPOS as $campo ) {
			$campo_aux = isset( $instance[ $campo['id'] ] ) ? $instance[ $campo['id'] ] : __( '', 'text_domain' ); // phpcs:ignore
			?>
				<p>
					<label for='<?php esc_html( $this->get_field_id( $campo['id'] ) ); ?>'>
						<?php esc_html( "{$campo['name']} :" ); ?>
					</label>
					<input class='widefat' type='<?php esc_html( $campo['type'] ); ?>'
						id='<?php esc_html( $this->get_field_id( $campo['id'] ) ); ?>'
						name='<?php esc_html( $this->get_field_name( $campo['id'] ) ); ?>'
						value='<?php esc_html( esc_attr( $campo_aux ) ); ?>'
					>
				</p>
			<?php
		}
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		foreach ( self::CAMPOS as $campo ) {
			$instance[ $campo['id'] ] = wp_strip_all_tags( $new_instance[ $campo['id'] ] );
		}
		return $instance;
	}
}
?>
