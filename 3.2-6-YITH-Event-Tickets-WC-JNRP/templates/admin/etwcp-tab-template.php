<?php
/**
 * This file belongs to the Event Tickets for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $product_data optional previous data.
 */

?>
<div id="yith_etwcp_panel" class="panel woocommerce_options_panel">
	<div class="options_group show_if_simple show_if_variable yith-etwcp-tab-container">
		<div class="yith-etwcp-tab">
			<h5 class="yith-etwcp-tab-title"><?php esc_html_e( 'Fields', 'yith-etwcp-text-domain' ); ?></h5>
			<?php
			wp_nonce_field( '_yith_etwcp_admin_data', '_yith_etwcp_nonce' );
			echo esc_html(
				yith_etwcp_get_view(
					'/input-fields/checkbox.php',
					array(
						'field_name'         => '_yith_etwcp[name]',
						'field_name_display' => __( 'Name required', 'yith-etwcp-text-domain' ),
						'classes'            => array( 'yith-etwcp-enable', 'yith-etwcp-enable-name' ),
						'checked'            => isset( $product_data['name'] ) ? $product_data['name'] : false,
					)
				)
			);
			echo esc_html(
				yith_etwcp_get_view(
					'/input-fields/checkbox.php',
					array(
						'field_name'         => '_yith_etwcp[surname]',
						'field_name_display' => __( 'Surname required', 'yith-etwcp-text-domain' ),
						'classes'            => array( 'yith-etwcp-enable', 'yith-etwcp-enable-name' ),
						'checked'            => isset( $product_data['surname'] ) ? $product_data['surname'] : false,
					)
				)
			);
			?>
		</div>
	</div>
</div>
