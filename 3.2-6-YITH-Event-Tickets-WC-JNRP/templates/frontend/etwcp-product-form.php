<?php
/**
 * This file belongs to the Event Tickets for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

?>
<div class="yith-etwcp-container">
	<div class="yith-etwcp-list">
		<div class="yith-etwcp-single-ticket yith-etwcp-single-ticket-template">
			<?php
			echo esc_html(
				yith_etwcp_get_view(
					'/input-fields/text.php',
					array(
						'field_name'         => '_yith_etwcp[--yith_template--][name]',
						'field_name_display' => __( 'Name', 'yith-etwcp-text-domain' ),
						'classes'            => array( 'yith-etwcp-input-name-field', 'yith-etwcp-input-text-field' ),
					)
				)
			);
			echo esc_html(
				yith_etwcp_get_view(
					'/input-fields/text.php',
					array(
						'field_name'         => '_yith_etwcp[--yith_template--][surname]',
						'field_name_display' => __( 'Surname', 'yith-etwcp-text-domain' ),
						'classes'            => array( 'yith-etwcp-input-surname-field', 'yith-etwcp-input-text-field' ),
					)
				)
			);
			?>
		</div>
	</div>
</div>
<?php
