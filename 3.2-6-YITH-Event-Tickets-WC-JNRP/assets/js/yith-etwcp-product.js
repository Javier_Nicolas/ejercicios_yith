jQuery(
	function($) {
		// Global
		var index = 0;
		// Events
		$( document ).ready( load_events );
		function load_events() {
			$( 'div.yith-etwcp-loading-container' ).fadeOut();
			$( 'div.yith-etwcp-addon-input-text' ).find( ":input" ).on( 'keyup', change_text_price );
			$( 'div.yith-etwcp-addon-input-group' ).find( ":input" ).on( 'change', update_price_summary );
			$( 'div.yith-etwcp-addon-input-check' ).find( ":input" ).on( 'change', update_price_summary );
			$( ".yith-etwcp-input-check-onoff-field" ).on( 'change', onoff_change );
		}

		$( 'div.yith-etwcp-container' ).ready( update_price_summary );

		function add_ticket(){
			var hidden = $('.yith-etwcp-single-ticket-template');
			var clonned = $( hidden ).clone( true, true ).removeClass( 'yith-etwcp-single-ticket-template' ).insertAfter( hidden );
			$( clonned ).each(
				function () {
					$( this ).find( ':input' ).each(
						function(){
							$( this ).attr( 'name', $( this ).attr( 'name' ).replace( '--yith_template--', index ) );
							$( this ).attr( 'id', $( this ).attr( 'id' ).replace( '--yith_template--', index ) );
						}
					);
					$( this ).find( 'label' ).each(
						function(){
							$( this ).attr( 'for', $( this ).attr( 'for' ).replace( '--yith_template--', index ) );
						}
					);
				}
			);
			index++;
		}
		
		function remove_ticket(){
			// TODO: while index > quantity
			$('.yith-etwcp-single-ticket').last().remove();
		}
	}
)
