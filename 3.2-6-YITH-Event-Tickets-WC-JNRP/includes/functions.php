<?php
/**
 * This file belongs to the Event Tickets for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

// HERE SET FUNCTIONS TO USE ON YOUR PROJECT LIKE Template functions.

if ( ! function_exists( 'yith_etwcp_get_template' ) ) {
	/**
	 * Include templates
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function yith_etwcp_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_ETWCP_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_etwcp_get_view' ) ) {
	/**
	 * Include views
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function yith_etwcp_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_ETWCP_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_etwcp_get_template' ) ) {
	/**
	 * Include templates
	 *
	 * @param string $file_name name of the file you want to include.
	 * @param array  $args (array) (optional) Arguments to retrieve.
	 */
	function yith_etwcp_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_CR_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


if ( ! function_exists( 'yith_etwcp_show_meta_name' ) ) {
	/**
	 * Filter metadata key to show a name.
	 *
	 * @param string $display_key .
	 */
	function yith_etwcp_show_meta_name( $display_key ) {
		if ( strpos( $display_key, 'yith_etwcp_addons_data' ) !== false ) {
			$aux = substr_replace( $display_key, '', 0, 23 );
			if ( strstr( $aux, '-' ) ) {
				$price       = substr_replace( strstr( $aux, '-' ), '', 0, 1 );
				$name        = strstr( $aux, '-', true );
				$display_key = $name . ' (+ ' . $price . ' ' . get_woocommerce_currency_symbol() . ')';
			} else {
				$display_key = $aux;
			}
		}
		if ( strpos( $display_key, 'yith_etwcp_base_price' ) !== false ) {
			$display_key = __( 'Base price', 'yith-etwcp-text-domain' );
		}
		return $display_key;
	}
}
add_filter( 'woocommerce_order_item_display_meta_key', 'yith_etwcp_show_meta_name', 10, 3 );
