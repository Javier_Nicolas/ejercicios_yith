<?php
/**
 * This file belongs to the Event Tickets for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_ETWCP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_ETWCP_PRODUCT_TYPE_ADMIN' ) ) {

	/**
	 * Admin related.
	 */
	class YITH_ETWCP_PRODUCT_TYPE_ADMIN {

		/**
		 * Main Instance
		 *
		 * @var YITH_ETWCP_PRODUCT_TYPE_ADMIN
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_ETWCP_PRODUCT_TYPE_ADMIN Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_ETWCP_PRODUCT_TYPE_ADMIN constructor.
		 */
		private function __construct() {
			// TABS.
			// Create the custom tab.
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'create_etwcp_tab' ) );
			// Add the custom fields.
			add_action( 'woocommerce_product_data_panels', array( $this, 'display_etwcp_fields' ) );
			// Save the custom fields.
			add_action( 'woocommerce_admin_process_product_object', array( $this, 'save_etwcp_fields' ) );

			// Product type.
			add_filter( 'product_type_selector', 'add_ticket_product_type_dropdown' );
			add_action( 'init', 'add_ticket_product_type_class' );
			add_filter( 'woocommerce_product_class', 'load_ticket_product_type_class', 10, 2 );
		}

		/**
		 * Add the new tab to the $tabs array.
		 *
		 * @param array $tabs .
		 */
		public function create_etwcp_tab( $tabs ) {
			if ( ! is_event_ticket() ) {
				return $tabs;
			}
			$tabs['yith_etwcp'] = array(
				'label'    => __( 'Ticket', 'yith-etwcp-text-domain' ),
				'target'   => 'yith_etwcp_panel',
				'class'    => array( 'etwcp_tab' ),
				'priority' => 80,
			);
			return $tabs;
		}

		/**
		 * Display fields for the new panel
		 */
		public function display_etwcp_fields() {
			if ( ! is_event_ticket() ) {
				return;
			}
			$post_id = get_the_ID();
			$product = wc_get_product( $post_id );

			echo esc_html(
				yith_etwcp_get_template(
					'/admin/etwcp-tab-template.php',
					array(
						'product_data' => ! empty( $product->get_meta( 'yith_etwcp_data' ) ) ? $product->get_meta( 'yith_etwcp_data' ) : array(),
					)
				)
			);
		}

		/**
		 * Save the custom fields.
		 *
		 * @param array $product .
		 */
		public function save_etwcp_fields( $product ) {
			if ( ! is_event_ticket() ) {
				return;
			}
			// Nonce verification.
			if ( isset( $_POST['_yith_etwcp_nonce'][ $index ] ) ) {
				if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['_yith_etwcp_nonce'][ $index ] ) ), '_yith_etwcp_admin_data' ) ) {
					print esc_html__( 'Sorry, your nonce did not verify.', 'yith-etwcp-text-domain' );
					exit;
				}
			} else {
				return;
			}

			$addons_data['name']    = isset( $_POST['_yith_etwcp']['name'] ) ? 1 : 0;
			$addons_data['surname'] = isset( $_POST['_yith_etwcp']['surname'] ) ? 1 : 0;
			$product->update_meta_data( 'yith_wcpa_data', $addons_data );
		}


		/**
		 * Add the custom type to the dropdown.
		 *
		 * @param array $types .
		 */
		public function add_ticket_product_type_dropdown( $types ) {
			$types['yith_etwcp_event_ticket'] = __( 'Event ticket', 'yith-etwcp-text-domain' );
			return $types;
		}

		// TODO: Make this file type class and move things to admin class.

		/**
		 * Add the custom type class.
		 */
		public function add_ticket_product_type_class() {
			class WC_Product_Custom extends WC_Product {
				public function get_type() {
					return 'yith_etwcp_event_ticket';
				}
			}
		}

		/**
		 * Load the custom type class.
		 *
		 * @param array $classname .
		 * @param array $product_type .
		 */
		public function load_ticket_product_type_class( $classname, $product_type ) {
			if ( $product_type == 'yith_etwcp_event_ticket' ) {
				$classname = 'YITH_Product_Event_Ticket';
			}
			return $classname;
		}

		/**
		 * Check if product of type.
		 */
		public function is_event_ticket() {
			$post_id = get_the_ID();
			$product = wc_get_product( $post_id );
			return ( $product->is_type( 'yith_etwcp_event_ticket' ) );
		}
	}
}

/** Admin CSS & JS */

if ( ! function_exists( 'yith_etwcp_load_tabs_css' ) ) {
	/** Purchase note tab css. */
	function yith_etwcp_load_tabs_css() {
		$screen     = get_current_screen();
		$is_product = 'product' === $screen->id;
		if ( $is_product ) {
			// Font Awesome.
			if ( ! wp_style_is( 'yith-etwcp-load-fa-css' ) ) {
				wp_register_style(
					'yith-etwcp-load-fa-css',
					'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css',
					array(),
					'5.11.2'
				);
				wp_enqueue_style( 'yith-etwcp-load-fa-css' );
			}
			wp_register_style(
				'yith-etwcp-load-tabs-css',
				YITH_ETWCP_DIR_ASSETS_CSS_URL . '/yith-etwcp-tab.css',
				array(),
				false
			);
			wp_enqueue_style( 'yith-etwcp-load-tabs-css' );
			wp_register_script(
				'yith-etwcp-load-tabs-js',
				YITH_ETWCP_DIR_ASSETS_JS_URL . '/yith-etwcp-tab.js',
				array( 'jquery' ),
				false,
				true
			);
			wp_enqueue_script( 'yith-etwcp-load-tabs-js' );
			wp_localize_script(
				'yith-etwcp-load-tabs-js',
				'js_strings',
				array(
					'untitled' => __( 'Untitled', 'yith-etwcp-text-domain' ),
				)
			);
		}
	}
}
add_action( 'admin_enqueue_scripts', 'yith_etwcp_load_tabs_css' );


