<?php
/**
 * This file belongs to the Event Tickets for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 */

if ( ! defined( 'YITH_ETWCP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_ETWCP_Frontend' ) ) {

	/**
	 * Frontend related.
	 */
	class YITH_ETWCP_Frontend {

		/**
		 * Main Instance
		 *
		 * @var YITH_ETWCP_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_ETWCP_Frontend Main instance
		 * @author Javier Nicolás Rodríguez Pérez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_ETWCP_Frotend constructor.
		 */
		private function __construct() {
			// Cart hooks.
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'add_cart_data' ), 10, 3 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'get_cart_data' ), 10, 3 );
			add_action( 'woocommerce_add_order_item_meta', array( $this, 'cart_data_to_order' ), 10, 3 );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'calculate_price' ), 10, 1 );

			// Display in shop.
			add_action( 'woocommerce_before_add_to_cart_quantity', array( $this, 'product_addons_form_display' ) );

			/*/ AJAX.
			add_action( 'wp_ajax_nopriv_yith-etwcp-change-variation', array( $this, 'yith_etwcp_change_form' ) );
			add_action( 'wp_ajax_yith-etwcp-change-variation', array( $this, 'yith_etwcp_change_form' ) );
			//*/
		}

		/**
		 * Save addons info at adding to cart.
		 *
		 * @param array $cart_item_data .
		 * @param int   $product_id .
		 * @param int   $variation_id .
		 */
		public function add_cart_data( $cart_item_data, $product_id, $variation_id ) {
			// Nonce verification.
			if ( isset( $_POST['_yith_etwcp_nonce'] ) ) {
				if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['_yith_etwcp_nonce'] ) ), '_yith_etwcp_client_data' ) ) {
					print esc_html__( 'Sorry, your nonce did not verify.', 'yith-etwcp-text-domain' );
					exit;
				}
			} else {
				return;
			}
			if ( empty( $_POST['_yith_etwcp'] ) ) {
				return;
			}
			unset( $_POST['_yith_etwcp']['--yith_template--'] );
			$cart_item_data['yith_etwcp'] = wp_unslash( $_POST['_yith_etwcp'] );

			return $cart_item_data;
		}

		/**
		 * Show addons info in cart.
		 *
		 * @param int   $data .
		 * @param array $cart_item .
		 */
		public function get_cart_data( $data, $cart_item ) {
			if ( isset( $cart_item['yith_etwcp'] ) ) {
				foreach ( $cart_item['yith_etwcp'] as $key => $ticket_data ) {
					// TODO: Display tickets purchased simultaneosly as the same product (?).
					$data[] = array(
						'name'  => __( 'Name', 'yith-etwcp-text-domain' ),
						'value' => $ticket_data['name'],
					);
					$data[] = array(
						'name'  => __( 'Surname', 'yith-etwcp-text-domain' ),
						'value' => $ticket_data['surname'],
					);
				}
			}
			return $data;
		}

		/**
		 * Save addons info as meta.
		 *
		 * @param int    $item_id .
		 * @param array  $values .
		 * @param string $key .
		 */
		public function cart_data_to_order( $item_id, $values, $key ) {
			if ( isset( $values['yith_etwcp'] ) ) {
				foreach ( $cart_item['yith_etwcp'] as $key => $ticket_data ) {
					wc_add_order_item_meta( $item_id, 'yith-etwcp-ticket-data-name', $ticket_data['name'] );
					wc_add_order_item_meta( $item_id, 'yith-etwcp-ticket-data-surname', $ticket_data['surname'] );
				}
			}
		}

		/**
		 * Modify the item price.
		 *
		 * @param array $cart .
		 */
		public function calculate_price( $cart ) {}

		/**
		 * Addons form display for products.
		 */
		public function product_addons_form_display() {
			global  $woocommerce;
			$product = wc_get_product( get_the_ID() );

			if ( ! empty( $product->get_meta( 'yith_wcpa_data' ) ) ) {
				wp_nonce_field( '_yith_etwcp_client_data', '_yith_etwcp_nonce' );
				echo esc_html(
					yith_etwcp_get_template(
						'/frontend/etwcp-product-form.php',
						array()
					)
				);
			}
		}
	}
}

if ( ! function_exists( 'yith_etwcp_load_addons' ) ) {
	/**
	 * Addons form css and js.
	 *
	 * @param string $inline_styles CSS to add dynamically.
	 */
	function yith_etwcp_load_addons( $inline_styles = null ) {
		wp_register_style(
			'yith-etwcp-load-product-css',
			YITH_ETWCP_DIR_ASSETS_CSS_URL . '/yith-etwcp-product.css',
			array(),
			false
		);
		wp_enqueue_style( 'yith-etwcp-load-product-css' );
		wp_register_script(
			'yith-etwcp-load-product',
			YITH_ETWCP_DIR_ASSETS_JS_URL . '/yith-etwcp-product.js',
			array( 'jquery' ),
			false,
			true
		);
		wp_enqueue_script( 'yith-etwcp-load-product' );
		/*
		wp_register_style(
			'yith-etwcp-load-fa-css',
			'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css',
			array(),
			'5.11.2'
		);
		wp_enqueue_style( 'yith-etwcp-load-fa-css' );
		//*/
	}
}

add_action( 'wp_enqueue_scripts', 'yith_etwcp_load_addons' );
