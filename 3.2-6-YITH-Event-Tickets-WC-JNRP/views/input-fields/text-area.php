<?php
/**
 * This file belongs to the Event Tickets for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $classes Extra classes for the container.
 * @param string $field_name Field's name.
 * @param string $field_name_display Name to display to users.
 * @param string $field_value Field's default or previous value.
 *
 * @param string $rows Textarea rows.
 * @param string $cols Textarea columns.
 * @param string $placeholder Textarea placeholder.
 */

?>
<div class="yith-etwcp-input yith-etwcp-input-text-area-container
	<?php
	if ( ! empty( $classes ) && is_array( $classes ) ) {
		foreach ( $classes as $class ) {
			echo esc_html( ' ' . $class );
		}
	}
	?>
">
	<?php if ( ! empty( $field_name_display ) ) { ?>
		<label class="yith-etwcp-input-text-area-label" for="input_text_area_<?php echo esc_html( $field_name ); ?>">
			<?php echo esc_html( $field_name_display ); ?>
		</label>
	<?php } ?>
	<textarea
		class="yith-etwcp-input-text-area-field"
		name="<?php echo esc_html( $field_name ); ?>"
		id="input_text_area_<?php echo esc_html( $field_name ); ?>"
		rows="<?php echo esc_html( ! empty( $rows ) ? $rows : '' ); ?>"
		cols="<?php echo esc_html( ! empty( $cols ) ? $cols : '' ); ?>"
		placeholder="<?php echo esc_html( ! empty( $placeholder ) ? $placeholder : '' ); ?>"
		><?php echo esc_html( ! empty( $field_value ) ? $field_value : '' ); ?></textarea>
</div>
