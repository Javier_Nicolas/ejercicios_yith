<?php
/**
 * This file belongs to the Event Tickets for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $classes Extra classes for the container.
 * @param string $field_name Field's name.
 * @param string $field_name_display Name to display to users.
 * @param string $field_value Field's default or previous value.
 *
 * @param string $min Field minimum.
 * @param string $max Field maximum.
 * @param string $step Field legal number intervals.
 */

?>
<div class="yith-etwcp-input yith-etwcp-input-number-container
	<?php
	if ( ! empty( $classes ) && is_array( $classes ) ) {
		foreach ( $classes as $class ) {
			echo esc_html( ' ' . $class );
		}
	}
	?>
">
	<?php if ( ! empty( $field_name_display ) ) { ?>
		<label class="yith-etwcp-input-number-label" for="input_number_<?php echo esc_html( $field_name ); ?>">
			<?php echo esc_html( $field_name_display ); ?>
		</label>
	<?php } ?>
	<input type="number"
		class="yith-etwcp-input-number-field"
		name="<?php echo esc_html( $field_name ); ?>"
		id="input_number_<?php echo esc_html( $field_name ); ?>"
		value="<?php echo esc_html( ! empty( $field_value ) ? $field_value : '0' ); ?>"
		min="<?php echo esc_html( ! is_null( $min ) ? $min : '' ); ?>"
		max="<?php echo esc_html( ! is_null( $max ) ? $max : '' ); ?>"
		step="<?php echo esc_html( ! is_null( $step ) ? $step : '' ); ?>"
	>
</div>
