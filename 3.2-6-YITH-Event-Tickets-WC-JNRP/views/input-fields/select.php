<?php
/**
 * This file belongs to the Event Tickets for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $classes Extra classes for the container.
 * @param array $group_data Data for each option.
 * @param string $field_name Select's name.
 * @param string $field_value Field's default or previous value.
 */

?>
<div class="yith-etwcp-input yith-etwcp-input-select-container
	<?php
	if ( ! empty( $classes ) && is_array( $classes ) ) {
		foreach ( $classes as $class ) {
			echo esc_html( ' ' . $class );
		}
	}
	?>
">
	<select
		class="yith-etwcp-input-select-field"
		name="<?php echo esc_html( $field_name ); ?>"
		id="input_select_<?php echo esc_html( $field_name ); ?>"
	>
		<?php foreach ( $group_data as $single_data ) { ?>
			<option
				value=<?php echo esc_html( $single_data['field_value'] ); ?>
				<?php echo esc_html( ! empty( $field_value ) && $field_value === $single_data['field_value'] ? 'selected' : '' ); ?>
			>
				<?php echo esc_html( $single_data['field_name_display'] ); ?>
			</option>
		<?php } ?>
	</select>
</div>
