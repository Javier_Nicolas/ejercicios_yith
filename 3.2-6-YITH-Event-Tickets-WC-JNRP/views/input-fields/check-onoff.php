<?php
/**
 * This file belongs to the Event Tickets for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $classes Extra classes for the container.
 * @param string $field_name Field's name.
 * @param string $field_name_display Name to display to users.
 *
 * @param string $checked .
 */

?>
<div class="yith-etwcp-input yith-etwcp-input-check-onoff-container
	<?php
	if ( ! empty( $classes ) && is_array( $classes ) ) {
		foreach ( $classes as $class ) {
			echo esc_html( ' ' . $class );
		}
	}
	?>
">
	<input
		type="checkbox"
		class="yith-etwcp-input-check-onoff-field"
		name="<?php echo esc_html( $field_name ); ?>"
		id="input_check_onoff_<?php echo esc_html( $field_name ); ?>"
		value="yes"
		<?php echo ! empty( $checked ) && $checked ? 'checked' : ''; ?>
	>
	<label class="yith-etwcp-input-check-onoff-label" for="input_check_onoff_<?php echo esc_html( $field_name ); ?>">
		<i class="yith-etwcp-input-check-onoff-icon fas <?php echo ! empty( $checked ) && $checked ? 'fa-toggle-on' : 'fa-toggle-off'; ?>"></i>
		<?php echo esc_html( ! empty( $field_name_display ) ? $field_name_display : '' ); ?>
	</label>
</div>
