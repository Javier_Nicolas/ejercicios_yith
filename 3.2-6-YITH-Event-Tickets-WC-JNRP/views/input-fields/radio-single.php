<?php
/**
 * This file belongs to the Event Tickets for Woocommerce Plugin.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package yith_formacion
 *
 * @param array $classes Extra classes for the container.
 * @param string $field_name Field's name.
 * @param string $field_name_display Name to display to users.
 * @param string $field_value Radio value.
 *
 * @param string $checked .
 */

?>
<div class="yith-etwcp-input yith-etwcp-input yith-etwcp-input-radio-container
	<?php
	if ( ! empty( $classes ) && is_array( $classes ) ) {
		foreach ( $classes as $class ) {
			echo esc_html( ' ' . $class );
		}
	}
	?>
">
	<input
		type="radio"
		class="yith-etwcp-input-radio-field"
		name="<?php echo esc_html( $field_name ); ?>"
		id="input_radio_<?php echo esc_html( $field_name ); ?>_<?php echo esc_html( $field_value ); ?>"
		value="<?php echo esc_html( $field_value ); ?>"
		<?php echo $checked ? 'checked' : ''; ?>
	>
	<label class="yith-etwcp-input-radio-label" for="input_radio_<?php echo esc_html( $field_name ); ?>_<?php echo esc_html( $field_value ); ?>">
		<?php echo esc_html( ! empty( $field_name_display ) ? $field_name_display : $field_value ); ?>
	</label>
</div>
